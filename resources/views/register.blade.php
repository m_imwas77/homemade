@extends('layout.master')
@section('content')

    <div class="page-wrapper bg-dark p-t-100 p-b-50" style="direction: rtl;background-color: #f1f1f1!important">
        <div class="wrapper wrapper--w900" style="background-color: cyan">
            <div class="card card-6">
                <div class="card-heading">
                    <h2 class="title" style="margin-top: -80px;float: right;color: #989494">انشاء حساب</h2>
                </div>
                <div  style="padding: 8px;">
                    <form method="POST" action="{{ route('createUser') }}">
                        @csrf
                        <div class="row" style="text-align: right">
                            <div class="col-sm-6">
                                <label for="inputPassword" class="col-form-label {{ $errors->has('first_name') ? 'text-danger' : '' }}">الاسم الاول</label>
                                <input value="{{ old('first_name') }}" type="text" class="form-control {{ $errors->has('first_name') ? 'is-invalid' : '' }}" id="inputPassword"   name="first_name" placeholder="الاسم الاول">
                                @error('first_name')
                                    <small id="passwordHelp"  class="text-danger">
                                        {{ $message }}
                                    </small>      
                                @enderror
                            </div>
                            <div class="col-sm-6">
                                <label for="inputPassword" class="col-form-label {{ $errors->has('last_name') ? 'text-danger' : '' }}">اسم العائله</label>
                                <input type="text" name="last_name" value="{{ old('last_name') }}"  type="password" class="form-control {{ $errors->has('last_name') ? 'is-invalid' : '' }}" id="inputPassword" placeholder="اسم العائله">
                                @error('last_name')
                                    <small id="passwordHelp"  class="text-danger">
                                        {{ $message }}
                                    </small>      
                                @enderror      
                            </div>
                        </div>


                        <div class="row" style="text-align: right">
                            <div class="col-sm-6">
                                <label for="inputPassword" class="col-form-label {{ $errors->has('email') ? 'text-danger' : '' }}">البريد الاكتروني</label>
                                <input type="email" name="email" value="{{ old('email') }}" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" id="inputPassword" placeholder="البريد الاكتروني">
                                @error('email')
                                    <small id="passwordHelp"  class="text-danger">
                                        {{ $message }}
                                    </small>      
                                @enderror      
                            </div>
                            <div class="col-sm-6">
                                <label for="inputPassword" class="col-form-label {{ $errors->has('user_name') ? 'text-danger' : '' }}">اسم المستخدم</label>
                                <input type="text" name="user_name"  value="{{ old('user_name') }}" type="password" class="form-control {{ $errors->has('user_name') ? 'is-invalid' : '' }}" id="inputPassword" placeholder="اسم المستخدم">
                                @error('user_name')
                                    <small id="passwordHelp"  class="text-danger">
                                        {{ $message }}
                                    </small>      
                                @enderror      
                            </div>
                        </div>

                        <div class="row" style="text-align: right">
                            <div class="col-sm-6">
                                <label for="inputPassword" class="col-form-label {{ $errors->has('password') ? 'text-danger' : '' }}" >كلمه المرور</label>
                                <input type="password" name="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" id="inputPassword" placeholder="كلمه المرور" id="passwordHelp" >
                                @error('password')
                                    <small id="passwordHelp"  class="text-danger">
                                        {{ $message }}
                                    </small>      
                                @enderror      
                            </div>
                            <div class="col-sm-6">
                                <label for="inputPassword" class="col-form-label ">تاكيد كلمه المرور</label>
                                <input type="password" name="password_confirmation" type="password" class="form-control" id="inputPassword" placeholder="تاكيد كلمه المرور">
                                    
                            </div>
                        </div>
                        <div class="row" style="text-align: right">
                            <div class="col-sm-6">
                                <label for="inputPassword" class="col-form-label {{ $errors->has('phone_number') ? 'text-danger' : '' }}">رقم الهاتف</label>
                                <input  type="text" name="phone_number" value="{{ old('phone_number') }}" class="form-control {{ $errors->has('phone_number') ? 'is-invalid' : '' }}" id="inputPassword" placeholder="رقم الهاتف" id="passwordHelp" >
                                @error('phone_number')
                                <small id="passwordHelp"  class="text-danger">
                                    {{ $message }}
                                </small>      
                            @enderror    
                            </div>
                            <div class="col-sm-6">
                                <label for="inputPassword" class="col-form-label">الجنس</label>
                                <div class="input-group">
                                    <select class="form-control" style="width: 100%;height: 40px;" name="gender" id="">
                                        <option value="0" {{ old('gender')  == 0 ? 'selected' : ''}}>ذكر</option>
                                        <option value="1" {{ old('gender')  == 1 ? 'selected' : ''}}>أنثى</option>
                                    </select>
                                </div>   
                            </div>
                        </div>
                        
                        <div class="row" style="text-align: right">
                            <div class="col-sm-4">
                                <label for="inputPassword" class="col-form-label {{ $errors->has('country') ? 'text-danger' : '' }}">الدوله</label>
                                <input  type="text" name="country" value="{{ old('country') }}"  class="form-control  {{ $errors->has('country') ? 'is-invalid' : '' }}" id="inputPassword" placeholder="الدوله" id="passwordHelp" >
                                @error('country')
                                    <small id="passwordHelp"  class="text-danger">
                                        {{ $message }}
                                    </small>      
                                @enderror      
                            </div>
                            <div class="col-sm-4">
                                <label for="inputPassword" class="col-form-label {{ $errors->has('') ? 'text-danger' : '' }}">المدينه</label>
                                <input type="text" name="city" type="text" value="{{ old('city') }}"  class="form-control  {{ $errors->has('city') ? 'is-invalid' : '' }}" id="inputPassword" placeholder="المدينه">
                                @error('city')
                                    <small id="passwordHelp"  class="text-danger">
                                        {{ $message }}
                                    </small>      
                                @enderror      
                            </div>
                            <div class="col-sm-4">
                                <label for="inputPassword" class="col-form-label  {{ $errors->has('street') ? 'text-danger' : '' }}">الشارع</label>
                                <input type="text" name="street" type="text" value="{{ old('street') }}"  class="form-control {{ $errors->has('street') ? 'is-invalid' : '' }}" id="inputPassword" placeholder="الشارع">
                                @error('street')
                                    <small id="passwordHelp"  class="text-danger">
                                        {{ $message }}
                                    </small>      
                                @enderror      
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="name" style="text-align:right">نوع الحساب</div>
                            <div class="value">
                                <div class="input-group">
                                    <select class="form-control"  name="role_id" style="width: 100%;height: 40px;" name="" id="type_account">
                                        <option value="4" {{ old('role_id')  == 4 ? 'selected' : ''}}>زبون</option>
                                        <option value="3" {{ old('role_id')  == 4 ? 'selected' : ''}}>بائع (أسرة متجة)</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div id="test"></div>
                        <div class="card-footer">
                            <button onclick="checkPassword(event)" class="btn btn--radius-2 btn--blue-2" type="submit" style="background-color:#007bff;color:white">انشاء الحساب</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="jquery-3.4.1.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
        function checkPassword(e) {
            var firstPass = document.getElementById('pass1').value;
            var secondPass = document.getElementById('pass2').value;
           
            if(firstPass != secondPass) {
                alert("كلمتا المرور غير متطابقات");
                e.preventDefault();
            }
            
        }
        function fields() {
            field =``;
            document.getElementById('test').innerHTML="";
            if($('#type_account').val() == 'customer'){
                field = ``;
                document.getElementById('role_id').value = 4;
            }
            else {
                field = `<div class="form-row">
                            <div class="name" style="text-align:right">تفاصيل اعمالك</div>
                            <div class="value">
                                <div class="input-group">
                                    <textarea class="textarea--style-6" name="story_body" placeholder="اضف نبذه من اعمالك"></textarea>
                                </div>
                            </div>
                        </div>`;
                document.getElementById('role_id').value = 3;
            }
            $('#test').append(field);
        }
    </script>
    <!-- Jquery JS-->
    <script src="vendor/jquery/jquery.min.js"></script>


    <!-- Main JS-->
    <script src="js/global.js"></script>
@endsection
<!-- end document-->