<footer class="site-footer">
    <div class="container" style="text-align: right">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            
            <div class="col-md-4">
              <h2 class="footer-heading mb-4">من نحن</h2>
              <p>اول موقع فلسطيني يضم كل الاسر المنجه التي تملك اعمالا منزليه ذات جوده عاليه</p>
            </div>
            <div class="col-md-4 ml-auto">
              <ul class="list-unstyled">
                <h2 class="footer-heading mb-4">روابط سريعه</h2>
                <li><a href="/" class="smoothscroll">الصفحه الرئيسييه</a></li>
                <li><a href=" {{ route('projectFillter') }} " class="smoothscroll">المشاريع</a></li>
              </ul>
            </div>
            <div class="col-md-4">
              <h2 class="footer-heading mb-4">تابعنا</h2>
              <a href="#" class="pl-0 pr-3"><span class="icon-facebook"></span></a>
              <a href="#" class="pl-3 pr-3"><span class="icon-twitter"></span></a>
              <a href="#" class="pl-3 pr-3"><span class="icon-instagram"></span></a>
              <a href="#" class="pl-3 pr-3"><span class="icon-linkedin"></span></a>
            </div>
            
            
          </div>
        </div>
      </div>
      <div class="row pt-1 mt-1 text-center">
        <div class="col-md-12">
          <div class="border-top pt-5">
            <p>
             &copy;  جميع الحقوق محفوظه للشخص الذي لغا كل معاني الابداع من داخلنا فربق العمل 
            </p>
          </div>
        </div>
        
      </div>
    </div>
  </footer>