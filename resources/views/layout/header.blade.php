<header class="site-navbar  js-sticky-header site-navbar-target" role="banner" style="background-color: #444;direction: rtl;">

    <div class="container">
      <div class="row align-items-center">
        
        <div class="col-6 col-xl-2">
          <img class="mb-0 site-logo" src="{{ Voyager::image(setting('admin.icon_image'))}}" style="width: 150px;height: 60px;">
        </div>

        <div class="col-12 col-md-10 d-none d-xl-block">
          <nav class="site-navigation position-relative text-right" role="navigation">

            <ul class="site-menu main-menu js-clone-nav mr-auto d-none d-lg-block">
              <li><a href="/" class="nav-link">الصفحة الرئيسية</a></li>
      
              <li><a href=" {{ route('projectFillter') }} " class="nav-link">تصفح المشاريع</a></li>
              <li>
                <div class="dropdown show">
                  <a href="#contact-section" class="nav-link" data-toggle="dropdown" style="color:white"> <i class="fas fa-cubes"></i> التصنيفات <span class="dropdown-toggle"></span></a>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <div class="row">
                      @php $main_categories = \App\MainCategory::all(); @endphp
                      @foreach($main_categories as $main_category)
                        <div class="col-3" style="text-align: right">  
                          <a class="dropdown-item" href="/main-category/{{ $main_category->id }}" style="color:#20466f;font-size: 15px;font-weight: bold;font-family: Noto Kufi Arabic, Open Sans, sans-serif">{{ $main_category->name }}</a>
                          <hr>
                          @php $sub_categories = \App\SubCategory::where('main_category_id', $main_category->id)->get() @endphp
                          @foreach($sub_categories as $sub_category)
                            <a class="dropdown-item" href="/sub-category/{{ $sub_category->id }}" style="font-size: 12px">{{ $sub_category->name }}</a>
                          @endforeach
                        </div>
                      @endforeach
                    </div>
                  </div>
                </div>
              </li>
              
                <li><a onclick="showSerche()" class="nav-link"><i class="fa fa-search" aria-hidden="true"></i></a></li>
              
              @if(!\Auth::check())
              <li class="pull-left" style="border: 1px solid white;color:white !important;margin-right: 10px"><a href="{{route('voyager.login')}}" class="nav-link"><i class="fas fa-sign-in-alt"></i> تسجيل الدخول</a></li>
              <li class="pull-left" style="border: 1px solid white;color:white !important"><a href="/register" class="nav-link"> <i class="fas fa-user-plus"></i> انشاء حساب</a></li>
              @else
                <li style="border: 1px solid white" class="pull-left">
                <div class="btn-group">
                  <a  href="#contact-section" class="nav-link" data-toggle="dropdown" style="color:white" href="{{route('voyager.profile')}}" class="nav-link">{{Auth::user()->first_name.' '.Auth::user()->last_name}} <span class="dropdown-toggle"></span></a>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <div style="text-align: right;font-size: 12px;width: 100%">  
                          <a class="dropdown-item" href="{{ route('voyager.profile') }}" >تعديل معلومات الحساب</a>
                          <a class="dropdown-item" href="{{ route('voyager.dashboard') }}" >لوحة التحكم</a>
                          <form action="{{ route('voyager.logout')}}" method="POST">
                            @csrf
                            <button class="dropdown-item" type="submit" >تسجيل خروج</button>
                          </form>
                        </div>
                  </div>
                </div>
              </li>
              @endif
            </ul>
          </nav>
        </div>
        <form action=" {{ route('sercheByName') }} "  style="width: 100%" method="get" id="sercheForm">
        <input type="text"   name="key" class="hsoub-search" placeholder="اكثب ثلاث احرف على الاقل ثم اضعض انتر للبحث" id="sercheBox" style="display: none">
        </form>
        <div class="col-6 d-inline-block d-xl-none ml-md-0 py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle float-right"><span class="icon-menu h3"></span></a></div>

      </div>
    </div>
    
  </header>
  <script>
        var show = true;
        function showSerche() {
          if(show)
            document.getElementById("sercheBox").style.display = "block";
          else 
            document.getElementById("sercheBox").style.display = "none";

          show = !show;
        }
        document.getElementById('sercheBox').onkeydown = function(e){
          console.log(document.getElementById('sercheBox').value.length);
          if(e.keyCode == 13 && document.getElementById('sercheBox').value.length >= 3){
           document.getElementById('sercheForm').submit();
          }else if(e.keyCode == 13){
            e.preventDefault();
          }
        };
  </script>
  <style>
    .hsoub-search {
      margin-bottom: 30px;
      margin-top: 30px;
      border-radius: 10px;
    height: 36px;
    padding: 2px 6px;
    width: 100%;
    background-color: #fff;
    font-family: "Noto Naskh Arabic",Helvetica,Arial,sans-serif;
    font-size: 18px;
    line-height: 1.1111111;
    border: 0;
    /* border-radius: 0; */
    -webkit-appearance: none;
    box-sizing: border-box;
    box-shadow: none;
    outline: 0;
}
  </style>