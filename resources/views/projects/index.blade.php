@extends('layout.master')
@section('content')
<style>
    .hov:hover{
        cursor: pointer;
        background-color: #f0f0f0!important;
    }
</style>

<div style="direction: rtl; text-align: right; padding-top: 30px ;background-color: #f0f0f0;padding-bottom: 80px">
    
    <div class="container">
          <div class="row">
              <div class="col-sm-12" style="text-align: right ; padding-bottom: 60px;">
                  <h2>الطبات المفتوحة
                </h2>
              </div>
          </div>
          <div class="row">
              <div class="col-3">
                  <form action=" {{ route('projectFillter') }} " method="GET">
                      {{-- @csrf --}}
                        {{-- <h2 style="text-align: right">التصنيف</h2> --}}
                        {{-- @foreach ($mainCategory as $item)
                            <div class="form-check">
                                <label class="form-check-label">
                                <input style="position: absolute;left: 0;" type="checkbox" class="form-check-input" name="categoty[]" id="" value="{{ $item->id }}" >
                                {{ $item->name }}
                                </label>
                            </div>
                        @endforeach --}}
                        
                        <h2 style="text-align: right">مدة التسليم</h2>
                        @php
                            $timeKeys = array_keys($time);
                        @endphp
                        @for ($i = 0; $i < count($timeKeys); $i++)
                            <div class="form-check">
                                <label class="form-check-label">
                                    {{ $timeKeys[$i] }}
                                    @php
                                        $requstTimes = (app('request')->input('time') == null) ? [] : app('request')->input('time') ;
                                    @endphp
                                    <input {{ in_array($time[$timeKeys[$i]], $requstTimes) ? 'checked' : '' }} onchange="this.form.submit();"  style="position: absolute;left: 0;" type="checkbox" class="form-check-input" name="time[]" id="" value=" {{ $time[$timeKeys[$i]] }} " >
                                </label>
                            </div>
                        @endfor
                        <br><br>
                        <h2 style="text-align: right">الميزانيه</h2>
                        @php
                            $priceKeys = array_keys($price);
                        @endphp
                        @for ($i = 0; $i < count($priceKeys); $i++)
                            <div class="form-check">
                                <label class="form-check-label">
                                    {{ $priceKeys[$i] }}
                                    @php
                                        $requstPrice = (app('request')->input('price') == null) ? [] : app('request')->input('price') ;
                                    @endphp
                                    <input {{ in_array( $price[$priceKeys[$i]], $requstPrice) ? 'checked' : '' }} onchange="this.form.submit();" style="position: absolute;left: 0;" type="checkbox" class="form-check-input" name="price[]" id="" value=" {{ $price[$priceKeys[$i]] }} " >
                                </label>
                            </div>
                        @endfor
                        <br><br><br>
                </form>
              </div>
              
              <div class="col-9">
              @if (count($orders) == 0)
                <section style="height: 400px">
                    <header class="container ">
                    <div class="d-flex align-items-center justify-content-center ">
                        <div class="d-flex flex-column">
                        <h4 class="text align-self-center p-2" style="padding-top: 150px!important">لا يوجد مشاريع مفتوحه حاليا</h4>
                        </div>
                    </div>
                    </header>
                </section>
                @else
                @foreach ($orders as $order)  
                    <div class="row  border hov" style="padding:15px;background-color: #fff">
                        <div class="col-2">
                            <img src="{{Voyager::image($order->customer->user->avatar )}}" alt="Avatar" style=" vertical-align: middle;
                            width: 50px;
                            height: 50px;
                            border-radius: 50%;">
                        </div> 
                        <div class="col-10">
                            <div style="text-align: right; font-size: 15px;padding-bottom:15px ">
                            <a href=" {{ route('browsProject',['id' => $order->id]) }} " style="color:black;text-decoration: none"> {{$order->title}}</a>
                            </div>  
                            <div class="row">
                                <div class="col-2" style="display: flex; flex-direction: row" title="اسم الزبون">
                                    <i class="fa fa-fw fa-user" ></i>
                                    <span style="padding-right: 10px"> {{$order->customer->user->first_name}}  </span>
                                </div>
                                <div class="col-3" style="display: flex;padding-right: 20px" title="تم الانشاء متذ">
                                    <i class="far fa-clock"></i>
                                    <span style="padding-right: 10px"> {{ $order->numberOfCreatedDay }} </span>
                                </div>
                                <div class="col-2" style="display: flex;padding-right: 50px" title="عدد العروض">
                                    <i class="fa fa-fw fa-ticket" ></i>
                                    <span style="padding-right: 10px"> {{ $order->numberOfBid }} </span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="row" style="padding-top: 10px">
                {{ $orders->links() }}
                </div>
                @endif
            </div>
          </div>
      </div>
    </div>
@endsection