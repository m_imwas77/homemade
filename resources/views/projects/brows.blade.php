@extends('layout.master')
@section('content')
<div style="direction: rtl;background-color: #f0f0f0; padding-top: 60px; padding-bottom: 80px">
      <div class="container">
          <div class="row">
              <div class="col-8">
                  <div class="row">
                    <h2 style="text-align: right; " >تفاصيل المشروع</h2> 
                    <h6 style="margin-right: 10px; padding-top: 15px">
                    @if($order->end_receive_bid)
                      <span class="badge badge-danger"  style="padding-top: 14px ; padding-right: 9px; padding-left: 9px;padding-bottom: 14px; ">مغلق</span>
                    @else
                      <span class="badge badge-success" style="padding-top: 14px ; padding-right: 9px; padding-left: 9px;padding-bottom: 14px;">مفتوح</span>
                    @endif
                    </h6>
                  </div>
                  <div class="row">
                        @foreach ($order->ordersDetails as $item)
                        <div class="card" style="width: 18rem;text-align: right" >
                          <div class="card-header">
                            <div class="row">
                              <div class="col-md-6" style="border-left: 1px solid black">
                                {{  $item->name }}
                              </div>
                              <div class="col-md-6">
                                {{ $item->quantity }} {{ $item->sub_cat->unit_name }}
                              </div>
                            </div>
                          </div>
                          <ul class="list-group list-group-flush">
                            <li class="list-group-item">{{ $item->desc}}</li>
                          </ul>
                        </div>
                        @endforeach
                    </div>
                     @if (Auth::check() && Auth::user()->role_id == 3 && $order->AuthBId == false && $order->end_receive_bid == false)
                        <form action="{{ route('addBid') }} " method="POST" style="padding-top: 50px">
                            @csrf
                            <input type="hidden" name="seller_id" value=" {{ Auth::user()->seller->id }} ">
                            <input type="hidden" name="order_id" value=" {{ $order->id }} ">
                            <h2 style="text-align: right" >أضف عرضك الآن</h2>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group" id="timeToDeleverGroup" style="text-align: right">
                                      <label for="">مدة التسليم </label>
                                      <input type="number" min="1"
                                        class="form-control" name="number_of_day" id="numberOfDay" aria-describedby="helpId" placeholder="">
                                      <small id="helpId"  class="form-text">مده التسليم ب الايام</small>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group has-error" id="priceGroup" style="text-align: right" >
                                        <label  for="">قيمة العرض </label>
                                        <input  type="number" min="1"
                                          class="form-control" name="price" id="price" aria-describedby="helpId" placeholder="">
                                        <small id="helpId" class="form-text " style="text-align: right">قيمه عرضك</small>
                                      </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group" id="detealsGroup" style="text-align: right" >
                                      <label for="">تفاصيل العرض</label>
                                      <textarea  class="form-control" name="description" id="detealsArea"></textarea>
                                      <small id="helpId" class="form-text" style="color:black!importamt">
                                        اضف هنا اختصار موجز لجذب انتباه العمليل لعرضك و لا تنسا ان تضيف 
                                          كل ما له علاقه باليه الاتسليم  و اليه الدف </small>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" onclick="checkValue(event)" name="" id="" class="btn btn-primary btn-lg btn-block">اضف عرضك</button>
                            </div>
                        </form>
                        <br>
                        @elseif($order->AuthBId)
                        @elseif(Auth::check() && Auth::user()->role_id == 4)
                          @if(count($order->bids) == 0)
                          <div class="row">
                           <h2>لا يوجد عروض حاليا</h2>
                          </div>
                          @endif
                        @else 
                        <div class="row" style="padding-top: 50px">
                            <h2>لا تستطيع اضافة عروض</h2>
                            <p style="text-align: right"> أنت غير مسجل حاليا
                                
                                <a href="/admin/login">. سجّل دخولك </a>
                                أو
                                 <a href="/register">
                                  أنشئ حسابا جديدا</a>
                                    لتتمكن من تقديم عرضك على هذا الطبيه.
                                    <br>
                                    او ان وقت تقديم العروض قد انتها
                                  </p>
                        </div>
                     @endif
                     @php
                              $authUserId = -1; 
                                if(Auth::check()) {
                                  $authUserId  = Auth::user()->id;
                                }
                      @endphp
                      @if (count($order->bids) > 0 && $authUserId == $order->bids[0]->seller->user->id )
                     <div class="row" style="padding-top: 50px">
                      <ul class="list-unstyled border col-12" style="background-color: #fff;padding:10px">
                        <li class="media">
                          <img  src="{{Voyager::image($order->bids[0]->seller->user->avatar )}}" alt="Avatar" style=" vertical-align: middle;
                          width: 50px;
                          height: 50px;
                          border-radius: 50%;">
                          <div class="media-body " style="text-align: right;">
                            <h5 class="mt-0 mb-1" >{{ $order->bids[0]->seller->user->first_name }}</h5>
                              <h6>   االسعر  : {{ $order->bids[0]->price }} </h6> 
                              <h6> الايام   :  {{ $order->bids[0]->number_of_day }} </h6>
                            
                            تفاصيل العرض : {{ $order->bids[0]->description }}
                              @if($order->end_receive_bid == false)
                              <div style="display: flex;flex-direction: row">
                                <form action="{{ route('deleteBid',['id' => $order->bids[0]->id]) }}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                  <button style="color: black" type="submit" class="btn btn-danger ">حذف</button>
                                </form>
                                <a style="margin-right: 10px;color: black" type="button" class="btn btn-primary" onclick="openEditModal('{{$order->bids[0]->id}}', '{{ $order->bids[0]->price }}', '{{ $order->bids[0]->number_of_day }}', '{{ $order->bids[0]->description }}')">
                                    تعديل
                                </a>
                              </div>
                              @endif
                            
                          </div>
                        </li>
                      </ul>
                    </div>
                    @endif
                     @if(Auth::check() && Auth::user()->role_id == 4 && $order->customer->user->id == Auth::user()->id)
                    @foreach ($order->bids as $item)
                        <div class="row">
                          <ul class="list-unstyled border col-12" style="background-color: #fff;padding:10px">
                            <li class="media">
                              <img  src="{{Voyager::image($item->seller->user->avatar )}}" alt="Avatar" style=" vertical-align: middle;
                              width: 50px;
                              height: 50px;
                              border-radius: 50%;">
                              <div class="media-body " style="text-align: right;">
                                <h5 class="mt-0 mb-1" >{{ $item->seller->user->first_name }}</h5>
                                @if (Auth::check() && Auth::user()->id ==  $order->customer->user->id)
                                  <h6>   االسعر  : {{ $item->price }} </h6> 
                                  <h6> الايام   :  {{ $item->number_of_day }} </h6>
                                @endif
                                تفاصيل العرض : {{ $item->description }}
                                @php
                                  $authUserId = -1; 
                                    if(Auth::check()) {
                                      $authUserId  = Auth::user()->id;
                                    }
                                @endphp
                                @if ($authUserId == $item->seller->user->id)
                                  <div style="display: flex;flex-direction: row">
                                    <form action="{{ route('deleteBid',['id' => $item->id]) }}" method="POST">
                                        @method('DELETE')
                                        @csrf
                                      <button type="submit" class="btn btn-danger ">حذف</button>
                                    </form>
                                    <a style="margin-right: 10px;" type="button" class="btn btn-primary" onclick="openEditModal('{{$item->id}}', '{{ $item->price }}', '{{ $item->number_of_day }}', '{{ $item->description }}')">
                                        تعديل
                                    </a>
                                  </div>
                                @endif
                                @if (Auth::check() && Auth::user()->id ==  $order->customer->user->id && $order->accepted == 0)
                                  <form action="{{ route('acceptBid',['seeler_id' => $item->seller->user->id, 'order_id' => $order->id]) }}" method="POST">
                                    @csrf
                                    <button style="margin-right: 10px;" type="submit"  class="btn btn-primary" >قبول العرض و بدء العمل</button>
                                  </form>
                                @endif
                              </div>
                            </li>
                          </ul>
                        </div>
                    @endforeach 
                    @endif
                </div>
              
              <div class="col-4" style="text-align: right; ">
                  <div class="border" style="background-color: #fff;padding:10px">
                    <h2>بطاقة المشروع</h2>
                    <div class="row">
                        <div class="col-6">
                          <h6>الميزانية</h6>
                          <h6>مدة التنفيذ	</h6>
                          <h6>عدد العروض</h6>
                          <h6>عدد الايام المتبقه لاستقبال العروض</h6>
                        </div>
                        <div class="col-6">
                          <h6>{{ $order->max_price }}</h6>
                          <h6>{{ $order->number_of_days }} يوم</h6>
                          <h6>{{ count($order->bids) }}</h6>
                          {{$order->end_data }}
                          @if (\Carbon\Carbon::now() > $order->end_date)
                              <h6>ايام 0</h6>
                          @else
                          <h6>{{\Carbon\Carbon::parse(\Carbon\Carbon::now())->diffInDays($order->end_date ) + 1}} ايام</h6>
                          @endif
                        </div>
                    </div>
                  </div>
                  <div class="border" style="background-color: #fff;padding:10px">
                    <h2>صاحب المشروع</h2>
                    <div class="row">
                      <div class="col-2">
                          <img src="{{Voyager::image($order->customer->user->avatar)}}" alt="Avatar" style=" vertical-align: middle;
                          width: 50px;
                          height: 50px;
                          border-radius: 50%;">
                      </div>
                      
                      <div class="col-10">
                          <h5>{{ $order->customer->user->first_name }}  {{ $order->customer->user->last_name }}</h5>
                          <h6> {{ $order->customer->user->country}}</h6>
                          @php
                              $rate = \App\CustomerRate::where('customer_id', '=', $order->customer->id);
                              $finalRate = 0;
                              if($rate->count() > 0)
                                $finalRate = round($rate->sum('rate')/ $rate->count());
                          @endphp
                          @if ($finalRate ==0)
                          <i class="fa fa-star"  aria-hidden="true"></i>
                          <i class="fa fa-star"  aria-hidden="true"></i>
                          <i class="fa fa-star"  aria-hidden="true"></i>
                          <i class="fa fa-star"  aria-hidden="true"></i>
                          <i class="fa fa-star"  aria-hidden="true"></i>
                      @endif
                          @if ($finalRate == 1)
                          <i class="fa fa-star" style="color: yellow" aria-hidden="true"></i>
                          <i class="fa fa-star"  aria-hidden="true"></i>
                          <i class="fa fa-star"  aria-hidden="true"></i>
                          <i class="fa fa-star"  aria-hidden="true"></i>
                          <i class="fa fa-star"  aria-hidden="true"></i>
                      @endif
                      @if ($finalRate == 2)
                          <i class="fa fa-star" style="color: yellow" aria-hidden="true"></i>
                          <i class="fa fa-star"  style="color: yellow" aria-hidden="true"></i>
                          <i class="fa fa-star"  aria-hidden="true"></i>
                          <i class="fa fa-star"  aria-hidden="true"></i>
                          <i class="fa fa-star"  aria-hidden="true"></i>
                      @endif
                      
                      @if ($finalRate == 3)
                          <i class="fa fa-star" style="color: yellow" aria-hidden="true"></i>
                          <i class="fa fa-star"  style="color: yellow" aria-hidden="true"></i>
                          <i class="fa fa-star"  style="color: yellow" aria-hidden="true"></i>
                          <i class="fa fa-star"  aria-hidden="true"></i>
                          <i class="fa fa-star"  aria-hidden="true"></i>
                      @endif
                      
                      @if ($finalRate == 4)
                          <i class="fa fa-star" style="color: yellow" aria-hidden="true"></i>
                          <i class="fa fa-star"  style="color: yellow" aria-hidden="true"></i>
                          <i class="fa fa-star"  style="color: yellow" aria-hidden="true"></i>
                          <i class="fa fa-star"  style="color: yellow" aria-hidden="true"></i>
                          <i class="fa fa-star"  aria-hidden="true"></i>
                      @endif
                      
                      @if ($finalRate == 5)
                          <i class="fa fa-star" style="color: yellow" aria-hidden="true"></i>
                          <i class="fa fa-star"  style="color: yellow" aria-hidden="true"></i>
                          <i class="fa fa-star"  style="color: yellow" aria-hidden="true"></i>
                          <i class="fa fa-star"  style="color: yellow" aria-hidden="true"></i>
                          <i class="fa fa-star"  style="color: yellow" aria-hidden="true"></i>
                      @endif
                      </div>
                      
                    </div>
                  </div>
                  <div  class="border" style="background-color: #fff;padding:10px">
                    <h2>شارك المشورع</h2>
                    <div class="row">
                        <div class="col-12">
                            <input type="text" style="width: 100%" readonly value="{{url("/go/{$order->id}")}}">
                        </div>
                    </div>
                  </div>
            </div>
          </div>
      </div>
    </div>
      <script>
          function openEditModal(bidId, price, numberOfDay, desc) {
            $("#modal_bid_id").val(bidId);
            $("#modal_price").val(price);
            $("#modal_number_of_day").val(numberOfDay);
            $("#modal_description").val(desc);
            $("#editBidModal").modal('show');
          }
          function checkValue(e) {
            let doPreventDefult = false;
            if($("#detealsArea").val() === "") {
              $("#detealsArea").css({border: "red 1px solid"});
              doPreventDefult = true;
            }
            if(parseInt($("#price").val()) < 0 || $("#price").val() === "") {
              $("#price").css({border: "red 1px solid"});
              doPreventDefult = true;
            }
            if(parseInt($("#numberOfDay").val()) < 0 ||  $("#numberOfDay").val() === "") {
              $("#numberOfDay").css({border: "red 1px solid"});
              doPreventDefult = true;
            }
            
            if(doPreventDefult) {
              e.preventDefault();
            }
          }
      </script>

@endsection

<div class="modal fade" id="editBidModal" style="text-align: right" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header" >
          <h5 class="modal-title" id="exampleModalLabel" style="position: absolute;right:50px">تعديل العرض</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{ route('updateBid') }} " method="POST">
                @method('put')
                @csrf
                <input type="hidden" name="id" id="modal_bid_id">
                <h2>أضف عرضك الآن</h2>
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                          <label for="">مدة التسليم </label>
                          <input type="number"
                            class="form-control" name="number_of_day"  id="modal_number_of_day" aria-describedby="helpId" placeholder="">
                          <small id="helpId" class="form-text text-muted">مده التسليم ب الايام</small>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="">قيمة العرض </label>
                            <input type="number"
                              class="form-control" name="price" id="modal_price" aria-describedby="helpId" placeholder="">
                            <small id="helpId" class="form-text text-muted">قيمه عرضك</small>
                          </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                          <label for="">تفاصيل العرض</label>
                          <textarea class="form-control"  id="modal_description" name="description" id=""></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <button type="submit" name="" id="" class="btn btn-primary btn-lg btn-block">عدل عرضك</button>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">اغلاق</button>
        </div>
      </div>
    </div>
  </div>