@extends('voyager::master')

@section('css')
    <style>
        .user-email {
            font-size: .85rem;
            margin-bottom: 1.5em;
        }
        .err{
            border-color: #dc3545;
    padding-right: calc(1.5em + 0.75rem);
    background-image: url(data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' fill='%23dc3545' viewBox='-2 -2 7 7'%3e%3cpath stroke='%23dc3545' d='M0 0l3 3m0-3L0 3'/%3e%3ccircle r='.5'/%3e%3ccircle cx='3' r='.5'/%3e%3ccircle cy='3' r='.5'/%3e%3ccircle cx='3' cy='3' r='.5'/%3e%3c/svg%3E);
    background-repeat: no-repeat;
    background-position: center right calc(0.375em + 0.1875rem);
    background-size: calc(0.75em + 0.375rem) calc(0.75em + 0.375rem)
        }
    </style>
@stop

@section('content')
    <div style="background-size:cover; background-image: url({{ Voyager::image( Voyager::setting('admin.bg_image'), voyager_asset('/images/bg.jpg')) }}); background-position: center center;position:absolute; top:0; left:0; width:100%; height:300px;"></div>
    <div style="height:160px; display:block; width:100%"></div>
    <div style="position:relative; z-index:9; text-align:center;">
        <img src="@if( !filter_var(Auth::user()->avatar, FILTER_VALIDATE_URL)){{ Voyager::image( Auth::user()->avatar ) }}@else{{ Auth::user()->avatar }}@endif"
             class="avatar"
             style="border-radius:50%; width:150px; height:150px; border:5px solid #fff;"
             alt="{{ Auth::user()->name }} avatar">
             <form action="changePass" method="POST">
                 @csrf
                 <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="" class="{{ session('old_pass1') ? 'text-danger' : '' }}">كلمه المرور الحاليه</label>
                            <input type="password"   name="old_pass" id="" class="form-control  {{session('old_pass1') ? 'err' : '' }}" placeholder="" aria-describedby="helpId">
                            @if(session('old_pass1'))
                             <small id="passwordHelp"  class="text-danger">
                                {{ session('old_pass1') }}
                            </small>      
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4"></div>
                </div>
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="" class="{{ $errors->has('password') ? 'text-danger' : '' }}">كلمه المرور الجديده</label>
                            <input type="password" name="password" id="" class="form-control {{ $errors->has('password') ? 'err' : '' }}" placeholder="" aria-describedby="helpId">
                            @error('password')
                            <small id="passwordHelp"  class="text-danger">
                                {{ $message }}
                            </small>      
                        @enderror      
                        </div>
                    </div>
                    <div class="col-md-4"></div>
                </div>
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label for="">تاكيد كلمه المرور الجديده</label>
                          <input type="password" name="password_confirmation" id="" class="form-control" placeholder="" aria-describedby="helpId">
                        </div>
                    </div>
                    <div class="col-md-4"></div>
                </div>
                <button type="submit" class="btn btn-primary">تغيير</button>
            </form>
    </div>
@stop
