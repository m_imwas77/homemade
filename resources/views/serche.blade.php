@extends('layout.master')
@section('content')
<div style="direction: rtl;background-color:#EEEEEE;">
<h1  style="color:black !important; padding: 30px;text-align: right">نتائج البحث بكلمه : {{  app('request')->input('key')  }} </h1>
    <div style="text-align: right;  padding-top: 50px; padding-bottom: 100px">
        <div class="row">
            <div class="col-md-3">
                <form action="{{ route('sercheByName') }}" method="GET" style="margin-right: 25px">
                    <input type="hidden" value="{{  app('request')->input('key')  }}" name="key">
                    <div class="form-check">
                    <label class="form-check-label">
                        التاريخ من الاحدث الى الاقدم                        
                        <input  {{ app('request')->input('sort') === 'n-o' ? 'checked' : '' }}   onchange="this.form.submit();" name="sort" value="n-o"  type="radio" class="form-check-input"  id=""  >
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                       التاريخ من القدم الى الاحدث
                        
                        <input  {{ app('request')->input('sort') === 'o-n' ? 'checked' : '' }} onchange="this.form.submit();" name="sort" value="o-n"  type="radio" class="form-check-input"  id=""  >
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        السعر من الاعلى لاى الاقل
                       
                        
                        <input {{ app('request')->input('sort') === 'h-l' ? 'checked' : '' }} onchange="this.form.submit();" name="sort" value="h-l"  type="radio" class="form-check-input"  id=""  >
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        السعر من الاقل الى  الاعلى
                        
                        <input {{ app('request')->input('sort') === 'l-h' ? 'checked' : '' }} onchange="this.form.submit();" name="sort" value="l-h"  type="radio" class="form-check-input"  id=""  >
                    </label>
                </div>
            </form>
            </div>
            <div class="col-md-9">
                <div class="row"> 
                @foreach ($products as $product)    
                    @php 
                        $image  = json_decode($product->images)[0];
                        if($image == ""){
                            $image = $product->images;
                        }
                    @endphp
                    <div class="col-md-2 col-sm-12" style="background-color:white;margin-right: 100px;margin-bottom: 20px;text-align: right">
                        <a href="/products/show/{{$product->id}}">
                            <img id="image" style="width: 100%;height: 150px; padding-top: 20px" src="{{asset(Voyager::image($image))}}" alt="">
                            <h3 style="color:black;padding: 15px 0;font-size: 20px;line-height: 1.9em;font-family: Noto Kufi Arabic, Open Sans, sans-serif;">
                                اسم المنتج: {{$product->name}}
                                <br>
                                سعر المنتج: {{$product->price}} 
                                @if($product->number_store != 0)
                                <br>
                                    عدد الشراء : {{$product->number_store}}</h3>
                                <hr>
                                    <p style="text-align: center">اشتري الآن</p>
                                @else
                                    </h3>
                                    <hr>
                                    <p style="text-align: center">كن اول مشتري</p>
                                @endif
                            </a>
                    </div>
                @endforeach
            </div>
             </div>
</div>

</div>
@endsection