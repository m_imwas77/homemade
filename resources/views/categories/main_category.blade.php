@extends('layout.master')
@section('content')
<div style="direction: rtl;background-color:#EEEEEE">
            <h1  style="text-align: center;color:black !important; padding: 30px;">{{$nameCategory}}</h1>
            
            @foreach ($subCategories as $category)
            @php
              $products = App\Product::orderby('products.created_at','desc')->where('sub_category_id', $category->id)->take(4)->get();
            @endphp
            @if( true)
                <div style="text-align: right">
                    <a href="/sub-category/{{ $category->id }}"><h1 style="padding-right: 100px;font-family: Noto Kufi Arabic, Open Sans, sans-serif;color:black">{{$category->name}} »</h1></a>
                    
                        <div class="row">
                            @if (count($products) == 0)
                                
                                 <h1  style="text-align: center;  margin: 0px auto; text-align: center;color:black !important; padding: 43px;">لا يوجد نتائج</h1>
                            @endif
                        @foreach ($products as $product)    
                            @php 
                                $image  = json_decode($product->images)[0];
                                if($image == ""){
                                    $image = $product->images;
                                }
                            @endphp
                           
                            <div class="col-2" style="background-color:white;margin-right: 100px;margin-bottom: 20px;text-align: right">
                                <a href="/products/show/{{$product->id}}">
                                <img id="image" style="width: 100%;height: 150px; padding-top: 20px" src="{{asset(Voyager::image($image))}}" alt="">
                                <h3 style="color:black;padding: 15px 0;font-size: 20px;line-height: 1.9em;font-family: Noto Kufi Arabic, Open Sans, sans-serif;">
                                   اسم المنتج: {{$product->name}}
                                    <br>
                                    سعر المنتج: {{$product->price}} 
                                    @if($product->number_store != 0)
                                    <br>
                                    عدد الشراء : {{$product->number_store}}</h3>
                                     <hr>
                                        <p style="text-align: center">اشتري الآن</p>
                                    @else
                                         </h3>
                                        <hr>
                                        <p style="text-align: center">كن اول مشتري</p>
                                    @endif
                                 </a>
                            </div>
                        @endforeach
                    </div>
                </div>
                @endif
            @endforeach
       
    </div>
@endsection