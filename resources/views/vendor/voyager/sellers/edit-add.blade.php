@php
    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());
@endphp

@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular'))

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular') }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                            class="form-edit-add"
                            action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) : route('voyager.'.$dataType->slug.'.store') }}"
                            method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        @if($edit)
                            {{ method_field("PUT") }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{($edit ? 'editRows' : 'addRows' )};
                            @endphp
                            <!-- GET THE DISPLAY OPTIONS -->
                            <input type="hidden" name="role_id" value="3">
                            <div class="form-group  col-md-6 ">
                                <label class="control-label" for="name">الاسم الاول</label>
                                <input required type="text" class="form-control" name="first_name" placeholder="First Name" value="{{isset($user->first_name) ? $user->first_name : ''}}">
                            </div>
                            <!-- GET THE DISPLAY OPTIONS -->
                            <div class="form-group  col-md-6 ">
                                <label class="control-label" for="name">اسم العائله</label>
                                <input type="text" class="form-control" name="last_name" placeholder="Last Name" value="{{isset($user->last_name) ? $user->last_name : ''}}">
                            </div>
                            <div class="form-group  col-md-6 ">
                                <label class="control-label" for="name">رقم الهاتف</label>
                                <input type="text" class="form-control" name="phone_number" placeholder="Phone Number" value="{{isset($user->phone_number) ? $user->phone_number : ''}}">
                            </div>
                            <!-- GET THE DISPLAY OPTIONS -->
                            <div class="form-group  col-md-6 ">
                                <label class="control-label" for="name">البريد الاكتروني</label>
                                <input type="text" class="form-control" name="email" placeholder="Email" value="{{isset($user->email) ? $user->email : ''}}">
                            </div>
                            
                            <!-- GET THE DISPLAY OPTIONS -->
                            <div class="form-group  col-md-6 ">
                                <label class="control-label" for="name">الجنس</label>
                                <select class="form-control select2" name="gender">
                                    <option value="0" {{ ( isset($user->gender) && $user->gender == 0 ) ? 'selected' : '' }}>ذكر</option>
                                    <option value="1" {{ ( isset($user->gender) && $user->gender == 1)  ? 'selected' : '' }} >انثى</option>
                                </select>
                            </div>
                            <!-- GET THE DISPLAY OPTIONS -->
                            <div class="form-group  col-md-6 ">
                                <label class="control-label" for="name">اسم المستخدم</label>
                                <input type="text" class="form-control" name="user_name" placeholder="User Name" value="{{isset($user->user_name) ? $user->user_name : ''}}">
                            </div>
                            <!-- GET THE DISPLAY OPTIONS -->
                            <div class="form-group  col-md-6 ">
                                <label class="control-label" for="name">كلمه المرور</label>
                                @isset($user)
                                    <h6> اذا ارت تغير كلمه المرور اكتب كلمه المرور الجديده هنا </h6>                                
                                @endisset
                                <input type="password" {{ isset($user) ? '' : 'required' }}  class="form-control" name="password" value="">
                            </div>
                            <!-- GET THE DISPLAY OPTIONS -->
                            <div class="form-group  col-md-6 ">
                                <label class="control-label" for="name">الصوره الشخصيه</label>
                                @isset($user->avatar)
                                    <img src="{{ Voyager::image($user->avatar) }}" alt="" style="width:100px;height: 100px; border-radius: 100%">
                                @endisset
                                <input type="file" name="avatar" accept="image/*">
                            </div>
                            <!-- GET THE DISPLAY OPTIONS -->
                            <div class="form-group  col-md-4 ">
                                <label class="control-label" for="name">الدوله</label>
                                <input type="text" class="form-control" name="country" placeholder="Country" value="{{isset($user->country) ? $user->country : ''}}">
                            </div>
                            <!-- GET THE DISPLAY OPTIONS -->
                            <div class="form-group  col-md-4 ">
                                <label class="control-label" for="name">المدينه</label>
                                <input type="text" class="form-control" name="city" placeholder="City" value="{{isset($user->city) ? $user->city : ''}}">
                            </div>
                            <!-- GET THE DISPLAY OPTIONS -->
                            <div class="form-group  col-md-4 ">
                                <label class="control-label" for="name">الشارع</label>
                                <input type="text" class="form-control" name="street" placeholder="Street" value="{{isset($user->street) ? $user->street : ''}}">
                            </div>
                            
                            @foreach($dataTypeRows as $row)
                                <!-- GET THE DISPLAY OPTIONS -->
                                @php
                                    $display_options = $row->details->display ?? NULL;
                                    if ($dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')}) {
                                        $dataTypeContent->{$row->field} = $dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')};
                                    }
                                @endphp
                                @if (isset($row->details->legend) && isset($row->details->legend->text))
                                    <legend class="text-{{ $row->details->legend->align ?? 'center' }}" style="background-color: {{ $row->details->legend->bgcolor ?? '#f0f0f0' }};padding: 5px;">{{ $row->details->legend->text }}</legend>
                                @endif

                                <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width ?? 12 }} {{ $errors->has($row->field) ? 'has-error' : '' }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                    {{ $row->slugify }}
                                    <label class="control-label" for="name">{{ $row->getTranslatedAttribute('display_name') }}</label>
                                    @include('voyager::multilingual.input-hidden-bread-edit-add')
                                    @if (isset($row->details->view))
                                        @include($row->details->view, ['row' => $row, 'dataType' => $dataType, 'dataTypeContent' => $dataTypeContent, 'content' => $dataTypeContent->{$row->field}, 'action' => ($edit ? 'edit' : 'add')])
                                    @elseif ($row->type == 'relationship')
                                        @include('voyager::formfields.relationship', ['options' => $row->details])
                                    @else
                                        {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                    @endif

                                    @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                        {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                    @endforeach
                                    @if ($errors->has($row->field))
                                        @foreach ($errors->get($row->field) as $error)
                                            <span class="help-block">{{ $error }}</span>
                                        @endforeach
                                    @endif
                                </div>
                            @endforeach
                            @if(Auth::user()->role->name === 'admin')
                            @isset($user->active)
                                <!-- GET THE DISPLAY OPTIONS -->
                                <div class="form-group  col-md-12 ">
                                    <label class="control-label" for="name">Active</label>
                                    <br>
                                    <input type="checkbox" name="active"  {{ $user->active ? 'checked' : ''  }}  class="toggleswitch">
                                </div>                            
                            @endisset
                        @endif
                        </div><!-- panel-body -->
                        <div class="panel-footer">
                            @section('submit-buttons')
                                <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                            @stop
                            @yield('submit-buttons')
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                                 onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script>
        var params = {};
        var $file;

        function deleteHandler(tag, isMulti) {
          return function() {
            $file = $(this).siblings(tag);

            params = {
                slug:   '{{ $dataType->slug }}',
                filename:  $file.data('file-name'),
                id:     $file.data('id'),
                field:  $file.parent().data('field-name'),
                multi: isMulti,
                _token: '{{ csrf_token() }}'
            }

            $('.confirm_delete_name').text(params.filename);
            $('#confirm_delete_modal').modal('show');
          };
        }

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', deleteHandler('img', true));
            $('.form-group').on('click', '.remove-single-image', deleteHandler('img', false));
            $('.form-group').on('click', '.remove-multi-file', deleteHandler('a', true));
            $('.form-group').on('click', '.remove-single-file', deleteHandler('a', false));

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.'.$dataType->slug.'.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $file.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing file.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@stop
