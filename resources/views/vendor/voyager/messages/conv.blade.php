@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' ')

@section('page_header')
    <div>
        @include('voyager::multilingual.language-selector')
    </div>
   
@stop

@section('content')
    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-bordered">
                        <div class="panel-body">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        <a  id="toggleMessageText"  style="margin-bottom: 18px" class="btn btn-primary" href="#" role="button" onclick="ToggleMessageForm()">اظهار نموذج الرساله</a>
                        <div id="messageForm" style="display: none">
                            <form action="{{ Route('storeMsg') }}" method="POST">
                                @csrf
                                <div  class="form-group  col-md-12 ">        
                                    <label class="control-label" for="name">الرساله</label>
                                    <textarea  class="form-control richTextBox" name="message" id="richtextmessage"></textarea> 
                                </div>
                                <input type="hidden" name="resever_id" value="{{ $id }}">
                                <button type="submit" class="btn btn-primary save">حفظ</button>
                            </form>
                        </div>
                        <hr>
                        @foreach ($sended_msg as $item)
                            <div class="row">
                                <div class="col-12" style="padding-right: 15px">
                                    <div class="float-right" style="padding-left: 25px">
                                        <img src="{{Voyager::image($item->user_sender->avatar )}}" alt="Avatar" style=" vertical-align: middle;
                                        width: 50px;
                                        height: 50px;
                                        border-radius: 50%;">
                                    </div>
                                    <div>
                                        <span>{{  $item->user_sender->first_name }} {{  $item->user_sender->last_name }} </span>
                                        <br>
                                        <span>  {{  \Carbon\Carbon::parse($item->created_at)->locale('ar')->diffForHumans() }} </span>
                                    </div>
                                </div>
                                <div class="col-12" style="padding-right: 40px;color: black">
                                    {!! $item->message  !!}
                                </div>
                                <hr>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('css')

@stop

@section('javascript')
    <script>
        const messageFormHiddenText = "اظهار نموذج الرساله";
        const messageFormUnHiddenText = "اخفاء نموذج الرساله";
        let toogle = false;
        function ToggleMessageForm() {
            $("#messageForm").fadeToggle("slow");
            if(toogle) {
                $("#toggleMessageText").html(messageFormHiddenText);
            }
            else {
                $("#toggleMessageText").html(messageFormUnHiddenText);
            }
            toogle = !toogle;

        }
        $(document).ready(function() {
            
        });
    </script>
@stop
