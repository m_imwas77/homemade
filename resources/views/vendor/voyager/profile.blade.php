@extends('voyager::master')

@section('css')
    <style>
        .user-email {
            font-size: .85rem;
            margin-bottom: 1.5em;
        }
    </style>
@stop

@section('content')
    <div style="background-size:cover; background-image: url({{ Voyager::image( Voyager::setting('admin.bg_image'), voyager_asset('/images/bg.jpg')) }}); background-position: center center;position:absolute; top:0; left:0; width:100%; height:300px;"></div>
    <div style="height:160px; display:block; width:100%"></div>
    <div style="position:relative; z-index:9; text-align:center;">
        <img src="@if( !filter_var(Auth::user()->avatar, FILTER_VALIDATE_URL)){{ Voyager::image( Auth::user()->avatar ) }}@else{{ Auth::user()->avatar }}@endif"
             class="avatar"
             style="border-radius:50%; width:150px; height:150px; border:5px solid #fff;"
             alt="{{ Auth::user()->name }} avatar">
        <h4>{{ ucwords(Auth::user()->name) }}</h4>
        <div class="user-email text-muted">{{ ucwords(Auth::user()->email) }}</div>
        <p>{{ Auth::user()->bio }}</p>
        @if ($route != '')
            @if(Auth::user()->role->name == 'seller') 
                @php 
                    $seller = App\Seller::where('user_id',Auth::user()->id)->first();
                    $route = 'sellers/'.$seller->id.'/edit';
                    $route2 = 'user/password/'.$seller->id;
                @endphp
            @elseif(Auth::user()->role->name == 'customer')
               @php 
                    $customer = App\Customer::where('user_id',Auth::user()->id)->first();
                    $route = 'customers/'.$customer->id.'/edit';
                    $route2 = 'user/password/'.$customer->id;
                @endphp
            @endif
            <a href="{{ $route }}" class="btn btn-primary">{{ __('voyager::profile.edit') }}</a>
            <a href="  {{   $route2 }} " class="btn btn-primary"> تغيير كلمه المرور </a>
        @endif
    </div>
@stop
