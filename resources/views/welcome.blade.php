@extends('layout.master')
@section('content')
    @php
    $sliders = App\Slider::where('View', '=', 1)->orderBy('created_at','desc')->get();
    @endphp
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            @php $i=0;@endphp
            @foreach ($sliders as $slider)
                @if($i==0)
                    <li data-target="#myCarousel" data-slide-to="{{$i++}}" class="active"></li>
                @else
                    <li data-target="#myCarousel" data-slide-to="{{$i++}}"></li>
                @endif
            @endforeach
        </ol>
        <div class="carousel-inner">
            @php $i=0;@endphp
            @foreach ($sliders as $slider)
                @if($i==0)
                <div class="carousel-item active" style="height: 400px;background: url({{Voyager::image($slider->image)}});background-repeat: no-repeat;background-size:100% 400px;">
                    <div style="text-align: center;position: relative;top:140px;">
                        <p style="font-size: 32px;padding: 100px 0 0;font-weight: normal;font-family: Noto Kufi Arabic, Open Sans, sans-serif;margin: 0;padding: 0;color: #fff;">{!!$slider->title!!}</p>
                        <p style="padding: 100px 0 0;font-weight: normal;font-family: Noto Kufi Arabic, Open Sans, sans-serif;margin: 0;padding: 0;color: #fff;">{!!$slider->body!!}</p>
                        @if( $slider->button_title != null)
                            <a href="{{ $slider->button_action??'#' }}" target="{{  $slider->target ?? '_blank' }} " class="btn btn-success" style="opacity: 0.8;color:white">{{ $slider->button_title }}</a>
                        @endif
                    </div>
                </div>
                @else
                    <div class="carousel-item" style="height: 400px;background: url({{Voyager::image($slider->image)}});background-repeat: no-repeat;background-size:100% 400px;">
                        <div style="text-align: center;position: relative;top:140px;">
                            <p style="font-size: 32px;padding: 100px 0 0;font-weight: normal;font-family: Noto Kufi Arabic, Open Sans, sans-serif;margin: 0;padding: 0;color: #fff;">{!!$slider->title!!}</p>
                            <p style="padding: 100px 0 0;font-weight: normal;font-family: Noto Kufi Arabic, Open Sans, sans-serif;margin: 0;padding: 0;color: #fff;">{!!$slider->body!!}</p>
                            @if( $slider->button_title != null)
                                <a href="{{ $slider->button_action??'#' }}" target="{{  $slider->target ?? '_blank' }} " class="btn btn-success" style="opacity: 0.8;color:white">{{ $slider->button_title }}</a>
                            @endif
                        </div>
                    </div>
                @endif
                @php
                    $i++;
                @endphp
            @endforeach
        </div>

        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>

        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

        <div style="direction: rtl;background-color:#EEEEEE">
            <h1 style="text-align: right;padding-top: 20px;margin-right: 70px; font-family: Noto Kufi Arabic, Open Sans, sans-serif;color:black" class="container">أضيف حديثا</h1>
            @php
                $products = App\Product::orderby('created_at','desc')->take(8)->get();   
            @endphp
            
                <div class="row" style="column-count: 20">
                @foreach ($products as $product)    
                    @php 
                        $image  = json_decode($product->images)[0];
                        if($image == ""){
                            $image = $product->images;
                        }
                    @endphp
                    <div class="col-md-2 col-sm-12" style="background-color:white;margin-right: 100px;margin-bottom: 20px;text-align: right">
                        <a href="/products/show/{{$product->id}}">
                            <img id="image" style="width: 100%;height: 150px; padding-top: 20px" src="{{asset(Voyager::image($image))}}" alt="">
                            <h3 style="color:black;padding: 15px 0;font-size: 20px;line-height: 1.9em;font-family: Noto Kufi Arabic, Open Sans, sans-serif;">
                                اسم المنتج: {{$product->name}} 
                                <br>
                                سعر المنتج: {{$product->price}}
                                @if($product->number_store != 0)
                                <br>
                                عدد الشراء : {{$product->number_store}}</h3>
                                 <hr>
                                    <p style="text-align: center">اشتري الآن</p>
                                @else
                                     </h3>
                                    <hr>
                                    <p style="text-align: center">كن اول مشتري</p>
                                @endif
                             </a>
                    </div>
                @endforeach
            
                </div>
                <br>
            @php
                $mainCategories = App\MainCategory::all();
            @endphp
            @foreach ($mainCategories as $category)
                @php
                    $products = App\Product::orderby('products.created_at','desc')->where('sub_categories.main_category_id', $category->id)->join('sub_categories', 'products.sub_category_id', 'sub_categories.id')->select('products.*')->take(4)->get();
                @endphp
                @if( count($products) != 0)
                    <div style="text-align: right">
                        <a href="/main-category/{{ $category->id }}"><h1 style="padding-right: 100px;font-family: Noto Kufi Arabic, Open Sans, sans-serif;color:black">{{$category->name}} »</h1></a>
                    
                            <div class="row">
                            @foreach ($products as $product)    
                                @php 
                                    $image  = json_decode($product->images)[0];
                                    if($image == ""){
                                        $image = $product->images;
                                    }
                                @endphp
                            
                                <div class="col-2" style="background-color:white;margin-right: 100px;margin-bottom: 20px;text-align: right">
                                    <a href="/products/show/{{$product->id}}">
                                    <img id="image" style="width: 100%;height: 150px; padding-top: 20px" src="{{asset(Voyager::image($image))}}" alt="">
                                    <h3 style="color:black;padding: 15px 0;font-size: 20px;line-height: 1.9em;font-family: Noto Kufi Arabic, Open Sans, sans-serif;">
                                        اسم المنتج: {{$product->name}}
                                        <br>
                                        سعر المنتج: {{$product->price}} 
                                        @if($product->number_store != 0)
                                        <br>
                                        عدد الشراء : {{$product->number_store}}</h3>
                                         <hr>
                                            <p style="text-align: center">اشتري الآن</p>
                                        @else
                                             </h3>
                                            <hr>
                                            <p style="text-align: center">كن اول مشتري</p>
                                        @endif
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif
            @endforeach
    </div>
@endsection