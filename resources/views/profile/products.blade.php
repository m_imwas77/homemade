@extends('layout.master')
@section('content')
    <div class="container-fluid" style="direction: rtl">
        <div class="row  pdn--vl">
            <div class="col-3"></div>
            <div class="col-6 col-md-offset-3">
                <div class="profile-card  pdn--vm text-center">
                    <div class="profile-card--avatar">
                        <a href="/profile/{{ $seller->id }}">
                            <img src="{{    Voyager::image($seller->user->avatar)    }}" class="profile-avatar img-shadow img-circle uavatar uavatar--128" width="128" height="128" alt=" {{ $seller->user->first_name.' '.$seller->user->last_name }}">
                        </a>
                    </div>
                    <div class="profile-details">
                        <h3 class="profile-name mrg--an">
                            <sup class="text-zeta" style="padding-top: 20px; font-size: 10px">
                                <i class="fa fa-circle clr-gray-silver" data-toggle="tooltip" title="" data-original-title="غير متصل"></i>
                            </sup>
                            {{ $seller->user->first_name.' '.$seller->user->last_name }} 
                        </h3>

                        <ul class="list-meta" style="display: flex;font-size: 15px;list-style-type: none;justify-content: center;">
                            <li class="profile-location">
                                <i class="fa fa-fw fa-map-marker"></i>
                                {{ $seller->user->country.' ـــ '.$seller->user->city.' ـــ '.$seller->user->street }}      
                            </li>
                            
                        </ul>
                    </div>
                </div>
            </div>
        </div>	
    </div>
    <div class="navbar navbar-secondary">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#profile-navbar">
                    <span class="sr-only">إظهار أو أخفاء القائمة</span>
                    <i class="icon-bar"></i>
                    <i class="icon-bar"></i>
                    <i class="icon-bar"></i>
                </button>
                <div class="navbar-left visible-xs pull-right">
                    <div class="navbar-btn mrg--as">
                        <div class="dropdown ">
                            <a tabindex="-1" class="btn" href="#">
                                <i class="fa fa-fw fa-send"></i>
                                <span class="action-text"></span>
                            </a>
                         </div>
                    </div>
                </div>
            </div>
                <ul class="nav navbar-nav mrg--vn" style="display: contents;direction: rtl;">
                    <li class=" ">
                        <p class="li_active" style="margin-top: -20px"></p>
                        <a href="/profile/product/{{ $seller->user_id }}">
                            <i class="fa fa-fw fa-briefcase"></i>
                            معرض الأعمال
                        </a>
                    </li>
                    <li class="">
                        <p class="li_notactive" style="margin-top: -20px"></p>
                        <a href="/profile/rate/{{ $seller->user_id }}">
                            <i class="fa fa-fw fa-star"></i>
                            التقييمات
                        </a>
                    </li>        
                 
                    <li>
                        <p class="li_notactive" style="margin-top: -20px"></p>
                        <a href="/profile/{{ $seller->id }}">
                            <i class="fa fa-fw fa-user"></i>
                            الملف الشخصي        
                        </a>
                    </li>
                    <li class="nav-divider mrg--an-imp"></li>
                </ul>
                
            </div>
        </div>	
    </div>
    <div>
    <div class="container-fluid">
    <div class="row">
        @foreach ($products as $product)    
            @php 
                $image  = json_decode($product->images)[0];
                if($image == ""){
                    $image = $product->images;
                }
            @endphp
            <div class="col-md-2 col-sm-12" style="background-color:white;margin-right: 100px;margin-bottom: 20px;text-align: right">
                <a href="/products/show/{{$product->id}}">
                    <img id="image" style="width: 100%;height: 150px; padding-top: 20px" src="{{asset(Voyager::image($image))}}" alt="">
                    <h3 style="color:black;padding: 15px 0;font-size: 20px;line-height: 1.9em;font-family: Noto Kufi Arabic, Open Sans, sans-serif;">
                        اسم المنتج : {{$product->name}}
                        <br>
                    السعر :  {{$product->price}}</h3>
                         <hr>
                         <p style="text-align: center">اشتري الان</p>
                     </a>
            </div>
        @endforeach
    
        </div>
    </div>
</div>
@endsection