@extends('layout.master')
@section('content')
<div style="background-color:  #f0f0f0">
    @if (session('status'))
   
    <div style="padding:50px;background-color:  #f0f0f0;text-align: right">
    <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session('status') }}
      </div>
    </div>
    @endif
    @php
    // dd($product);
        $image  = json_decode($product->images)[0];
        if($image == ""){
            $image = $product->images;
        }
    @endphp
    <div class="container" style="padding-top: 20px ; background-color: #f0f0f0">
        <div style="display: inline-flex;width: 100%;">
        <div class="col-4" style="text-align: right; direction: rtl">
            <div class="border" style="background-color: #fff;padding:10px">
              <h2>بطاقة المنتج</h2>
              <div class="row">
                <div class="col-6">
                  <h6>اسم المنتج : </h6>
                  <h6>سعر المنتج : </h6>
                  <h6>المنتج : </h6>
                </div>
                <div class="col-6">
                  <h6>{{ $product->name }}</h6>
                  <h6>{{ $product->price }}</h6>
                  <h6>
                    @if($product->available !=0)
                        <span style="color:green"> متوفر</span>
                    @else 
                        <span style="color:red"> غير متوفر</span>
                    @endif
                  </h6>
                </div>
            </div>
            </div>
            <div class="border" style="background-color: #fff;padding:10px">
              <h2>صاحب المنتج</h2>
              <div class="row">
                @php
                    $seller = \App\Seller::where('user_id', $product->seller_id)->first();
                    
                    $rates = \App\SellerRate::where('seller_id', '=', $seller->user->id);
                    $finalRate = 0;
                    if($rates->count() > 0)
                        $finalRate  = round($rates->sum('rate') / $rates->count());
                @endphp
               <div class="col-2">
                    <img src="{{Voyager::image($seller->user->avatar)}}" alt="Avatar" style=" vertical-align: middle;
                    width: 50px;
                    height: 50px;
                    border-radius: 50%;">
                </div>
                
                <div class="col-10">
                    <h5>{{ $seller->user->first_name.' '.$seller->user->last_name }}</h5>
                    <h6> {{ $seller->user->country}}</h6>
                    @if ($finalRate == 0)
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star"  aria-hidden="true"></i>
                    <i class="fa fa-star"  aria-hidden="true"></i>
                    <i class="fa fa-star"  aria-hidden="true"></i>
                    <i class="fa fa-star"  aria-hidden="true"></i>
                @endif
                    @if ($finalRate == 1)
                          <i class="fa fa-star" style="color: yellow" aria-hidden="true"></i>
                          <i class="fa fa-star"  aria-hidden="true"></i>
                          <i class="fa fa-star"  aria-hidden="true"></i>
                          <i class="fa fa-star"  aria-hidden="true"></i>
                          <i class="fa fa-star"  aria-hidden="true"></i>
                      @endif
                      @if ($finalRate == 2)
                          <i class="fa fa-star" style="color: yellow" aria-hidden="true"></i>
                          <i class="fa fa-star"  style="color: yellow" aria-hidden="true"></i>
                          <i class="fa fa-star"  aria-hidden="true"></i>
                          <i class="fa fa-star"  aria-hidden="true"></i>
                          <i class="fa fa-star"  aria-hidden="true"></i>
                      @endif
                      
                      @if ($finalRate == 3)
                          <i class="fa fa-star" style="color: yellow" aria-hidden="true"></i>
                          <i class="fa fa-star"  style="color: yellow" aria-hidden="true"></i>
                          <i class="fa fa-star"  style="color: yellow" aria-hidden="true"></i>
                          <i class="fa fa-star"  aria-hidden="true"></i>
                          <i class="fa fa-star"  aria-hidden="true"></i>
                      @endif
                      
                      @if ($finalRate == 4)
                          <i class="fa fa-star" style="color: yellow" aria-hidden="true"></i>
                          <i class="fa fa-star"  style="color: yellow" aria-hidden="true"></i>
                          <i class="fa fa-star"  style="color: yellow" aria-hidden="true"></i>
                          <i class="fa fa-star"  style="color: yellow" aria-hidden="true"></i>
                          <i class="fa fa-star"  aria-hidden="true"></i>
                      @endif
                      
                      @if ($finalRate == 5)
                          <i class="fa fa-star" style="color: yellow" aria-hidden="true"></i>
                          <i class="fa fa-star"  style="color: yellow" aria-hidden="true"></i>
                          <i class="fa fa-star"  style="color: yellow" aria-hidden="true"></i>
                          <i class="fa fa-star"  style="color: yellow" aria-hidden="true"></i>
                          <i class="fa fa-star"  style="color: yellow" aria-hidden="true"></i>
                      @endif
                      <br>
                    <a class="btn btn-success btn-sm" style="font-size: 11px !important; padding: 7px 7px !important;margin-right: -20%;" href="/profile/{{ $seller->id }}"> التعرف اكثر</a>
                
                </div>
                
              </div>
            </div>
            <div  class="border" style="background-color: #fff;padding:10px">
              <h2>شارك المنتج</h2>
              <div class="row">
                  <div class="col-12">
                      <input type="text" style="width: 100%" readonly value="{{url("/pro/{$product->id}")}}">
                  </div>
              </div>
            </div>
            @if( Auth::check() && Auth::user()->role->name == 'customer')
            <div class="border" style="background-color: #fff;padding:10px">
                {{-- <h2>شراء المنتج</h2> --}}
                <div class="col-12">
                        @if ($product->available == 1)
                            <a  class="btn btn-lg btn-primary" style="font-size: 10px;font-weight: bold; color:white" data-toggle="modal" data-target="#exampleModal"> <i class="fas fa-shopping-cart"></i>طلب المنتج </a>

                        @endif   
                        @php $favorites = App\Favorite::where('customer_id', Auth::user()->id)->where('type','products')->where('type_id', $product->id)->first()@endphp
                        @if($favorites)
                            <form action="" method="get" style="all:unset">
                                <input type="hidden" name="action" value="delete">
                                <button type="submit" class="btn btn-lg btn-success" style="font-size: 10px;font-weight: bold;">
                                    <i class="fas fa-star" style="color:yellow"></i>
                                    اضافة الى المفضلة
                                </button>
                            </form>
                        @else
                            <form action="" method="get" style="all:unset">
                                <input type="hidden" name="action" value="add">
                                <button type="submit" class="btn btn-lg btn-success" style="font-size: 10px;font-weight: bold;">
                                    <i class="far fa-star"></i>
                                    اضافة الى المفضلة
                                </button>
                            </form>
                        @endif    
            </div>

          </div>
        @endif    

        </div>
        
            <div style="width:70%;box-shadow: 0px 3px 3px 2px;">
                <img id="image" style="width: 100%;height: 400px;" src="{{asset(Voyager::image($image))}}" alt="">
                @php
                  $images  = json_decode($product->images);
                @endphp
                  <div class="row" style="direction: rtl;">
                    <div class="col-11" style="float: right;">
                        @php
                            $images  = json_decode($product->images);
                        @endphp
                        @if(empty($images))
                            <a>
                                <img class="images_products_multipul image_product_multipul_position" style="margin-top: 10px;float:right;margin-right: 10px;" id="0" src="{{asset(Voyager::image($image))}}" alt="">
                            </a>
                        @else
                            @for ($i = 0 ;  $i <count($images) ; $i++)
                                <a >
                                    @if($i!=0)
                                    <img class="images_products_multipul" style="margin-top: 10px;float:right;border-color: #666666;border-radius: 0px;margin-right: 10px;" id="{{$i}}" onclick="Color({{$i}})" height="100px" width="100px" src="{{asset(Voyager::image($images[$i]))}}" alt="">
                                    @else
                                        <img class="images_products_multipul" style="margin-top: 10px;float:right;margin-right: 10px;" id="{{$i}}" onclick="Color({{$i}})" height="100px" width="100px" src="{{asset(Voyager::image($images[$i]))}}" alt="">
                                    @endif
                                </a>
                            @endfor
                        @endif
                    </div>
                    <div class="col-11" style="text-align: right;margin-right: 20px;">
                        @if($product->description != null)
                            <h1 style="color:#4a4949">الوصف</h1>
                            <p style="color: #5a4f4f;font-size: 17px;">{{ $product->description }}</p>
                        @endif
                    </div>
                </div>
            </div>
            
        </div>

        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">شراء المنتج</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    {{-- <span aria-hidden="true" style="float:left">&times;</span> --}}
                  </button>
                </div>
                <form action="/orders/products" method="POST">
                    @csrf
                    <div class="modal-body" style="text-align: right;color:black;direction: rtl">
                        <img src="{{ Voyager::image($image) }}" alt="" style="position: absolute; height: 100px;width: 100px;left:20px;">
                        <div style="display:flex;width: 100%;">
                            <input type="hidden" name="products_id" value="{{ $product->id }}">
                            <h5> اسم المنتج : <span>{{ $product->name }} </span></h5>
                            <input type="hidden" name="title" value="{{$product->name }}">
                        </div>
                        <div style="display:flex;">
                            <h5> سعر المنتج : <span>{{ $product->price }} </span></h5>
                            <input type="hidden" name="price" value="{{$product->price }}">
                        </div>
                        <div style="display:flex;">    
                            <h5> صاحب المنتج : <span>{{ $seller->user->first_name.' '.$seller->user->last_name }} </span></h5>
                            <input type="hidden" name="seller_id" value="{{$product->sellers->user_id??null }}">
                        </div>
                        <div style="display:flex;">    
                            <h5> كم {{ $product->SubCategory->unit->title??null }} تريد  </span></h5>
                            <input style="width: 40%;
                            text-align: center;
                            font-size: larger;
                            margin-right: 10px;
                            border: 1px solid #f1f1f1;
                            height: 30px;" type="number" name="quantity" value="1" min = "1">
                        </div>
                        <input type="hidden" name="number_store" value="{{$product->number_store+1}}">
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">إغلاق</button>
                    <button type="submit" class="btn btn-primary">شراء المنتج</button>
                    </div>
                </form>
              </div>
            </div>
          </div>
          
    </div>
  
    <script>
      
        

        function Color(index) {
            @if(empty($images))

            @else
                @for ($i = 0 ;  $i <count($images) ; $i++)
                    document.getElementById({{$i}}).style.borderColor = "#666666";
                    document.getElementById({{$i}}).style.borderRadius = "0%";
                @endfor
            @endif
                document.getElementById(index).style.borderColor = "blue";
                document.getElementById(index).style.borderRadius = "15px";
                document.getElementById('image').src = document.getElementById(index).src;
        }
        Color(0);
    </script>
    <br>
</div>
@endsection