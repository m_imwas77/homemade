-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 27, 2020 at 03:16 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `home1`
--

-- --------------------------------------------------------

--
-- Table structure for table `bids`
--

CREATE TABLE `bids` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `price` double NOT NULL,
  `number_of_day` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bids`
--

INSERT INTO `bids` (`id`, `order_id`, `seller_id`, `price`, `number_of_day`, `description`, `created_at`, `updated_at`) VALUES
(14, 83, 6, 22, 22, 'asdasd sd asd asd asd asd asd', '2020-04-25 16:33:55', '2020-04-25 16:33:55');

-- --------------------------------------------------------

--
-- Table structure for table `bindorder`
--

CREATE TABLE `bindorder` (
  `id` int(10) UNSIGNED NOT NULL,
  `seller_id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bindorder`
--

INSERT INTO `bindorder` (`id`, `seller_id`, `order_id`, `status`, `created_at`, `updated_at`) VALUES
(2, 33, 232, 1, '2020-03-23 08:19:13', '2020-03-23 09:25:47'),
(3, 6, 10, 10, '2020-03-23 08:20:19', '2020-03-23 08:20:19'),
(4, 22, 12, 2, '2020-03-23 08:27:35', '2020-03-23 09:27:45'),
(5, 22, 8, 1, '2020-03-23 08:34:29', '2020-03-23 09:27:41'),
(6, 22, 23, 1, '2020-04-07 15:56:41', '2020-04-09 07:20:37'),
(7, 22, 24, 2, '2020-04-07 16:11:45', '2020-04-09 07:18:14'),
(8, 22, 25, 2, '2020-04-09 07:10:57', '2020-04-09 07:11:21'),
(9, 22, 26, 2, '2020-04-09 08:49:55', '2020-04-09 09:02:17'),
(10, 22, 27, 1, '2020-04-09 08:55:01', '2020-04-09 08:59:21'),
(11, 22, 28, 2, '2020-04-09 08:55:57', '2020-04-09 08:59:13'),
(12, 22, 29, 2, '2020-04-09 08:57:51', '2020-04-09 08:58:19'),
(13, 22, 30, 0, '2020-04-09 09:07:24', '2020-04-09 09:07:24'),
(14, 22, 31, 0, '2020-04-09 09:07:54', '2020-04-09 09:07:54'),
(15, 22, 32, 0, '2020-04-09 09:08:26', '2020-04-09 09:08:26'),
(16, 22, 33, 0, '2020-04-09 09:08:52', '2020-04-09 09:08:52'),
(17, 22, 34, 2, '2020-04-09 09:10:33', '2020-04-09 09:10:47'),
(18, 22, 35, 2, '2020-04-09 09:24:50', '2020-04-09 10:03:08'),
(19, 22, 36, 0, '2020-04-11 06:04:49', '2020-04-11 06:04:49'),
(20, 22, 37, 0, '2020-04-11 06:06:10', '2020-04-11 06:06:10'),
(21, 22, 38, 0, '2020-04-11 06:06:31', '2020-04-11 06:06:31'),
(22, 22, 39, 0, '2020-04-11 06:06:44', '2020-04-11 06:06:44'),
(23, 22, 40, 0, '2020-04-11 06:18:45', '2020-04-11 06:18:45'),
(24, 22, 41, 0, '2020-04-11 06:19:07', '2020-04-11 06:19:07'),
(25, 22, 42, 0, '2020-04-11 06:19:53', '2020-04-11 06:19:53'),
(26, 22, 43, 0, '2020-04-11 06:20:31', '2020-04-11 06:20:31'),
(27, 22, 44, 0, '2020-04-11 06:21:09', '2020-04-11 06:21:09'),
(28, 22, 45, 0, '2020-04-11 06:21:40', '2020-04-11 06:21:40'),
(29, 22, 46, 0, '2020-04-11 06:29:21', '2020-04-11 06:29:21'),
(30, 22, 47, 0, '2020-04-11 06:32:59', '2020-04-11 06:32:59'),
(31, 22, 48, 0, '2020-04-11 06:33:25', '2020-04-11 06:33:25'),
(32, 22, 49, 0, '2020-04-11 06:35:14', '2020-04-11 06:35:14'),
(33, 22, 50, 0, '2020-04-11 06:35:32', '2020-04-11 06:35:32'),
(34, 22, 51, 0, '2020-04-11 06:37:00', '2020-04-11 06:37:00'),
(35, 22, 52, 0, '2020-04-11 06:37:14', '2020-04-11 06:37:14'),
(36, 22, 53, 0, '2020-04-11 06:38:17', '2020-04-11 06:38:17'),
(37, 22, 54, 0, '2020-04-11 06:38:54', '2020-04-11 06:38:54'),
(38, 22, 55, 0, '2020-04-11 06:39:11', '2020-04-11 06:39:11'),
(39, 22, 56, 4, '2020-04-11 06:40:04', '2020-04-26 16:29:23'),
(40, 22, 57, 0, '2020-04-11 06:41:45', '2020-04-11 06:41:45'),
(41, 22, 58, 0, '2020-04-11 06:46:39', '2020-04-11 06:46:39'),
(42, 22, 59, 0, '2020-04-11 06:47:56', '2020-04-11 06:47:56'),
(43, 22, 60, 0, '2020-04-11 06:48:38', '2020-04-11 06:48:38'),
(44, 22, 61, 0, '2020-04-11 07:04:01', '2020-04-11 07:04:01'),
(45, 22, 62, 1, '2020-04-11 07:07:48', '2020-04-15 15:11:58'),
(46, 22, 69, 2, '2020-04-15 11:30:53', '2020-04-15 15:11:55'),
(47, 22, 70, 1, '2020-04-15 11:31:45', '2020-04-15 15:11:08'),
(48, 22, 71, 2, '2020-04-15 11:33:00', '2020-04-15 15:11:06'),
(49, 22, 72, 2, '2020-04-15 11:33:42', '2020-04-15 15:10:46'),
(50, 22, 73, 2, '2020-04-15 11:36:43', '2020-04-15 15:10:14'),
(51, 22, 74, 2, '2020-04-15 14:02:36', '2020-04-15 14:10:49'),
(52, 20, 75, 0, '2020-04-18 10:01:16', '2020-04-18 10:01:16'),
(53, 20, 76, 0, '2020-04-20 07:54:09', '2020-04-20 07:54:09'),
(54, 22, 77, 2, '2020-04-20 13:21:05', '2020-04-26 07:02:16'),
(55, 22, 83, 4, '2020-04-25 17:15:36', '2020-04-26 16:34:29'),
(57, 22, 92, 4, '2020-04-25 17:19:30', '2020-04-26 16:38:12');

-- --------------------------------------------------------

--
-- Table structure for table `bind_categories`
--

CREATE TABLE `bind_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `customer_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `user_id`, `created_at`, `updated_at`, `customer_name`) VALUES
(3, 14, '2020-02-05 07:00:00', '2020-02-05 07:01:11', 'Mohammad Qawasmeh'),
(4, 18, '2020-02-11 06:36:00', '2020-02-11 06:37:01', 'mohammad imwas'),
(5, 21, '2020-03-03 06:13:00', '2020-03-24 14:14:40', 'زبون زببين'),
(6, 24, '2020-03-05 08:25:32', '2020-03-05 08:25:32', 'مالك التميمي'),
(7, 25, '2020-03-08 07:15:50', '2020-03-08 07:15:50', 'mohammad qawasmeh');

-- --------------------------------------------------------

--
-- Table structure for table `customer_rate`
--

CREATE TABLE `customer_rate` (
  `id` int(10) UNSIGNED NOT NULL,
  `rate` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customer_rate`
--

INSERT INTO `customer_rate` (`id`, `rate`, `description`, `customer_id`, `seller_id`, `created_at`, `updated_at`) VALUES
(1, 3, 'asdasdas', 5, 22, '2020-04-26 12:20:27', '2020-04-26 12:20:27'),
(2, 3, 'asdasd asd asd as', 5, 22, '2020-04-26 12:22:39', '2020-04-26 12:22:39'),
(3, 1, 'asdasdasd', 5, 22, '2020-04-26 12:26:13', '2020-04-26 12:26:13'),
(4, 1, 'asdsadasdasd', 5, 22, '2020-04-26 12:31:18', '2020-04-26 12:31:18'),
(5, 2, 'asd sd sad asd asd asd asd', 5, 22, '2020-04-26 16:26:43', '2020-04-26 16:26:43'),
(6, 3, 'asdasd a', 5, 22, '2020-04-26 16:29:23', '2020-04-26 16:29:23'),
(7, 2, 'ad sad asd asd asd', 5, 22, '2020-04-26 16:34:29', '2020-04-26 16:34:29'),
(8, 3, 'asd sd asd asd s', 5, 22, '2020-04-26 16:35:26', '2020-04-26 16:35:26');

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `browse` tinyint(1) NOT NULL DEFAULT 1,
  `read` tinyint(1) NOT NULL DEFAULT 1,
  `edit` tinyint(1) NOT NULL DEFAULT 1,
  `add` tinyint(1) NOT NULL DEFAULT 1,
  `delete` tinyint(1) NOT NULL DEFAULT 1,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
(3, 1, 'email', 'text', 'البريد الاكتروني', 0, 1, 1, 1, 1, 1, '{}', 4),
(4, 1, 'password', 'password', 'كلمه المرور', 1, 0, 0, 1, 1, 0, '{}', 8),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, '{}', 9),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, '{}', 18),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 19),
(8, 1, 'avatar', 'image', 'الصوره الشخصيه', 0, 1, 1, 1, 1, 1, '{}', 11),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\",\"taggable\":\"0\"}', 20),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 21),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, '{}', 13),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, '{}', 5),
(21, 1, 'role_id', 'text', 'Role', 0, 0, 1, 1, 1, 1, '{}', 12),
(22, 4, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 0),
(23, 4, 'order_id', 'hidden', 'Order Id', 1, 0, 0, 1, 1, 1, '{}', 3),
(24, 4, 'seller_id', 'hidden', 'Seller Id', 1, 0, 0, 1, 1, 1, '{}', 5),
(25, 4, 'price', 'text', 'السعر', 1, 1, 1, 1, 1, 1, '{}', 6),
(26, 4, 'number_of_day', 'text', 'عدد الايام', 1, 1, 1, 1, 1, 1, '{}', 7),
(27, 4, 'description', 'text', 'التفاصيل', 0, 1, 1, 1, 1, 1, '{}', 8),
(28, 4, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 9),
(29, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 10),
(30, 6, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(31, 6, 'user_id', 'hidden', 'User Id', 0, 0, 0, 1, 1, 1, '{}', 3),
(32, 6, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 4),
(33, 6, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(34, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(35, 7, 'log', 'text', 'Log', 1, 1, 1, 1, 1, 1, '{}', 3),
(36, 7, 'user_id', 'text', 'User Id', 1, 1, 1, 1, 1, 1, '{}', 4),
(37, 7, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(38, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(39, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(40, 8, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(41, 8, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 3),
(42, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(43, 9, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(44, 9, 'message', 'rich_text_box', 'الرساله', 0, 1, 1, 1, 1, 1, '{}', 6),
(45, 9, 'sender_id', 'hidden', 'المرسل', 0, 1, 1, 1, 1, 1, '{}', 3),
(46, 9, 'resever_id', 'hidden', 'Resever Id', 0, 1, 1, 1, 1, 1, '{}', 5),
(47, 9, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 8),
(48, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(49, 10, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(50, 10, 'customer_id', 'hidden', 'Customer Id', 1, 0, 0, 1, 1, 1, '{}', 4),
(51, 10, 'seller_id', 'text', 'Seller Id', 0, 1, 1, 1, 1, 1, '{}', 6),
(52, 10, 'min_price', 'hidden', 'Min Price', 1, 0, 0, 0, 0, 0, '{\"validation\":{\"rule\":\"min:1\",\"messages\":{\"max\":\"  \\u0627\\u0644\\u062d\\u062f \\u0627\\u0644\\u0627\\u062f\\u0646\\u0649 \\u0644\\u0644\\u0633\\u0639\\u0631 :min.\"}}}', 7),
(53, 10, 'max_price', 'number', 'السعر', 1, 1, 1, 1, 1, 1, '{}', 8),
(54, 10, 'final_price', 'hidden', 'Final Price', 0, 0, 0, 0, 0, 0, '{}', 9),
(55, 10, 'start_date', 'date', 'ترايخ بدايه عرض المشروع', 1, 1, 1, 1, 1, 1, '{}', 10),
(56, 10, 'end_date', 'date', 'اخر تاريخ لتلقي العروض', 1, 1, 1, 1, 1, 1, '{}', 11),
(57, 10, 'description', 'hidden', 'الوصف', 0, 0, 0, 0, 0, 0, '{}', 12),
(58, 10, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 14),
(59, 10, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 15),
(60, 11, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 0),
(61, 11, 'order_id', 'text', 'Order Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(63, 11, 'quantity', 'number', 'Quantity', 0, 1, 1, 1, 1, 1, '{}', 6),
(66, 11, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 9),
(67, 11, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 10),
(68, 12, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(69, 12, 'name', 'text', 'اسم المنتج', 1, 1, 1, 1, 1, 1, '{}', 2),
(70, 12, 'price', 'number', 'السعر', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"min:1\",\"messages\":{\"max\":\"  \\u0627\\u0644\\u062d\\u062f \\u0627\\u0644\\u0627\\u062f\\u0646\\u0649 \\u0644\\u0644\\u0633\\u0639\\u0631 :min.\"}}}', 4),
(71, 12, 'available', 'checkbox', 'متوفر', 1, 1, 1, 1, 1, 1, '{\"on\":\"\\u0645\\u062a\\u0648\\u0641\\u0631\",\"off\":\"\\u063a\\u064a\\u0631 \\u0645\\u062a\\u0648\\u0641\\u0631\",\"checked\":true}', 5),
(72, 12, 'sub_category_id', 'hidden', 'Sub Category Id', 1, 1, 1, 1, 1, 1, '{}', 8),
(78, 12, 'seller_id', 'hidden', 'Seller Id', 1, 0, 0, 1, 1, 1, '{}', 10),
(79, 12, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 12),
(80, 12, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 13),
(81, 14, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(82, 14, 'user_id', 'hidden', 'User Id', 1, 0, 0, 1, 1, 1, '{}', 3),
(84, 14, 'story_body', 'rich_text_box', 'Story', 1, 1, 1, 1, 1, 1, '{}', 5),
(88, 14, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 9),
(89, 14, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 10),
(90, 15, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(91, 15, 'title', 'rich_text_box', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(92, 15, 'body', 'rich_text_box', 'Body', 0, 1, 1, 1, 1, 1, '{}', 3),
(93, 15, 'image', 'image', 'Image', 1, 1, 1, 1, 1, 1, '{}', 4),
(94, 15, 'button_title', 'text', 'Button Title', 0, 1, 1, 1, 1, 1, '{}', 5),
(95, 15, 'button_action', 'text', 'Button Action', 0, 1, 1, 1, 1, 1, '{}', 6),
(96, 15, 'View', 'checkbox', 'View', 1, 1, 1, 1, 1, 1, '{\"on\":\"view\",\"off\":\"dont view\"}', 7),
(97, 15, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 9),
(98, 15, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 10),
(99, 16, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(100, 16, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, '{}', 2),
(101, 16, 'main_category_id', 'text', 'Main Category Id', 0, 1, 1, 1, 1, 1, '{}', 4),
(102, 16, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
(103, 16, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(104, 7, 'log_belongsto_user_relationship', 'relationship', 'users', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"first_name\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(106, 9, 'message_belongsto_user_relationship_1', 'relationship', 'المستقبل', 0, 1, 1, 0, 0, 0, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"resever_id\",\"key\":\"id\",\"label\":\"first_name\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":\"0\"}', 4),
(109, 16, 'sub_category_belongsto_main_category_relationship', 'relationship', 'main_categories', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\MainCategory\",\"table\":\"main_categories\",\"type\":\"belongsTo\",\"column\":\"main_category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":\"0\"}', 3),
(110, 12, 'product_belongsto_sub_category_relationship', 'relationship', 'التصنيف', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\SubCategory\",\"table\":\"sub_categories\",\"type\":\"belongsTo\",\"column\":\"sub_category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":\"0\"}', 7),
(112, 11, 'orders_detail_belongsto_product_relationship', 'relationship', 'products', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Product\",\"table\":\"products\",\"type\":\"belongsTo\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":\"0\"}', 4),
(113, 11, 'orders_detail_belongsto_order_relationship', 'relationship', 'orders', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Order\",\"table\":\"orders\",\"type\":\"belongsTo\",\"column\":\"order_id\",\"key\":\"id\",\"label\":\"description\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(114, 10, 'order_belongsto_customer_relationship', 'relationship', 'الزبون', 0, 0, 0, 1, 1, 1, '{\"model\":\"App\\\\Customer\",\"table\":\"customers\",\"type\":\"belongsTo\",\"column\":\"customer_id\",\"key\":\"user_id\",\"label\":\"customer_name\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":\"0\"}', 3),
(115, 10, 'order_belongsto_seller_relationship', 'relationship', 'البائع', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Seller\",\"table\":\"sellers\",\"type\":\"belongsTo\",\"column\":\"seller_id\",\"key\":\"user_id\",\"label\":\"seller_name\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":\"0\"}', 5),
(116, 4, 'bid_belongsto_seller_relationship', 'relationship', 'البائع', 0, 0, 0, 1, 1, 1, '{\"model\":\"App\\\\Seller\",\"table\":\"sellers\",\"type\":\"belongsTo\",\"column\":\"seller_id\",\"key\":\"user_id\",\"label\":\"seller_name\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":\"0\"}', 4),
(117, 4, 'bid_belongsto_order_relationship', 'relationship', 'الطلبيه', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Order\",\"table\":\"orders\",\"type\":\"belongsTo\",\"column\":\"order_id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(118, 23, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
(119, 23, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(120, 23, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 3),
(121, 23, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(122, 1, 'first_name', 'text', 'الاسم الاول', 1, 1, 1, 1, 1, 1, '{}', 2),
(123, 1, 'email_verified_at', 'timestamp', 'Email Verified At', 0, 0, 0, 0, 0, 1, '{}', 10),
(124, 1, 'last_name', 'text', 'اسم العائله', 0, 1, 1, 1, 1, 1, '{}', 3),
(125, 1, 'phone_number', 'text', 'رقم الهاتف', 0, 1, 1, 1, 1, 1, '{}', 6),
(126, 1, 'user_name', 'text', 'اسم المستخدم', 0, 1, 1, 1, 1, 1, '{}', 5),
(127, 1, 'country', 'text', 'الدوله', 0, 0, 1, 1, 1, 1, '{}', 14),
(128, 1, 'city', 'text', 'المدينه', 0, 0, 1, 1, 1, 1, '{}', 15),
(129, 1, 'street', 'text', 'الشارع', 0, 0, 1, 1, 1, 1, '{}', 16),
(130, 1, 'gender', 'select_dropdown', 'الجنس', 0, 0, 1, 1, 1, 1, '{\"default\":\"male\",\"options\":{\"male\":\"male\",\"female\":\"female\"}}', 7),
(131, 1, 'active', 'checkbox', 'Active', 0, 1, 1, 1, 1, 1, '{}', 17),
(132, 6, 'customer_name', 'hidden', 'Customer Name', 0, 0, 0, 1, 1, 1, '{}', 5),
(133, 14, 'seller_name', 'hidden', 'Seller Name', 0, 0, 0, 1, 1, 1, '{}', 6),
(134, 22, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(135, 22, 'rate', 'text', 'Rate', 1, 1, 1, 1, 1, 1, '{}', 2),
(136, 22, 'description', 'text', 'Description', 1, 1, 1, 1, 1, 1, '{}', 3),
(137, 22, 'customer_id', 'text', 'Customer Id', 1, 1, 1, 1, 1, 1, '{}', 4),
(138, 22, 'seller_id', 'text', 'Seller Id', 1, 1, 1, 1, 1, 1, '{}', 5),
(139, 22, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(140, 22, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(142, 10, 'number_of_days', 'number', 'عدد الايام المتوقعه لعمل المشروع', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"min:1\",\"messages\":{\"max\":\" \\u0627\\u0644\\u062d\\u062f \\u0627\\u0644\\u0627\\u062f\\u0646\\u0649 \\u0644\\u0644\\u0627\\u064a\\u0627\\u0645 :min.\"}}}', 13),
(143, 10, 'title', 'text', 'العنوان', 1, 1, 1, 1, 1, 1, '{}', 2),
(144, 10, 'accepted', 'text', 'الموافقه', 0, 1, 1, 1, 1, 1, '{}', 14),
(145, 12, 'images', 'multiple_images', 'الصور', 1, 1, 1, 1, 1, 1, '{}', 6),
(148, 25, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(149, 25, 'customer_id', 'number', 'Customer Id', 0, 0, 0, 1, 1, 1, '{}', 2),
(150, 25, 'type', 'text', 'Type', 0, 0, 0, 1, 1, 1, '{}', 3),
(151, 25, 'type_id', 'number', 'Type Id', 0, 0, 0, 1, 1, 1, '{}', 4),
(152, 25, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 1, '{}', 5),
(153, 25, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(154, 25, 'favorite_belongsto_customer_relationship', 'relationship', 'customers', 0, 0, 0, 0, 0, 1, '{\"model\":\"App\\\\Customer\",\"table\":\"customers\",\"type\":\"belongsTo\",\"column\":\"customer_id\",\"key\":\"user_id\",\"label\":\"customer_name\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":\"0\"}', 7),
(155, 9, 'read', 'hidden', 'مقروئه', 0, 0, 0, 0, 0, 0, '{}', 7),
(156, 9, 'message_belongsto_user_relationship', 'relationship', 'المرسل', 0, 1, 1, 0, 0, 0, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"sender_id\",\"key\":\"id\",\"label\":\"first_name\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(163, 28, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 0),
(164, 28, 'seller_id', 'hidden', 'Seller Id', 0, 1, 1, 1, 1, 1, '{}', 3),
(165, 28, 'order_id', 'hidden', 'Order Id', 0, 1, 1, 1, 1, 1, '{}', 5),
(166, 28, 'status', 'hidden', 'الحاله', 0, 0, 0, 0, 0, 0, '{}', 6),
(167, 28, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 7),
(168, 28, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(169, 28, 'bindorder_belongsto_user_relationship', 'relationship', 'الزبون', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"seller_id\",\"key\":\"id\",\"label\":\"first_name\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(170, 28, 'bindorder_belongsto_order_relationship', 'relationship', 'الطلبيه', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Order\",\"table\":\"orders\",\"type\":\"belongsTo\",\"column\":\"order_id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":\"0\"}', 4),
(171, 29, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(172, 29, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(173, 29, 'user_id', 'hidden', 'User Id', 1, 0, 0, 1, 1, 1, '{}', 3),
(174, 29, 'product_id', 'hidden', 'Product Id', 1, 0, 0, 1, 1, 1, '{}', 5),
(175, 29, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
(176, 29, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(177, 29, 'bind_category_hasone_user_relationship', 'relationship', 'seller', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"first_name\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":\"0\"}', 4),
(178, 29, 'bind_category_hasone_product_relationship', 'relationship', 'product', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Product\",\"table\":\"products\",\"type\":\"belongsTo\",\"column\":\"product_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":\"0\"}', 6),
(179, 12, 'product_hasone_seller_relationship', 'relationship', 'البائع', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Seller\",\"table\":\"sellers\",\"type\":\"belongsTo\",\"column\":\"seller_id\",\"key\":\"user_id\",\"label\":\"seller_name\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":\"0\"}', 9),
(181, 15, 'target', 'checkbox', 'Target', 0, 1, 1, 1, 1, 1, '{\"on\":\"blank\",\"off\":\"home\"}', 8),
(183, 16, 'sub_category_belongsto_unit_relationship', 'relationship', 'units', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Unit\",\"table\":\"units\",\"type\":\"belongsTo\",\"column\":\"unit_id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":\"0\"}', 5),
(184, 16, 'unit_id', 'hidden', 'Unit Id', 0, 1, 1, 1, 1, 1, '{}', 6),
(185, 30, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(186, 30, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(187, 30, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 3),
(188, 30, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(189, 12, 'number_store', 'hidden', 'Number Store', 0, 0, 0, 0, 0, 1, '{}', 11),
(191, 11, 'sub_category_id', 'text', 'Sub Category Id', 0, 1, 1, 1, 1, 1, '{}', 3),
(192, 11, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, '{}', 6),
(193, 11, 'desc', 'text', 'Desc', 0, 1, 1, 1, 1, 1, '{}', 7),
(194, 12, 'description', 'text_area', 'وصف المنتج', 0, 0, 1, 1, 1, 1, '{}', 3),
(195, 31, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(196, 31, 'rate', 'text', 'Rate', 1, 1, 1, 1, 1, 1, '{}', 2),
(197, 31, 'description', 'text', 'Description', 1, 1, 1, 1, 1, 1, '{}', 3),
(198, 31, 'customer_id', 'text', 'Customer Id', 1, 1, 1, 1, 1, 1, '{}', 4),
(199, 31, 'seller_id', 'text', 'Seller Id', 1, 1, 1, 1, 1, 1, '{}', 5),
(200, 31, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(201, 31, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(202, 25, 'favorite_belongsto_product_relationship', 'relationship', 'اسم المنتج', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Product\",\"table\":\"products\",\"type\":\"belongsTo\",\"column\":\"type_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":\"0\"}', 8);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT 0,
  `server_side` tinyint(4) NOT NULL DEFAULT 0,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'App\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":\"users\"}', '2019-12-15 13:02:12', '2020-04-22 07:57:31'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2019-12-15 13:02:12', '2020-02-05 06:35:11'),
(4, 'bids', 'bids', 'عطائات', 'عطائات', 'voyager-wallet', 'App\\Bid', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":\"bids\"}', '2019-12-15 15:19:31', '2020-03-23 11:22:06'),
(6, 'customers', 'customers', 'معلومات الحساب', 'معلومات الحساب', NULL, 'App\\Customer', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":\"customers\"}', '2019-12-15 15:20:26', '2020-03-25 15:11:17'),
(7, 'logs', 'logs', 'Log', 'Logs', NULL, 'App\\Log', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-12-15 15:20:32', '2020-02-05 06:34:07'),
(8, 'main_categories', 'main-categories', 'Main Category', 'Main Categories', NULL, 'App\\MainCategory', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-12-15 15:20:40', '2020-02-05 06:34:15'),
(9, 'messages', 'messages', 'رساله', 'الرسائل', NULL, 'App\\Message', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":\"messages\"}', '2019-12-15 15:21:04', '2020-03-23 13:52:34'),
(10, 'orders', 'orders', 'طلبيه', 'طلباتي', NULL, 'App\\Order', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":\"orders\"}', '2019-12-15 15:21:44', '2020-03-30 04:03:21'),
(11, 'orders_details', 'orders-details', 'Orders Detail', 'Orders Details', NULL, 'App\\OrdersDetail', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":\"orderDetails\"}', '2019-12-15 15:21:52', '2020-04-20 07:46:37'),
(12, 'products', 'products', 'منتج', 'المنتجات', NULL, 'App\\Product', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":\"products\"}', '2019-12-15 15:22:03', '2020-04-20 07:59:14'),
(14, 'sellers', 'sellers', 'معلومات الحساب', 'معلومات الحساب', NULL, 'App\\Seller', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":\"sellers\"}', '2019-12-15 15:22:14', '2020-03-24 14:18:48'),
(15, 'sliders', 'sliders', 'Slider', 'Sliders', NULL, 'App\\Slider', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-12-15 15:22:38', '2020-03-30 12:11:36'),
(16, 'sub_categories', 'sub-categories', 'Sub Category', 'Sub Categories', NULL, 'App\\SubCategory', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-12-15 15:22:42', '2020-04-18 09:31:28'),
(22, 'customer_rate', 'customer-rate', 'Customer Rate', 'Customer Rates', NULL, 'App\\CustomerRate', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-12-15 17:48:19', '2020-04-26 16:37:06'),
(23, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, 'App\\Http\\Controllers\\Voyager\\VoyagerBaseController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2020-01-31 05:05:56', '2020-03-14 09:12:26'),
(25, 'favorites', 'favorites', 'Favorite', 'Favorites', NULL, 'App\\Favorite', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":\"favorite\"}', '2020-03-11 06:28:27', '2020-04-27 06:36:17'),
(26, 'seller_order', 'seller-order', 'Seller Order', 'Seller Orders', NULL, 'App\\SellerOrder', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-03-23 08:01:06', '2020-03-23 08:01:06'),
(28, 'bindorder', 'bindorder', 'طلياتي', 'طلباتي', NULL, 'App\\Bindorder', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":\"user\"}', '2020-03-23 08:08:11', '2020-03-23 08:43:43'),
(29, 'bind_categories', 'bind-categories', 'Bind Category', 'Bind Categories', NULL, 'App\\BindCategory', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-03-26 09:22:24', '2020-03-26 09:32:40'),
(30, 'units', 'units', 'Unit', 'Units', NULL, 'App\\Unit', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-04-18 09:32:04', '2020-04-18 09:32:04'),
(31, 'seller_rate', 'seller-rate', 'Seller Rate', 'Seller Rates', NULL, 'App\\SellerRate', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-04-26 16:37:03', '2020-04-26 16:37:03');

-- --------------------------------------------------------

--
-- Table structure for table `favorites`
--

CREATE TABLE `favorites` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `favorites`
--

INSERT INTO `favorites` (`id`, `customer_id`, `type`, `type_id`, `created_at`, `updated_at`) VALUES
(16, 21, 'products', 13, '2020-04-27 06:36:40', '2020-04-27 06:36:40');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `log` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `main_categories`
--

CREATE TABLE `main_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `main_categories`
--

INSERT INTO `main_categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(7, 'الصنف الثاني', '2020-02-06 04:59:00', '2020-03-05 08:03:40'),
(8, 'الصنف الاول', '2020-02-07 11:04:00', '2020-03-05 08:03:02'),
(9, 'it new from bind just to test', '2020-03-26 15:02:52', '2020-03-26 15:02:52'),
(10, 'asdtest', '2020-03-26 17:47:02', '2020-03-26 17:47:02'),
(11, 'asdtestasd', '2020-03-26 17:50:15', '2020-03-26 17:50:15'),
(12, 'Q54WE', '2020-03-26 17:54:01', '2020-03-26 17:54:01');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2019-12-15 13:02:13', '2019-12-15 13:02:13'),
(2, 'seller', '2019-12-15 13:02:13', '2019-12-15 13:02:13'),
(3, 'customer', '2019-12-15 13:02:13', '2019-12-15 13:02:13');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2019-12-15 13:02:13', '2019-12-15 13:02:13', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 4, '2019-12-15 13:02:13', '2020-02-05 06:40:53', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2019-12-15 13:02:13', '2019-12-15 13:02:13', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2019-12-15 13:02:13', '2019-12-15 13:02:13', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 5, '2019-12-15 13:02:13', '2020-02-05 06:40:53', NULL, NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2019-12-15 13:02:13', '2020-02-05 06:40:53', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2019-12-15 13:02:13', '2020-02-05 06:40:53', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2019-12-15 13:02:13', '2020-02-05 06:40:53', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 10, '2019-12-15 13:02:13', '2020-02-05 06:42:13', 'voyager.settings.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2019-12-15 13:02:17', '2020-02-05 06:40:53', 'voyager.hooks', NULL),
(12, 1, 'Bids', '', '_self', 'voyager-megaphone', '#000000', NULL, 11, '2019-12-15 15:19:32', '2020-02-05 06:42:11', 'voyager.bids.index', 'null'),
(14, 1, 'Customers', '', '_self', 'voyager-person', '#000000', NULL, 6, '2019-12-15 15:20:26', '2020-02-05 06:41:54', 'voyager.customers.index', 'null'),
(15, 1, 'Logs', '', '_self', 'voyager-logbook', '#000000', NULL, 12, '2019-12-15 15:20:32', '2020-02-05 06:42:11', 'voyager.logs.index', 'null'),
(16, 1, 'Main Categories', '', '_self', 'voyager-categories', '#000000', NULL, 8, '2019-12-15 15:20:40', '2020-02-05 06:42:05', 'voyager.main-categories.index', 'null'),
(17, 1, 'Messages', '', '_self', 'voyager-chat', '#000000', NULL, 13, '2019-12-15 15:21:05', '2020-02-05 06:42:11', 'voyager.messages.index', 'null'),
(18, 1, 'Orders', '', '_self', 'voyager-basket', '#000000', NULL, 14, '2019-12-15 15:21:44', '2020-02-05 06:42:11', 'voyager.orders.index', 'null'),
(19, 1, 'Orders Details', '', '_self', 'voyager-basket', '#000000', NULL, 15, '2019-12-15 15:21:52', '2020-02-05 06:42:11', 'voyager.orders-details.index', 'null'),
(20, 1, 'Products', '', '_self', 'voyager-truck', '#000000', NULL, 16, '2019-12-15 15:22:04', '2020-02-05 06:42:11', 'voyager.products.index', 'null'),
(22, 1, 'Sellers', '', '_self', 'voyager-person', '#000000', NULL, 7, '2019-12-15 15:22:14', '2020-02-05 06:41:58', 'voyager.sellers.index', 'null'),
(23, 1, 'Sliders', '', '_self', 'voyager-new', '#000000', NULL, 17, '2019-12-15 15:22:38', '2020-02-05 06:42:11', 'voyager.sliders.index', 'null'),
(24, 1, 'Sub Categories', '', '_self', 'voyager-categories', '#000000', NULL, 9, '2019-12-15 15:22:43', '2020-02-05 06:42:13', 'voyager.sub-categories.index', 'null'),
(30, 1, 'Customer Rates', '', '_self', 'voyager-star-two', '#000000', NULL, 18, '2019-12-15 17:48:19', '2020-02-05 06:40:53', 'voyager.customer-rate.index', 'null'),
(31, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2020-01-31 05:05:56', '2020-02-05 06:40:53', 'voyager.menus.index', NULL),
(32, 1, 'Favorites', '', '_self', NULL, NULL, NULL, 19, '2020-03-11 06:28:27', '2020-03-11 06:28:27', 'voyager.favorites.index', NULL),
(34, 2, 'test', '', '_self', NULL, '#000000', 33, 1, '2020-03-14 09:15:15', '2020-03-14 09:15:18', NULL, ''),
(39, 2, 'الرسائل', 'admin/messages', '_self', 'voyager-mail', '#000000', NULL, 2, '2020-03-21 11:28:37', '2020-03-21 11:35:19', NULL, ''),
(40, 2, 'المنتجات', 'admin/products', '_self', NULL, '#000000', NULL, 3, '2020-03-21 11:32:53', '2020-03-21 11:35:19', NULL, ''),
(41, 2, 'الرئيسيه', '/admin', '_self', NULL, '#000000', NULL, 1, '2020-03-21 11:35:13', '2020-03-21 11:35:19', NULL, ''),
(42, 2, 'عطائاتي', '/admin/bids', '_self', NULL, '#000000', NULL, 20, '2020-03-21 11:37:38', '2020-03-23 11:21:15', NULL, ''),
(43, 1, 'Seller Orders', '', '_self', NULL, NULL, NULL, 21, '2020-03-23 08:01:06', '2020-03-23 08:01:06', 'voyager.seller-order.index', NULL),
(44, 2, 'طلبياتي', 'admin/bindorder', '_self', NULL, '#000000', NULL, 22, '2020-03-23 08:02:57', '2020-03-23 08:13:12', NULL, ''),
(46, 1, 'Bindorders', '', '_self', NULL, NULL, NULL, 23, '2020-03-23 08:08:11', '2020-03-23 08:08:11', 'voyager.bindorder.index', NULL),
(47, 3, 'الرئيسيه', 'admin', '_self', NULL, '#000000', NULL, 24, '2020-03-24 05:15:30', '2020-03-24 05:15:30', NULL, ''),
(48, 3, 'الرسائل', 'admin/messages', '_self', NULL, '#000000', NULL, 25, '2020-03-24 05:15:47', '2020-03-24 05:15:47', NULL, ''),
(49, 3, 'طلبياتي', 'admin/orders', '_self', NULL, '#000000', NULL, 26, '2020-03-24 05:16:23', '2020-03-24 05:16:23', NULL, ''),
(50, 1, 'Bind Categories', '', '_self', NULL, NULL, NULL, 27, '2020-03-26 09:22:24', '2020-03-26 09:22:24', 'voyager.bind-categories.index', NULL),
(51, 1, 'Units', '', '_self', NULL, NULL, NULL, 28, '2020-04-18 09:32:05', '2020-04-18 09:32:05', 'voyager.units.index', NULL),
(52, 1, 'Seller Rates', '', '_self', NULL, NULL, NULL, 29, '2020-04-26 16:37:03', '2020-04-26 16:37:03', 'voyager.seller-rate.index', NULL),
(53, 3, 'المفضله', 'admin/favorites', '_self', NULL, '#000000', NULL, 30, '2020-04-27 06:15:40', '2020-04-27 06:15:40', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sender_id` bigint(20) DEFAULT NULL,
  `resever_id` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `read` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `message`, `sender_id`, `resever_id`, `created_at`, `updated_at`, `read`) VALUES
(91, '<p>sd asd asd asd asd asd asd asd asd asd as</p>', 1, 21, '2020-04-27 06:44:40', '2020-04-27 06:44:40', 1),
(93, '<p>sada sdas asd as</p>', 22, 21, '2020-04-27 07:22:34', '2020-04-27 07:22:34', 1),
(94, '<p>الحمد لله</p>', 21, 22, '2020-04-27 08:20:05', '2020-04-27 08:20:05', 0),
(95, '<p>ثهضعقرتمنضثقتلاض ق تضصقضص</p>', 21, 1, '2020-04-27 08:23:31', '2020-04-27 08:23:31', 0);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` bigint(20) NOT NULL,
  `seller_id` bigint(20) DEFAULT NULL,
  `min_price` double DEFAULT NULL,
  `max_price` double NOT NULL,
  `final_price` double DEFAULT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `number_of_days` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `accepted` int(11) DEFAULT 0,
  `in_progress` int(11) DEFAULT 0,
  `completed` int(11) DEFAULT 0,
  `rejected` int(11) DEFAULT 0,
  `done_me` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `customer_id`, `seller_id`, `min_price`, `max_price`, `final_price`, `start_date`, `end_date`, `created_at`, `updated_at`, `number_of_days`, `title`, `description`, `accepted`, `in_progress`, `completed`, `rejected`, `done_me`) VALUES
(35, 5, 22, 123, 123, 123, '2020-04-09', '2020-04-09', '2020-04-09 09:24:50', '2020-04-09 10:03:08', 10, 'AS', NULL, 1, 0, 0, 1, 0),
(36, 5, 22, 123, 123, 123, '2020-04-11', '2020-04-11', '2020-04-11 06:04:49', '2020-04-11 06:04:49', 10, 'AS', NULL, 1, 0, 0, 0, 0),
(37, 5, 22, 123, 123, 123, '2020-04-11', '2020-04-11', '2020-04-11 06:06:10', '2020-04-11 06:06:10', 10, 'AS', NULL, 1, 0, 0, 0, 0),
(38, 5, 22, 123, 123, 123, '2020-04-11', '2020-04-11', '2020-04-11 06:06:31', '2020-04-11 06:06:31', 10, 'AS', NULL, 1, 0, 0, 0, 0),
(39, 5, 22, 123, 123, 123, '2020-04-11', '2020-04-11', '2020-04-11 06:06:44', '2020-04-11 06:06:44', 10, 'AS', NULL, 1, 0, 0, 0, 0),
(40, 5, 22, 123, 123, 123, '2020-04-11', '2020-04-11', '2020-04-11 06:18:45', '2020-04-11 06:18:45', 10, 'AS', NULL, 1, 0, 0, 0, 0),
(41, 5, 22, 123, 123, 123, '2020-04-11', '2020-04-11', '2020-04-11 06:19:07', '2020-04-11 06:19:07', 10, 'AS', NULL, 1, 0, 0, 0, 0),
(42, 5, 22, 123, 123, 123, '2020-04-11', '2020-04-11', '2020-04-11 06:19:53', '2020-04-11 06:19:53', 10, 'AS', NULL, 1, 0, 0, 0, 0),
(43, 5, 22, 123, 123, 123, '2020-04-11', '2020-04-11', '2020-04-11 06:20:31', '2020-04-11 06:20:31', 10, 'AS', NULL, 1, 0, 0, 0, 0),
(44, 5, 22, 123, 123, 123, '2020-04-11', '2020-04-11', '2020-04-11 06:21:09', '2020-04-11 06:21:09', 10, 'AS', NULL, 1, 0, 0, 0, 0),
(45, 5, 22, 123, 123, 123, '2020-04-11', '2020-04-11', '2020-04-11 06:21:40', '2020-04-11 06:21:40', 10, 'AS', NULL, 1, 0, 0, 0, 0),
(46, 5, 22, 123, 123, 123, '2020-04-11', '2020-04-11', '2020-04-11 06:29:21', '2020-04-11 06:29:21', 10, 'AS', NULL, 1, 0, 0, 0, 0),
(47, 5, 22, 123, 123, 123, '2020-04-11', '2020-04-11', '2020-04-11 06:32:59', '2020-04-11 06:32:59', 10, 'AS', NULL, 1, 0, 0, 0, 0),
(48, 5, 22, 123, 123, 123, '2020-04-11', '2020-04-11', '2020-04-11 06:33:25', '2020-04-11 06:33:25', 10, 'AS', NULL, 1, 0, 0, 0, 0),
(49, 5, 22, 123, 123, 123, '2020-04-11', '2020-04-11', '2020-04-11 06:35:13', '2020-04-11 06:35:13', 10, 'AS', NULL, 1, 0, 0, 0, 0),
(50, 5, 22, 123, 123, 123, '2020-04-11', '2020-04-11', '2020-04-11 06:35:32', '2020-04-11 06:35:32', 10, 'AS', NULL, 1, 0, 0, 0, 0),
(51, 5, 22, 123, 123, 123, '2020-04-11', '2020-04-11', '2020-04-11 06:37:00', '2020-04-11 06:37:00', 10, 'AS', NULL, 1, 0, 0, 0, 0),
(52, 5, 22, 123, 123, 123, '2020-04-11', '2020-04-11', '2020-04-11 06:37:14', '2020-04-11 06:37:14', 10, 'AS', NULL, 1, 0, 0, 0, 0),
(53, 5, 22, 123, 123, 123, '2020-04-11', '2020-04-11', '2020-04-11 06:38:17', '2020-04-11 06:38:17', 10, 'AS', NULL, 1, 0, 0, 0, 0),
(54, 5, 22, 123, 123, 123, '2020-04-11', '2020-04-11', '2020-04-11 06:38:54', '2020-04-11 06:38:54', 10, 'AS', NULL, 1, 0, 0, 0, 0),
(55, 5, 22, 123, 123, 123, '2020-04-11', '2020-04-11', '2020-04-11 06:39:11', '2020-04-11 06:39:11', 10, 'AS', NULL, 1, 0, 0, 0, 0),
(56, 5, 22, 123, 123, 123, '2020-04-11', '2020-04-11', '2020-04-11 06:40:04', '2020-04-11 06:40:04', 10, 'AS', NULL, 1, 0, 0, 0, 0),
(57, 5, 22, 123, 123, 123, '2020-04-11', '2020-04-11', '2020-04-11 06:41:45', '2020-04-11 06:41:45', 10, 'AS', NULL, 1, 0, 0, 0, 0),
(58, 5, 22, 123, 123, 123, '2020-04-11', '2020-04-11', '2020-04-11 06:46:39', '2020-04-11 06:46:39', 10, 'AS', NULL, 1, 0, 0, 0, 0),
(59, 5, 22, 123, 123, 123, '2020-04-11', '2020-04-11', '2020-04-11 06:47:56', '2020-04-11 06:47:56', 10, 'AS', NULL, 1, 0, 0, 0, 0),
(60, 5, 22, 123, 123, 123, '2020-04-11', '2020-04-11', '2020-04-11 06:48:38', '2020-04-11 06:48:38', 10, 'AS', NULL, 1, 0, 0, 0, 0),
(61, 5, 22, 123, 123, 123, '2020-04-11', '2020-04-11', '2020-04-11 07:04:01', '2020-04-11 07:04:01', 10, 'AS', NULL, 1, 0, 0, 0, 0),
(62, 5, 22, 123, 123, 123, '2020-04-11', '2020-04-11', '2020-04-11 07:07:48', '2020-04-15 15:11:58', 10, 'AS', NULL, 1, 1, 0, 0, 0),
(69, 5, 22, 123, 123, 123, '2020-04-15', '2020-04-15', '2020-04-15 11:30:53', '2020-04-15 15:11:55', 10, 'AS', NULL, 1, 0, 0, 1, 0),
(70, 5, 22, 123, 123, 123, '2020-04-15', '2020-04-15', '2020-04-15 11:31:45', '2020-04-15 15:11:08', 10, 'AS', NULL, 1, 1, 0, 0, 0),
(71, 5, 22, 123, 123, 123, '2020-04-15', '2020-04-15', '2020-04-15 11:33:00', '2020-04-15 15:11:05', 10, 'AS', NULL, 1, 0, 0, 1, 0),
(72, 5, 22, 123, 123, 123, '2020-04-15', '2020-04-15', '2020-04-15 11:33:42', '2020-04-15 15:10:46', 10, 'AS', NULL, 1, 0, 0, 1, 0),
(73, 5, 22, 123, 123, 123, '2020-04-15', '2020-04-15', '2020-04-15 11:36:43', '2020-04-15 15:10:14', 10, 'AS', NULL, 1, 0, 0, 1, 0),
(74, 5, 22, 123, 123, 123, '2020-04-15', '2020-04-15', '2020-04-15 14:02:36', '2020-04-15 14:10:49', 10, 'AS', NULL, 1, 0, 0, 1, 0),
(75, 5, 20, 5, 5, 5, '2020-04-18', '2020-04-18', '2020-04-18 10:01:16', '2020-04-18 10:01:16', 10, 'سبانخ', NULL, 1, 0, 0, 0, 0),
(76, 5, 20, 5, 5, 5, '2020-04-20', '2020-04-20', '2020-04-20 07:54:09', '2020-04-20 07:54:09', 10, 'سبانخ', NULL, 1, 0, 0, 0, 0),
(77, 5, 22, 123, 123, 123, '2020-04-20', '2020-04-20', '2020-04-20 13:21:05', '2020-04-26 07:02:16', 10, 'AS', NULL, 1, 0, 0, 1, 0),
(78, 5, -1, NULL, 123, NULL, '2020-04-25', '2020-04-25', '2020-04-25 10:04:40', '2020-04-25 10:04:40', 1, 'asd', NULL, 1, 0, 0, 0, 0),
(79, 5, -1, NULL, 120, NULL, '2020-04-25', '2020-04-25', '2020-04-25 10:23:25', '2020-04-25 10:23:25', 123, 'asd', NULL, 1, 0, 0, 0, 0),
(80, 5, -1, NULL, 120, NULL, '2020-04-25', '2020-04-25', '2020-04-25 10:23:47', '2020-04-25 10:23:47', 123, 'asd', NULL, 1, 0, 0, 0, 0),
(81, 5, -1, NULL, 123, NULL, '2020-04-25', '2020-04-25', '2020-04-25 10:27:35', '2020-04-25 10:27:35', 123, 'asd', NULL, 1, 0, 0, 0, 0),
(82, 5, -1, NULL, 123, NULL, '2020-04-25', '2020-04-25', '2020-04-25 12:09:09', '2020-04-25 12:09:09', 123, 'sad', NULL, 1, 0, 0, 0, 0),
(83, 5, 22, NULL, 12, NULL, '2020-04-17', '2020-04-30', '2020-04-25 15:36:58', '2020-04-26 11:48:35', 12, 'اخر طلبيه', NULL, 1, 1, 1, 0, 1),
(84, 5, -1, NULL, 2, NULL, '2020-04-25', '2020-04-25', '2020-04-25 15:47:20', '2020-04-25 15:47:20', 12, 'asd', NULL, 1, 0, 0, 0, 0),
(85, 5, -1, NULL, 2, NULL, '2020-04-29', '2020-04-30', '2020-04-25 15:48:06', '2020-04-25 15:48:06', 8, 'asd', NULL, NULL, 0, 0, 0, 0),
(86, 5, -1, NULL, 2, NULL, '2020-04-25', '2020-04-25', '2020-04-25 15:48:20', '2020-04-25 15:48:20', 8, 'asd', NULL, 1, 0, 0, 0, 0),
(87, 5, -1, NULL, 2, NULL, '2020-04-21', '2020-04-29', '2020-04-25 15:48:43', '2020-04-25 15:48:43', 12, 'as', NULL, NULL, 0, 0, 0, 0),
(88, 5, -1, NULL, 2, NULL, '2020-04-21', '2020-04-29', '2020-04-25 15:49:01', '2020-04-25 15:49:01', 12, 'as', NULL, NULL, 0, 0, 0, 0),
(89, 5, -1, NULL, 2, NULL, '2020-04-21', '2020-04-29', '2020-04-25 15:49:07', '2020-04-25 15:49:07', 12, 'as', NULL, NULL, 0, 0, 0, 0),
(90, 5, -1, NULL, 2, NULL, '2020-04-21', '2020-04-29', '2020-04-25 15:49:15', '2020-04-25 15:49:15', 12, 'as', NULL, NULL, 0, 0, 0, 0),
(91, 5, -1, NULL, 2, NULL, '2020-04-21', '2020-04-29', '2020-04-25 15:49:23', '2020-04-25 15:49:23', 12, 'as', NULL, 0, 0, 0, 0, 0),
(92, 5, 22, 123, 123, 123, '2020-04-25', '2020-04-25', '2020-04-25 17:19:30', '2020-04-26 11:50:56', 10, 'AS', NULL, 1, 1, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `orders_details`
--

CREATE TABLE `orders_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `sub_category_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders_details`
--

INSERT INTO `orders_details` (`id`, `order_id`, `sub_category_id`, `created_at`, `updated_at`, `name`, `desc`, `quantity`) VALUES
(3, 5, 2, '2020-02-12 16:37:36', '2020-02-12 16:41:01', '13123', 'asdasd12312312312', NULL),
(4, 6, 1, '2020-02-18 16:26:12', '2020-02-18 16:26:12', 'مقلوبه', 'مقلوبه مع جاد', NULL),
(5, 6, 3, '2020-02-18 16:26:12', '2020-02-18 16:26:12', 'بيتزا', 'بيتزا حجم صغبر بعنب سناك و اشي زي هيك', NULL),
(6, 8, 1, '2020-03-03 06:18:40', '2020-03-03 06:18:40', 'المنتج الاول', 'لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود تيمبور\r\n\r\nأنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا . يوت انيم أد مينيم فينايم,', NULL),
(7, 8, 2, '2020-03-03 06:18:40', '2020-03-03 06:18:40', 'النتج الثاني', 'لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود تيمبور\r\n\r\nأنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا . يوت انيم أد مينيم فينايم,', NULL),
(8, 8, 3, '2020-03-03 06:18:40', '2020-03-03 06:18:40', 'المنتج الثاني', 'لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود تيمبور\r\n\r\nأنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا . يوت انيم أد مينيم فينايم,', NULL),
(9, 8, 11, '2020-03-03 06:18:40', '2020-03-03 06:18:40', 'المنتج الرابع', 'لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود تيمبور\r\n\r\nأنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا . يوت انيم أد مينيم فينايم,', NULL),
(10, 10, 2, '2020-03-03 08:29:00', '2020-03-03 08:29:00', 'qwewqe', 'wqe wqe qwe qweqw eqwe qweqw', NULL),
(11, 11, 2, '2020-03-05 08:37:47', '2020-03-05 08:37:47', 'سفيحه', 'سفيحه بالطحينه', NULL),
(12, 24, 24, '2020-04-07 16:11:45', '2020-04-07 16:11:45', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(13, 25, 24, '2020-04-09 07:10:57', '2020-04-09 07:10:57', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(14, 26, 24, '2020-04-09 08:49:55', '2020-04-09 08:49:55', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(15, 27, 24, '2020-04-09 08:55:01', '2020-04-09 08:55:01', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(16, 28, 24, '2020-04-09 08:55:57', '2020-04-09 08:55:57', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(17, 29, 24, '2020-04-09 08:57:51', '2020-04-09 08:57:51', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(18, 30, 24, '2020-04-09 09:07:24', '2020-04-09 09:07:24', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(19, 31, 24, '2020-04-09 09:07:54', '2020-04-09 09:07:54', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(20, 32, 24, '2020-04-09 09:08:26', '2020-04-09 09:08:26', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(21, 33, 24, '2020-04-09 09:08:52', '2020-04-09 09:08:52', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(22, 34, 24, '2020-04-09 09:10:33', '2020-04-09 09:10:33', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(23, 35, 24, '2020-04-09 09:24:50', '2020-04-09 09:24:50', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(24, 36, 24, '2020-04-11 06:04:49', '2020-04-11 06:04:49', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(25, 37, 24, '2020-04-11 06:06:10', '2020-04-11 06:06:10', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(26, 38, 24, '2020-04-11 06:06:31', '2020-04-11 06:06:31', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(27, 39, 24, '2020-04-11 06:06:44', '2020-04-11 06:06:44', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(28, 40, 24, '2020-04-11 06:18:45', '2020-04-11 06:18:45', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(29, 41, 24, '2020-04-11 06:19:07', '2020-04-11 06:19:07', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(30, 42, 24, '2020-04-11 06:19:53', '2020-04-11 06:19:53', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(31, 43, 24, '2020-04-11 06:20:31', '2020-04-11 06:20:31', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(32, 44, 24, '2020-04-11 06:21:09', '2020-04-11 06:21:09', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(33, 45, 24, '2020-04-11 06:21:40', '2020-04-11 06:21:40', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(34, 46, 24, '2020-04-11 06:29:21', '2020-04-11 06:29:21', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(35, 47, 24, '2020-04-11 06:32:59', '2020-04-11 06:32:59', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(36, 48, 24, '2020-04-11 06:33:25', '2020-04-11 06:33:25', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(37, 49, 24, '2020-04-11 06:35:14', '2020-04-11 06:35:14', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(38, 50, 24, '2020-04-11 06:35:32', '2020-04-11 06:35:32', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(39, 51, 24, '2020-04-11 06:37:00', '2020-04-11 06:37:00', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(40, 52, 24, '2020-04-11 06:37:14', '2020-04-11 06:37:14', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(41, 53, 24, '2020-04-11 06:38:17', '2020-04-11 06:38:17', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(42, 54, 24, '2020-04-11 06:38:54', '2020-04-11 06:38:54', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(43, 55, 24, '2020-04-11 06:39:11', '2020-04-11 06:39:11', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(44, 56, 24, '2020-04-11 06:40:04', '2020-04-11 06:40:04', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(45, 57, 24, '2020-04-11 06:41:45', '2020-04-11 06:41:45', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(46, 58, 24, '2020-04-11 06:46:39', '2020-04-11 06:46:39', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(47, 59, 24, '2020-04-11 06:47:56', '2020-04-11 06:47:56', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(48, 60, 24, '2020-04-11 06:48:38', '2020-04-11 06:48:38', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(49, 61, 24, '2020-04-11 07:04:01', '2020-04-11 07:04:01', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(50, 62, 24, '2020-04-11 07:07:48', '2020-04-11 07:07:48', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(54, 68, 1, '2020-04-11 12:46:18', '2020-04-11 12:46:18', 'asd', 'asd', NULL),
(55, 69, 24, '2020-04-15 11:30:53', '2020-04-15 11:30:53', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(56, 70, 24, '2020-04-15 11:31:45', '2020-04-15 11:31:45', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(57, 71, 24, '2020-04-15 11:33:00', '2020-04-15 11:33:00', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(58, 72, 24, '2020-04-15 11:33:42', '2020-04-15 11:33:42', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(59, 73, 24, '2020-04-15 11:36:43', '2020-04-15 11:36:43', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(60, 74, 24, '2020-04-15 14:02:36', '2020-04-15 14:02:36', 'AS', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(61, 75, 2, '2020-04-18 10:01:16', '2020-04-18 10:01:16', 'سبانخ', 'اريد عمل هذا المنتج كما هو موضح', NULL),
(62, 76, 2, '2020-04-20 07:54:09', '2020-04-20 07:54:09', 'سبانخ', NULL, 30),
(63, 77, 24, '2020-04-20 13:21:05', '2020-04-20 13:21:05', 'AS', 'as ad sd asd asd asd asdasdas dasdas sdgfdgg egr ewf sfdgregdsf sgsd  daf ', 13),
(64, 81, 1, '2020-04-25 10:27:35', '2020-04-25 10:27:35', 'asd', 'asd', 1),
(65, 82, 18, '2020-04-25 12:09:09', '2020-04-25 12:09:09', 'asd', 'asd', 4),
(66, 82, 19, '2020-04-25 12:12:42', '2020-04-25 12:12:42', 'asd', 'asd', 123),
(67, 83, 21, '2020-04-25 15:36:58', '2020-04-25 15:36:58', 'شسي', 'شسي', 1),
(68, 83, 19, '2020-04-25 15:36:58', '2020-04-25 15:36:58', 'شسي', 'شسي', 5),
(69, 92, 24, '2020-04-25 17:19:30', '2020-04-25 17:19:30', 'AS', 'as ad sd asd asd asd asdasdas dasdas sdgfdgg egr ewf sfdgregdsf sgsd  daf ', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2019-12-15 13:02:14', '2019-12-15 13:02:14'),
(2, 'browse_bread', NULL, '2019-12-15 13:02:14', '2019-12-15 13:02:14'),
(3, 'browse_database', NULL, '2019-12-15 13:02:14', '2019-12-15 13:02:14'),
(4, 'browse_media', NULL, '2019-12-15 13:02:14', '2019-12-15 13:02:14'),
(5, 'browse_compass', NULL, '2019-12-15 13:02:14', '2019-12-15 13:02:14'),
(11, 'browse_roles', 'roles', '2019-12-15 13:02:14', '2019-12-15 13:02:14'),
(12, 'read_roles', 'roles', '2019-12-15 13:02:14', '2019-12-15 13:02:14'),
(13, 'edit_roles', 'roles', '2019-12-15 13:02:14', '2019-12-15 13:02:14'),
(14, 'add_roles', 'roles', '2019-12-15 13:02:14', '2019-12-15 13:02:14'),
(15, 'delete_roles', 'roles', '2019-12-15 13:02:14', '2019-12-15 13:02:14'),
(16, 'browse_users', 'users', '2019-12-15 13:02:14', '2019-12-15 13:02:14'),
(17, 'read_users', 'users', '2019-12-15 13:02:15', '2019-12-15 13:02:15'),
(18, 'edit_users', 'users', '2019-12-15 13:02:15', '2019-12-15 13:02:15'),
(19, 'add_users', 'users', '2019-12-15 13:02:15', '2019-12-15 13:02:15'),
(20, 'delete_users', 'users', '2019-12-15 13:02:15', '2019-12-15 13:02:15'),
(21, 'browse_settings', 'settings', '2019-12-15 13:02:15', '2019-12-15 13:02:15'),
(22, 'read_settings', 'settings', '2019-12-15 13:02:15', '2019-12-15 13:02:15'),
(23, 'edit_settings', 'settings', '2019-12-15 13:02:15', '2019-12-15 13:02:15'),
(24, 'add_settings', 'settings', '2019-12-15 13:02:15', '2019-12-15 13:02:15'),
(25, 'delete_settings', 'settings', '2019-12-15 13:02:15', '2019-12-15 13:02:15'),
(26, 'browse_hooks', NULL, '2019-12-15 13:02:17', '2019-12-15 13:02:17'),
(27, 'browse_bids', 'bids', '2019-12-15 15:19:31', '2019-12-15 15:19:31'),
(28, 'read_bids', 'bids', '2019-12-15 15:19:32', '2019-12-15 15:19:32'),
(29, 'edit_bids', 'bids', '2019-12-15 15:19:32', '2019-12-15 15:19:32'),
(30, 'add_bids', 'bids', '2019-12-15 15:19:32', '2019-12-15 15:19:32'),
(31, 'delete_bids', 'bids', '2019-12-15 15:19:32', '2019-12-15 15:19:32'),
(37, 'browse_customers', 'customers', '2019-12-15 15:20:26', '2019-12-15 15:20:26'),
(38, 'read_customers', 'customers', '2019-12-15 15:20:26', '2019-12-15 15:20:26'),
(39, 'edit_customers', 'customers', '2019-12-15 15:20:26', '2019-12-15 15:20:26'),
(40, 'add_customers', 'customers', '2019-12-15 15:20:26', '2019-12-15 15:20:26'),
(41, 'delete_customers', 'customers', '2019-12-15 15:20:26', '2019-12-15 15:20:26'),
(42, 'browse_logs', 'logs', '2019-12-15 15:20:32', '2019-12-15 15:20:32'),
(43, 'read_logs', 'logs', '2019-12-15 15:20:32', '2019-12-15 15:20:32'),
(44, 'edit_logs', 'logs', '2019-12-15 15:20:32', '2019-12-15 15:20:32'),
(45, 'add_logs', 'logs', '2019-12-15 15:20:32', '2019-12-15 15:20:32'),
(46, 'delete_logs', 'logs', '2019-12-15 15:20:32', '2019-12-15 15:20:32'),
(47, 'browse_main_categories', 'main_categories', '2019-12-15 15:20:40', '2019-12-15 15:20:40'),
(48, 'read_main_categories', 'main_categories', '2019-12-15 15:20:40', '2019-12-15 15:20:40'),
(49, 'edit_main_categories', 'main_categories', '2019-12-15 15:20:40', '2019-12-15 15:20:40'),
(50, 'add_main_categories', 'main_categories', '2019-12-15 15:20:40', '2019-12-15 15:20:40'),
(51, 'delete_main_categories', 'main_categories', '2019-12-15 15:20:40', '2019-12-15 15:20:40'),
(52, 'browse_messages', 'messages', '2019-12-15 15:21:04', '2019-12-15 15:21:04'),
(53, 'read_messages', 'messages', '2019-12-15 15:21:04', '2019-12-15 15:21:04'),
(54, 'edit_messages', 'messages', '2019-12-15 15:21:04', '2019-12-15 15:21:04'),
(55, 'add_messages', 'messages', '2019-12-15 15:21:04', '2019-12-15 15:21:04'),
(56, 'delete_messages', 'messages', '2019-12-15 15:21:04', '2019-12-15 15:21:04'),
(57, 'browse_orders', 'orders', '2019-12-15 15:21:44', '2019-12-15 15:21:44'),
(58, 'read_orders', 'orders', '2019-12-15 15:21:44', '2019-12-15 15:21:44'),
(59, 'edit_orders', 'orders', '2019-12-15 15:21:44', '2019-12-15 15:21:44'),
(60, 'add_orders', 'orders', '2019-12-15 15:21:44', '2019-12-15 15:21:44'),
(61, 'delete_orders', 'orders', '2019-12-15 15:21:44', '2019-12-15 15:21:44'),
(62, 'browse_orders_details', 'orders_details', '2019-12-15 15:21:52', '2019-12-15 15:21:52'),
(63, 'read_orders_details', 'orders_details', '2019-12-15 15:21:52', '2019-12-15 15:21:52'),
(64, 'edit_orders_details', 'orders_details', '2019-12-15 15:21:52', '2019-12-15 15:21:52'),
(65, 'add_orders_details', 'orders_details', '2019-12-15 15:21:52', '2019-12-15 15:21:52'),
(66, 'delete_orders_details', 'orders_details', '2019-12-15 15:21:52', '2019-12-15 15:21:52'),
(67, 'browse_products', 'products', '2019-12-15 15:22:04', '2019-12-15 15:22:04'),
(68, 'read_products', 'products', '2019-12-15 15:22:04', '2019-12-15 15:22:04'),
(69, 'edit_products', 'products', '2019-12-15 15:22:04', '2019-12-15 15:22:04'),
(70, 'add_products', 'products', '2019-12-15 15:22:04', '2019-12-15 15:22:04'),
(71, 'delete_products', 'products', '2019-12-15 15:22:04', '2019-12-15 15:22:04'),
(77, 'browse_sellers', 'sellers', '2019-12-15 15:22:14', '2019-12-15 15:22:14'),
(78, 'read_sellers', 'sellers', '2019-12-15 15:22:14', '2019-12-15 15:22:14'),
(79, 'edit_sellers', 'sellers', '2019-12-15 15:22:14', '2019-12-15 15:22:14'),
(80, 'add_sellers', 'sellers', '2019-12-15 15:22:14', '2019-12-15 15:22:14'),
(81, 'delete_sellers', 'sellers', '2019-12-15 15:22:14', '2019-12-15 15:22:14'),
(82, 'browse_sliders', 'sliders', '2019-12-15 15:22:38', '2019-12-15 15:22:38'),
(83, 'read_sliders', 'sliders', '2019-12-15 15:22:38', '2019-12-15 15:22:38'),
(84, 'edit_sliders', 'sliders', '2019-12-15 15:22:38', '2019-12-15 15:22:38'),
(85, 'add_sliders', 'sliders', '2019-12-15 15:22:38', '2019-12-15 15:22:38'),
(86, 'delete_sliders', 'sliders', '2019-12-15 15:22:38', '2019-12-15 15:22:38'),
(87, 'browse_sub_categories', 'sub_categories', '2019-12-15 15:22:43', '2019-12-15 15:22:43'),
(88, 'read_sub_categories', 'sub_categories', '2019-12-15 15:22:43', '2019-12-15 15:22:43'),
(89, 'edit_sub_categories', 'sub_categories', '2019-12-15 15:22:43', '2019-12-15 15:22:43'),
(90, 'add_sub_categories', 'sub_categories', '2019-12-15 15:22:43', '2019-12-15 15:22:43'),
(91, 'delete_sub_categories', 'sub_categories', '2019-12-15 15:22:43', '2019-12-15 15:22:43'),
(117, 'browse_customer_rate', 'customer_rate', '2019-12-15 17:48:19', '2019-12-15 17:48:19'),
(118, 'read_customer_rate', 'customer_rate', '2019-12-15 17:48:19', '2019-12-15 17:48:19'),
(119, 'edit_customer_rate', 'customer_rate', '2019-12-15 17:48:19', '2019-12-15 17:48:19'),
(120, 'add_customer_rate', 'customer_rate', '2019-12-15 17:48:19', '2019-12-15 17:48:19'),
(121, 'delete_customer_rate', 'customer_rate', '2019-12-15 17:48:19', '2019-12-15 17:48:19'),
(122, 'browse_menus', 'menus', '2020-01-31 05:05:56', '2020-01-31 05:05:56'),
(123, 'read_menus', 'menus', '2020-01-31 05:05:56', '2020-01-31 05:05:56'),
(124, 'edit_menus', 'menus', '2020-01-31 05:05:56', '2020-01-31 05:05:56'),
(125, 'add_menus', 'menus', '2020-01-31 05:05:56', '2020-01-31 05:05:56'),
(126, 'delete_menus', 'menus', '2020-01-31 05:05:56', '2020-01-31 05:05:56'),
(127, 'browse_favorites', 'favorites', '2020-03-11 06:28:27', '2020-03-11 06:28:27'),
(128, 'read_favorites', 'favorites', '2020-03-11 06:28:27', '2020-03-11 06:28:27'),
(129, 'edit_favorites', 'favorites', '2020-03-11 06:28:27', '2020-03-11 06:28:27'),
(130, 'add_favorites', 'favorites', '2020-03-11 06:28:27', '2020-03-11 06:28:27'),
(131, 'delete_favorites', 'favorites', '2020-03-11 06:28:27', '2020-03-11 06:28:27'),
(132, 'browse_seller_order', 'seller_order', '2020-03-23 08:01:06', '2020-03-23 08:01:06'),
(133, 'read_seller_order', 'seller_order', '2020-03-23 08:01:06', '2020-03-23 08:01:06'),
(134, 'edit_seller_order', 'seller_order', '2020-03-23 08:01:06', '2020-03-23 08:01:06'),
(135, 'add_seller_order', 'seller_order', '2020-03-23 08:01:06', '2020-03-23 08:01:06'),
(136, 'delete_seller_order', 'seller_order', '2020-03-23 08:01:06', '2020-03-23 08:01:06'),
(142, 'browse_bindorder', 'bindorder', '2020-03-23 08:08:11', '2020-03-23 08:08:11'),
(143, 'read_bindorder', 'bindorder', '2020-03-23 08:08:11', '2020-03-23 08:08:11'),
(144, 'edit_bindorder', 'bindorder', '2020-03-23 08:08:11', '2020-03-23 08:08:11'),
(145, 'add_bindorder', 'bindorder', '2020-03-23 08:08:11', '2020-03-23 08:08:11'),
(146, 'delete_bindorder', 'bindorder', '2020-03-23 08:08:11', '2020-03-23 08:08:11'),
(147, 'browse_bind_categories', 'bind_categories', '2020-03-26 09:22:24', '2020-03-26 09:22:24'),
(148, 'read_bind_categories', 'bind_categories', '2020-03-26 09:22:24', '2020-03-26 09:22:24'),
(149, 'edit_bind_categories', 'bind_categories', '2020-03-26 09:22:24', '2020-03-26 09:22:24'),
(150, 'add_bind_categories', 'bind_categories', '2020-03-26 09:22:24', '2020-03-26 09:22:24'),
(151, 'delete_bind_categories', 'bind_categories', '2020-03-26 09:22:24', '2020-03-26 09:22:24'),
(152, 'browse_units', 'units', '2020-04-18 09:32:04', '2020-04-18 09:32:04'),
(153, 'read_units', 'units', '2020-04-18 09:32:04', '2020-04-18 09:32:04'),
(154, 'edit_units', 'units', '2020-04-18 09:32:04', '2020-04-18 09:32:04'),
(155, 'add_units', 'units', '2020-04-18 09:32:04', '2020-04-18 09:32:04'),
(156, 'delete_units', 'units', '2020-04-18 09:32:04', '2020-04-18 09:32:04'),
(157, 'browse_seller_rate', 'seller_rate', '2020-04-26 16:37:03', '2020-04-26 16:37:03'),
(158, 'read_seller_rate', 'seller_rate', '2020-04-26 16:37:03', '2020-04-26 16:37:03'),
(159, 'edit_seller_rate', 'seller_rate', '2020-04-26 16:37:03', '2020-04-26 16:37:03'),
(160, 'add_seller_rate', 'seller_rate', '2020-04-26 16:37:03', '2020-04-26 16:37:03'),
(161, 'delete_seller_rate', 'seller_rate', '2020-04-26 16:37:03', '2020-04-26 16:37:03');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 3),
(1, 4),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(27, 3),
(27, 4),
(28, 1),
(28, 3),
(28, 4),
(29, 1),
(29, 4),
(30, 1),
(30, 4),
(31, 1),
(31, 4),
(37, 1),
(38, 1),
(39, 1),
(39, 4),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(52, 3),
(52, 4),
(53, 1),
(53, 3),
(53, 4),
(54, 1),
(55, 1),
(55, 3),
(55, 4),
(56, 1),
(57, 1),
(57, 3),
(57, 4),
(58, 1),
(58, 3),
(58, 4),
(59, 1),
(59, 4),
(60, 1),
(60, 4),
(61, 1),
(61, 4),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(67, 3),
(68, 1),
(68, 3),
(69, 1),
(69, 3),
(70, 1),
(70, 3),
(71, 1),
(71, 3),
(77, 1),
(78, 1),
(79, 1),
(79, 3),
(80, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(117, 1),
(118, 1),
(119, 1),
(120, 1),
(121, 1),
(122, 1),
(123, 1),
(124, 1),
(125, 1),
(126, 1),
(127, 1),
(127, 4),
(128, 1),
(129, 1),
(130, 1),
(131, 1),
(131, 4),
(132, 1),
(132, 3),
(133, 1),
(133, 3),
(134, 1),
(134, 3),
(135, 1),
(135, 3),
(136, 1),
(136, 3),
(142, 1),
(142, 3),
(143, 1),
(144, 1),
(145, 1),
(146, 1),
(147, 1),
(148, 1),
(149, 1),
(150, 1),
(151, 1),
(152, 1),
(153, 1),
(154, 1),
(155, 1),
(156, 1),
(157, 1),
(158, 1),
(159, 1),
(160, 1),
(161, 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `available` int(11) NOT NULL DEFAULT 1,
  `sub_category_id` int(11) NOT NULL,
  `images` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `seller_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `number_store` int(11) DEFAULT 0,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `available`, `sub_category_id`, `images`, `seller_id`, `created_at`, `updated_at`, `number_store`, `description`) VALUES
(2, 'اكسسوارات', 15, 1, 22, '[\"products\\\\March2020\\\\NtV4GW1Ve4FdNkX7fPhJ.jpg\"]', 3, '2020-03-05 08:04:43', '2020-03-26 17:47:02', 0, NULL),
(3, 'كنب', 1000, 1, 1, '[\"products\\\\March2020\\\\BvOnKL1tNX1wVrku3aGl.jpg\",\"products\\\\March2020\\\\ynoDdSrzbny8wRVUbdc5.jpg\",\"products\\\\March2020\\\\9S5VHEzkin5RugaocnK0.jpg\"]', 4, '2020-03-05 08:05:09', '2020-03-05 08:05:09', 0, NULL),
(4, 'اجهزة منزلية', 30000, 1, 3, '[\"products\\\\March2020\\\\JiS4VHUvP9NbF2OqnFMb.jpeg\"]', 6, '2020-03-05 08:05:49', '2020-03-05 08:05:49', 0, NULL),
(5, 'سفيحا', 2, 1, 1, '[\"products\\\\March2020\\\\byuDAg01b1kRay3UmX7V.jpg\",\"products\\\\March2020\\\\D4BN7nv7WKxhuTVSWgvZ.jpg\",\"products\\\\March2020\\\\DUkANKeir70JTvP7sSOF.jpg\",\"products\\\\March2020\\\\UVhMWIVqNggyIgf77Wab.jpg\"]', 5, '2020-03-05 08:23:00', '2020-03-08 08:26:29', 0, NULL),
(6, 'سبانخ', 5, 1, 2, '[\"products\\\\March2020\\\\mskC5H8VTtqe5VD5Kw1U.jpg\",\"products\\\\April2020\\\\oSxWCDP4NjyuGKKPQrxX.jpg\",\"products\\\\April2020\\\\kqX6fLeEGMe9x680Fqgt.jpg\",\"products\\\\April2020\\\\n5cY9GJmaQbLWEo8GiIf.jpg\"]', 20, '2020-03-05 08:24:00', '2020-04-20 08:00:53', 2, 'كيلو سبانخ عجينة يتم جعلهم مثل اقراص مثلثه ويوضع السبانخ داخل العجين ويتم خبيزة في الفرن يصبح جاهزا'),
(7, 'حفر على الخشب', 120, 0, 1, '[\"products\\\\March2020\\\\pO7lCh2oi3INcduoFdcG.png\",\"products\\\\March2020\\\\wT9DWrUgdvgMa2bVOZYO.PNG\",\"products\\\\March2020\\\\3MjlRx5ttnn56tzFYP0y.png\"]', 22, '2020-03-21 12:48:58', '2020-03-25 12:10:45', 0, NULL),
(10, 'asd', 12, 1, -1, '[\"products\\\\March2020\\\\7cfkMK1qtMyBMyfnYzLP.png\",\"products\\\\March2020\\\\EV9ZwYoiBnoFwIO1LG6L.png\",\"products\\\\March2020\\\\zZTMfOK83wbEEX6Tn4Vn.png\",\"products\\\\March2020\\\\ZP2TfdH3IIZDKCq25ET2.png\"]', 22, '2020-03-26 17:43:40', '2020-03-26 17:43:40', 0, NULL),
(11, 'asd', 120, 1, -1, '[\"products\\\\March2020\\\\EQUKU73DI5Oqkf3PxZMW.png\",\"products\\\\March2020\\\\IhhIch6ugU5WaUUEpXmp.jpg\",\"products\\\\March2020\\\\NzWHGbTKa3uCUy3cC1Ez.jpg\",\"products\\\\March2020\\\\QLfmAKSyB0VgxeK6w9rI.PNG\",\"products\\\\March2020\\\\StHDc4oiF8YYAxqJzcUB.png\",\"products\\\\March2020\\\\jO2FqZ5D6uHsalOCg0Wn.png\"]', 22, '2020-03-26 17:46:31', '2020-03-26 17:46:31', 0, NULL),
(12, 'asd', 123, 1, -1, '[\"products\\\\March2020\\\\SRC8Sil3sQUCDCH6IHwW.jpg\",\"products\\\\March2020\\\\d9e8tDO4HHqjRdACFD8d.jpg\",\"products\\\\March2020\\\\H55IM1E2kgOvZUNunw6D.PNG\",\"products\\\\March2020\\\\UJzD7dGm5tpbBfiExCAE.png\",\"products\\\\March2020\\\\E7LGB1jjrawNyGXn61fF.png\"]', 22, '2020-03-26 17:49:46', '2020-03-26 17:49:46', 0, NULL),
(13, 'AS', 123, 1, 24, '[\"products\\\\March2020\\\\ewYzyHoGv5748UYInPAZ.jpg\",\"products\\\\March2020\\\\PYmiNEVvDRhipprEWtJa.jpg\",\"products\\\\March2020\\\\lmE5pZa9R6eXGUQsGInI.PNG\",\"products\\\\March2020\\\\y2FVSyP4JtPOX1UWSlSn.png\",\"products\\\\March2020\\\\abk0ekReUpYZFj8e6fnU.png\",\"products\\\\April2020\\\\GKTIUjyNltoMEFoBvTex.jpg\",\"products\\\\April2020\\\\sIFqRtnj4vNgOihGkLMN.jpg\",\"products\\\\April2020\\\\8IFUbda5Sjw5rcFtMurh.jpg\",\"products\\\\April2020\\\\vYsS3E79P6X1rJLaWIFS.jpg\",\"products\\\\April2020\\\\od5Zv1HwqsiKS1liZoBY.jpg\",\"products\\\\April2020\\\\oO7ox6T3tQgdCZiM0ob8.jpg\",\"products\\\\April2020\\\\LaIvtL5cT1mkIMKRytkM.jpg\",\"products\\\\April2020\\\\T9Lo7y7FQH91l6pdKTZx.jpg\"]', 22, '2020-03-26 17:53:41', '2020-04-25 17:19:30', 2, 'as ad sd asd asd asd asdasdas dasdas sdgfdgg egr ewf sfdgregdsf sgsd  daf ');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2019-12-15 13:02:13', '2019-12-15 13:02:13'),
(2, 'user', 'Normal User', '2019-12-15 13:02:14', '2019-12-15 13:02:14'),
(3, 'seller', 'seller', '2020-01-31 07:47:47', '2020-01-31 07:47:47'),
(4, 'customer', 'customer', '2020-01-31 07:48:06', '2020-01-31 07:48:06');

-- --------------------------------------------------------

--
-- Table structure for table `sellers`
--

CREATE TABLE `sellers` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `story_body` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `seller_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sellers`
--

INSERT INTO `sellers` (`id`, `user_id`, `story_body`, `created_at`, `updated_at`, `seller_name`) VALUES
(3, 17, '<p>wqewqe</p>', '2020-02-05 07:10:00', '2020-02-05 07:17:43', 'sameer Mohammad'),
(4, 19, '123123', '2020-02-14 14:24:58', '2020-02-14 14:24:58', 'mm mm'),
(5, 20, 'اقوم بعمل اشياء مذهله', '2020-02-21 09:07:03', '2020-02-21 09:07:03', 'محمد امواس'),
(6, 22, '<p>asdasdasdasda asdasdasdasdaasdasdasdasdaasdasdasdasdaasdasdasdasdaasdasdasdasdaasdasdasdasda</p>', '2020-03-03 06:36:27', '2020-03-24 14:47:25', 'testSeller testSeller'),
(7, 23, 'معجنات', '2020-03-05 08:22:43', '2020-03-05 08:22:43', 'طارق التميمي'),
(8, 26, ' ', '2020-04-22 08:21:49', '2020-04-22 08:21:49', 'asd asd'),
(9, 27, NULL, '2020-04-22 08:23:50', '2020-04-22 08:23:50', '123 123');

-- --------------------------------------------------------

--
-- Table structure for table `seller_rate`
--

CREATE TABLE `seller_rate` (
  `id` int(10) UNSIGNED NOT NULL,
  `rate` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `seller_rate`
--

INSERT INTO `seller_rate` (`id`, `rate`, `description`, `customer_id`, `seller_id`, `created_at`, `updated_at`) VALUES
(1, 4, 'سكشي تشسكمي يسح تشيك شسيىشسيتكةمسشيمهت شسيشس شسي شسي شسي شسي شسي', 5, 22, '2020-04-26 11:48:35', '2020-04-26 11:48:35'),
(2, 1, 'شسيشيشس', 5, 22, '2020-04-26 12:19:06', '2020-04-26 12:19:06');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', 'settings\\December2019\\6TKxaSE20lW5jSUuFW0a.png', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'productive family', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'The productive family website is the first online platform for productive families', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', 'settings\\December2019\\DB0Egkb5ZY4PNa22iy7s.png', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', 'settings\\March2020\\XM1OkpEYKP5y9aK8gzsa.jpg', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `button_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `button_action` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `View` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `target` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `title`, `body`, `image`, `button_title`, `button_action`, `View`, `created_at`, `updated_at`, `target`) VALUES
(3, '<h1><strong><span style=\"color: #0000ff; background-color: #000000;\">نعم انه الاعلان الاول</span></strong></h1>\r\n<p style=\"text-align: center;\">لاكن لا يمكن ب الامكان ما كان الى الذي كان ان يكون</p>\r\n<p style=\"text-align: center;\">نعم يمككنا فعل شيئل ما لاكن لا اعلم ما هو</p>', NULL, 'sliders\\March2020\\pEFyYYMiGI9ZKQ18jOnP.jpg', 'اضغض هنا', 'https://www.google.com', 1, '2020-03-30 10:04:00', '2020-03-30 11:08:42', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `main_category_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `unit_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `name`, `main_category_id`, `created_at`, `updated_at`, `unit_id`) VALUES
(1, 'الصنف الثاني 1', 7, '2020-02-05 15:40:00', '2020-04-18 09:34:36', 3),
(2, 'الصنف الثاني 2', 7, '2020-02-05 15:40:00', '2020-04-18 09:34:29', 3),
(3, 'الصنف الثاني 3', 7, '2020-02-05 15:40:00', '2020-04-18 09:34:45', 1),
(10, 'الصنف الثاني 4', 7, '2020-02-06 04:59:00', '2020-04-18 09:34:05', 2),
(11, 'الصنف الثاني 5', 7, '2020-02-06 04:59:00', '2020-04-18 09:34:12', 1),
(17, 'صنف الاول رقم واحد', 8, '2020-03-05 08:03:00', '2020-04-18 09:33:59', 3),
(18, 'name', 9, '2020-03-26 15:02:00', '2020-04-18 09:33:49', 1),
(19, 'test', 7, '2020-03-26 15:03:00', '2020-04-18 09:33:41', 3),
(20, 'وحش', 7, '2020-03-26 17:29:00', '2020-04-18 09:33:35', 1),
(21, 'asd', 7, '2020-03-26 17:44:00', '2020-04-18 09:33:28', 2),
(22, 'asd', 10, '2020-03-26 17:47:00', '2020-04-18 09:33:19', 3),
(23, 'asd', 11, '2020-03-26 17:50:00', '2020-04-18 09:33:13', 2),
(24, '123654', 12, '2020-03-26 17:54:00', '2020-04-18 09:33:07', 1);

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE `units` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'كيلو', '2020-04-18 09:32:16', '2020-04-18 09:32:16'),
(2, 'حبة', '2020-04-18 09:32:23', '2020-04-18 09:32:23'),
(3, 'دزدينة', '2020-04-18 09:32:29', '2020-04-18 09:32:29');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `first_name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`, `last_name`, `phone_number`, `user_name`, `country`, `city`, `street`, `gender`, `active`) VALUES
(1, 1, 'mohammad imwas', 'admin@admin.com', 'users/default.png', NULL, '$2y$10$j48conB0W2Hp/7XWqNSgnenlzx5ua9EUBWdHoETUc.68iEpysVV1i', 'F7gkzdFwIf80CeS5SutrutYyo5wxB6zl2dBgyhFSmgJaaKIHvqd79tzTB66o', NULL, '2019-12-15 13:03:02', '2019-12-15 13:03:02', NULL, NULL, 'mo', NULL, NULL, NULL, NULL, 1),
(14, 4, 'Mohammad', 'm@m.com', 'users/default.png', NULL, '$2y$10$m4FLe3bO7KgRq0UBKwy4v.HdUnr1qlFhTTpLvO2E1spJCL9S0IHAa', NULL, NULL, '2020-02-05 07:00:46', '2020-02-05 07:06:52', 'Qawasmeh', '2203354', 'mm', 'swe', 'aswqe', 'wqe', 0, 0),
(17, 3, 'sameer', 's@s.com', 'users/default.png', NULL, '$2y$10$EuBpTd51KcnSu0.cQtCeW.F6GrzGiI5ApRh8EU2eC66vmUU5bZNUa', NULL, NULL, '2020-02-05 07:10:17', '2020-02-05 07:17:58', 'Mohammad', '1234567', 'ss', 'ewqe', 'wqe', 'wqe', 0, 0),
(18, 4, 'mohammad', 'mohammadimwas77@gmail.com', 'users/default.png', NULL, '$2y$10$ixN39HtcRvSSzEWhgnALN./W2Hrot5enjE6kha2TOMnS1sNxQ6fBm', NULL, NULL, '2020-02-11 06:36:19', '2020-02-11 06:37:01', 'imwas', '059991699', 'manager@wazcam.com', 'asdfd', 'asdas', 'asdass', 0, 0),
(19, 3, 'mm', 'mm@nn.com', 'users/default.png', NULL, '$2y$10$og6O/XfA/LC7YJ/ecEFPo.KuDNtFTHfKqTJ0csFYgcGu/CCqh8.Wq', NULL, NULL, '2020-02-14 14:24:57', '2020-02-14 14:24:57', 'mm', '123', 'manager@wazcam.com', '123', '123', '123', 0, 0),
(20, 3, 'محمد', 'imwas@gmial.com', 'users/default.png', NULL, '$2y$10$1uwarwOMiQeusbPRQJLVsO0Gw0oE8huSNjRjyurLYDp6E2SJieISy', 'IwMOgYvF1cVMPCa2YOKTjDzbL4gVzqbZGBTSeIvdpXhq9VhVlFdfK90g91u1', '{\"locale\":\"ar\"}', '2020-02-21 09:07:03', '2020-02-21 09:07:03', 'امواس', '0599197580', 'manager@wazcam.com', 'فلسطين', 'الخليل', 'بني نعيم', 0, 0),
(21, 4, 'زبون', 'customer@gmail.com', 'users/default.png', NULL, '$2y$10$Puikgd0J6sloZBBAFKryIO2xp8p8kPWrDgBO8r8QnBmNNAjENyONu', NULL, '{\"locale\":\"ar\"}', '2020-03-03 06:13:59', '2020-04-22 08:08:03', 'زببين 123', '059919758', 'manager@wazcam.com', 'asd', 'asd', 'asd', 1, 0),
(22, 3, 'testSeller', 'testSeeler@gmial.com', 'users/default.png', NULL, '$2y$10$Sb0Bs82M4rdoGVbz9rqi2uY.oD4dZSvrgHFyuHanbb9rJ4HIyxm/O', NULL, '{\"locale\":\"ar\"}', '2020-03-03 06:36:27', '2020-03-24 16:24:38', '9999', '0599875', '888', '11111', '222222', '333333', 1, 0),
(23, 3, 'طارق', 't@t.com', 'users/default.png', NULL, '$2y$10$8hqvnJGeE/MG2qFJ65eBVO/3epLzC7mRAPy/nb3Z.otJURZAYZpEu', NULL, NULL, '2020-03-05 08:22:43', '2020-03-05 08:22:43', 'التميمي', '025597425', 'ta3', 'فلسطين', 'الخليل', 'مفرق الجامعة', 0, 0),
(24, 4, 'مالك', 'm@m2.com', 'users/default.png', NULL, '$2y$10$xiEOnGcFfpvMEeAeOXEoIueSB/OUc/2BMI2UPzjkcPp1CN5x07v/S', NULL, NULL, '2020-03-05 08:25:32', '2020-03-05 08:25:32', 'التميمي', '1234455', 'ms', 'فلسطين', 'الخليل', 'حي الجامعة', 0, 0),
(25, 4, 'mohammad', 'Q@q.com', 'users/default.png', NULL, '$2y$10$lyxc1K8K7UHubRQv6NrOO.9ziBrXLwpxHTbn.jgjoqJV2RozsgPvi', NULL, '{\"locale\":\"ar\"}', '2020-03-08 07:15:50', '2020-03-08 07:15:50', 'qawasmeh', '123456789', 'ewq', 'فلسطين', 'الخليل', 'خخ', 0, 0),
(26, 3, 'asd', 'asd@asd.com', 'users/default.png', NULL, '$2y$10$0CYR4IKoY7Hsiigt9AWQNeAKB0m3mVgvegKf8HB5m/nGsO.4ahdfC', NULL, '{\"locale\":\"ar\"}', '2020-04-22 08:21:49', '2020-04-22 08:21:49', 'asd', '0599197580', '123456', '123', '123', '123', 0, 1),
(27, 3, '123', 'sss@sssssss.sssss', 'users/default.png', NULL, '$2y$10$8EMHCE01RwDm72lcljk.8uKZ85lHeqFsiBe/l4fOdIrlKkG0RbzY2', NULL, '{\"locale\":\"ar\"}', '2020-04-22 08:23:50', '2020-04-22 08:24:08', '123', '0599197585', '15487', '12', '12', '12', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bids`
--
ALTER TABLE `bids`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bindorder`
--
ALTER TABLE `bindorder`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bind_categories`
--
ALTER TABLE `bind_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_rate`
--
ALTER TABLE `customer_rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `favorites`
--
ALTER TABLE `favorites`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_categories`
--
ALTER TABLE `main_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders_details`
--
ALTER TABLE `orders_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `sellers`
--
ALTER TABLE `sellers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seller_rate`
--
ALTER TABLE `seller_rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bids`
--
ALTER TABLE `bids`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `bindorder`
--
ALTER TABLE `bindorder`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `bind_categories`
--
ALTER TABLE `bind_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `customer_rate`
--
ALTER TABLE `customer_rate`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=203;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `favorites`
--
ALTER TABLE `favorites`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `main_categories`
--
ALTER TABLE `main_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT for table `orders_details`
--
ALTER TABLE `orders_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=162;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sellers`
--
ALTER TABLE `sellers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `seller_rate`
--
ALTER TABLE `seller_rate`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
