-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 15, 2019 at 09:23 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `homemade`
--

-- --------------------------------------------------------

--
-- Table structure for table `bids`
--

CREATE TABLE `bids` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `price` double NOT NULL,
  `number_of_day` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customer_rate`
--

CREATE TABLE `customer_rate` (
  `id` int(10) UNSIGNED NOT NULL,
  `rate` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(22, 4, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 0),
(23, 4, 'order_id', 'text', 'Order Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(24, 4, 'seller_id', 'text', 'Seller Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(25, 4, 'price', 'text', 'Price', 1, 1, 1, 1, 1, 1, '{}', 4),
(26, 4, 'number_of_day', 'text', 'Number Of Day', 1, 1, 1, 1, 1, 1, '{}', 5),
(27, 4, 'description', 'text', 'Description', 0, 1, 1, 1, 1, 1, '{}', 6),
(28, 4, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
(29, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(30, 6, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(31, 6, 'user_id', 'text', 'User Id', 0, 1, 1, 1, 1, 1, '{}', 3),
(32, 6, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(33, 6, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(34, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(35, 7, 'log', 'text', 'Log', 1, 1, 1, 1, 1, 1, '{}', 3),
(36, 7, 'user_id', 'text', 'User Id', 1, 1, 1, 1, 1, 1, '{}', 4),
(37, 7, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(38, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(39, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(40, 8, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(41, 8, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 3),
(42, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(43, 9, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(44, 9, 'message', 'text', 'Message', 0, 1, 1, 1, 1, 1, '{}', 2),
(45, 9, 'sender_id', 'text', 'Sender Id', 0, 1, 1, 1, 1, 1, '{}', 4),
(46, 9, 'resever_id', 'text', 'Resever Id', 0, 1, 1, 1, 1, 1, '{}', 6),
(47, 9, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
(48, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(49, 10, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(50, 10, 'customer_id', 'text', 'Customer Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(51, 10, 'seller_id', 'text', 'Seller Id', 0, 1, 1, 1, 1, 1, '{}', 3),
(52, 10, 'min_price', 'text', 'Min Price', 1, 1, 1, 1, 1, 1, '{}', 4),
(53, 10, 'max_price', 'text', 'Max Price', 1, 1, 1, 1, 1, 1, '{}', 5),
(54, 10, 'final_price', 'text', 'Final Price', 0, 1, 1, 1, 1, 1, '{}', 6),
(55, 10, 'start_date', 'text', 'Start Date', 1, 1, 1, 1, 1, 1, '{}', 7),
(56, 10, 'end_date', 'text', 'End Date', 1, 1, 1, 1, 1, 1, '{}', 8),
(57, 10, 'description', 'text', 'Description', 1, 1, 1, 1, 1, 1, '{}', 9),
(58, 10, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 10),
(59, 10, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 11),
(60, 11, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 0),
(61, 11, 'order_id', 'text', 'Order Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(62, 11, 'product_id', 'text', 'Product Id', 0, 1, 1, 1, 1, 1, '{}', 3),
(63, 11, 'quantity', 'text', 'Quantity', 1, 1, 1, 1, 1, 1, '{}', 4),
(64, 11, 'price', 'text', 'Price', 0, 1, 1, 1, 1, 1, '{}', 5),
(65, 11, 'type', 'text', 'Type', 1, 1, 1, 1, 1, 1, '{}', 6),
(66, 11, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
(67, 11, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(68, 12, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(69, 12, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(70, 12, 'price', 'text', 'Price', 1, 1, 1, 1, 1, 1, '{}', 3),
(71, 12, 'available', 'text', 'Available', 1, 1, 1, 1, 1, 1, '{}', 4),
(72, 12, 'sub_category_id', 'text', 'Sub Category Id', 1, 1, 1, 1, 1, 1, '{}', 5),
(73, 12, 'image1', 'text', 'Image1', 1, 1, 1, 1, 1, 1, '{}', 6),
(74, 12, 'image2', 'text', 'Image2', 1, 1, 1, 1, 1, 1, '{}', 7),
(75, 12, 'image3', 'text', 'Image3', 0, 1, 1, 1, 1, 1, '{}', 8),
(76, 12, 'image4', 'text', 'Image4', 0, 1, 1, 1, 1, 1, '{}', 9),
(77, 12, 'image5', 'text', 'Image5', 0, 1, 1, 1, 1, 1, '{}', 10),
(78, 12, 'seller_id', 'text', 'Seller Id', 1, 1, 1, 1, 1, 1, '{}', 11),
(79, 12, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 12),
(80, 12, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 13),
(81, 14, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(82, 14, 'user_id', 'text', 'User Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(83, 14, 'story_title', 'text', 'Story Title', 1, 1, 1, 1, 1, 1, '{}', 4),
(84, 14, 'story_body', 'text', 'Story Body', 1, 1, 1, 1, 1, 1, '{}', 5),
(85, 14, 'story_image1', 'text', 'Story Image1', 1, 1, 1, 1, 1, 1, '{}', 6),
(86, 14, 'story_image2', 'text', 'Story Image2', 0, 1, 1, 1, 1, 1, '{}', 7),
(87, 14, 'story_image3', 'text', 'Story Image3', 0, 1, 1, 1, 1, 1, '{}', 8),
(88, 14, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 9),
(89, 14, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 10),
(90, 15, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(91, 15, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 2),
(92, 15, 'body', 'text', 'Body', 1, 1, 1, 1, 1, 1, '{}', 3),
(93, 15, 'image', 'text', 'Image', 1, 1, 1, 1, 1, 1, '{}', 4),
(94, 15, 'button_title', 'text', 'Button Title', 0, 1, 1, 1, 1, 1, '{}', 5),
(95, 15, 'button_action', 'text', 'Button Action', 0, 1, 1, 1, 1, 1, '{}', 6),
(96, 15, 'View', 'text', 'View', 1, 1, 1, 1, 1, 1, '{}', 7),
(97, 15, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(98, 15, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(99, 16, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(100, 16, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, '{}', 2),
(101, 16, 'main_category_id', 'text', 'Main Category Id', 0, 1, 1, 1, 1, 1, '{}', 3),
(102, 16, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(103, 16, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(104, 7, 'log_belongsto_user_relationship', 'relationship', 'users', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(105, 9, 'message_belongsto_user_relationship', 'relationship', 'users', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"sender_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":\"0\"}', 3),
(106, 9, 'message_belongsto_user_relationship_1', 'relationship', 'users', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"resever_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":\"0\"}', 5),
(107, 14, 'seller_hasone_user_relationship', 'relationship', 'users', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"hasOne\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(108, 6, 'customer_hasone_user_relationship', 'relationship', 'users', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"hasOne\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2019-12-15 13:02:12', '2019-12-15 13:02:12'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2019-12-15 13:02:12', '2019-12-15 13:02:12'),
(4, 'bids', 'bids', 'Bid', 'Bids', 'voyager-wallet', 'App\\Bid', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-12-15 15:19:31', '2019-12-15 15:23:53'),
(6, 'customers', 'customers', 'Customer', 'Customers', NULL, 'App\\Customer', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-12-15 15:20:26', '2019-12-15 17:22:19'),
(7, 'logs', 'logs', 'Log', 'Logs', NULL, 'App\\Log', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-12-15 15:20:32', '2019-12-15 17:10:20'),
(8, 'main_categories', 'main-categories', 'Main Category', 'Main Categories', NULL, 'App\\MainCategory', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-12-15 15:20:40', '2019-12-15 15:20:40'),
(9, 'messages', 'messages', 'Message', 'Messages', NULL, 'App\\Message', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-12-15 15:21:04', '2019-12-15 17:12:56'),
(10, 'orders', 'orders', 'Order', 'Orders', NULL, 'App\\Order', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-12-15 15:21:44', '2019-12-15 15:21:44'),
(11, 'orders_details', 'orders-details', 'Orders Detail', 'Orders Details', NULL, 'App\\OrdersDetail', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-12-15 15:21:52', '2019-12-15 15:21:52'),
(12, 'products', 'products', 'Product', 'Products', NULL, 'App\\Product', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-12-15 15:22:03', '2019-12-15 15:22:03'),
(14, 'sellers', 'sellers', 'Seller', 'Sellers', NULL, 'App\\Seller', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-12-15 15:22:14', '2019-12-15 17:21:37'),
(15, 'sliders', 'sliders', 'Slider', 'Sliders', NULL, 'App\\Slider', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-12-15 15:22:38', '2019-12-15 15:22:38'),
(16, 'sub_categories', 'sub-categories', 'Sub Category', 'Sub Categories', NULL, 'App\\SubCategory', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-12-15 15:22:42', '2019-12-15 15:22:42'),
(22, 'customer_rate', 'customer-rate', 'Customer Rate', 'Customer Rates', NULL, 'App\\CustomerRate', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-12-15 17:48:19', '2019-12-15 17:48:19');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `log` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `main_categories`
--

CREATE TABLE `main_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2019-12-15 13:02:13', '2019-12-15 13:02:13');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2019-12-15 13:02:13', '2019-12-15 13:02:13', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 5, '2019-12-15 13:02:13', '2019-12-15 13:02:13', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2019-12-15 13:02:13', '2019-12-15 13:02:13', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2019-12-15 13:02:13', '2019-12-15 13:02:13', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 9, '2019-12-15 13:02:13', '2019-12-15 13:02:13', NULL, NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 11, '2019-12-15 13:02:13', '2019-12-15 13:02:13', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 12, '2019-12-15 13:02:13', '2019-12-15 13:02:13', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 13, '2019-12-15 13:02:13', '2019-12-15 13:02:13', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 14, '2019-12-15 13:02:13', '2019-12-15 13:02:13', 'voyager.settings.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 13, '2019-12-15 13:02:17', '2019-12-15 13:02:17', 'voyager.hooks', NULL),
(12, 1, 'Bids', '', '_self', NULL, NULL, NULL, 15, '2019-12-15 15:19:32', '2019-12-15 15:19:32', 'voyager.bids.index', NULL),
(14, 1, 'Customers', '', '_self', NULL, NULL, NULL, 17, '2019-12-15 15:20:26', '2019-12-15 15:20:26', 'voyager.customers.index', NULL),
(15, 1, 'Logs', '', '_self', NULL, NULL, NULL, 18, '2019-12-15 15:20:32', '2019-12-15 15:20:32', 'voyager.logs.index', NULL),
(16, 1, 'Main Categories', '', '_self', NULL, NULL, NULL, 19, '2019-12-15 15:20:40', '2019-12-15 15:20:40', 'voyager.main-categories.index', NULL),
(17, 1, 'Messages', '', '_self', NULL, NULL, NULL, 20, '2019-12-15 15:21:05', '2019-12-15 15:21:05', 'voyager.messages.index', NULL),
(18, 1, 'Orders', '', '_self', NULL, NULL, NULL, 21, '2019-12-15 15:21:44', '2019-12-15 15:21:44', 'voyager.orders.index', NULL),
(19, 1, 'Orders Details', '', '_self', NULL, NULL, NULL, 22, '2019-12-15 15:21:52', '2019-12-15 15:21:52', 'voyager.orders-details.index', NULL),
(20, 1, 'Products', '', '_self', NULL, NULL, NULL, 23, '2019-12-15 15:22:04', '2019-12-15 15:22:04', 'voyager.products.index', NULL),
(22, 1, 'Sellers', '', '_self', NULL, NULL, NULL, 25, '2019-12-15 15:22:14', '2019-12-15 15:22:14', 'voyager.sellers.index', NULL),
(23, 1, 'Sliders', '', '_self', NULL, NULL, NULL, 26, '2019-12-15 15:22:38', '2019-12-15 15:22:38', 'voyager.sliders.index', NULL),
(24, 1, 'Sub Categories', '', '_self', NULL, NULL, NULL, 27, '2019-12-15 15:22:43', '2019-12-15 15:22:43', 'voyager.sub-categories.index', NULL),
(30, 1, 'Customer Rates', '', '_self', NULL, NULL, NULL, 28, '2019-12-15 17:48:19', '2019-12-15 17:48:19', 'voyager.customer-rate.index', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `message` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sender_id` bigint(20) DEFAULT NULL,
  `resever_id` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` bigint(20) NOT NULL,
  `seller_id` bigint(20) DEFAULT NULL,
  `min_price` double NOT NULL,
  `max_price` double NOT NULL,
  `final_price` double DEFAULT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders_details`
--

CREATE TABLE `orders_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `quantity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2019-12-15 13:02:14', '2019-12-15 13:02:14'),
(2, 'browse_bread', NULL, '2019-12-15 13:02:14', '2019-12-15 13:02:14'),
(3, 'browse_database', NULL, '2019-12-15 13:02:14', '2019-12-15 13:02:14'),
(4, 'browse_media', NULL, '2019-12-15 13:02:14', '2019-12-15 13:02:14'),
(5, 'browse_compass', NULL, '2019-12-15 13:02:14', '2019-12-15 13:02:14'),
(11, 'browse_roles', 'roles', '2019-12-15 13:02:14', '2019-12-15 13:02:14'),
(12, 'read_roles', 'roles', '2019-12-15 13:02:14', '2019-12-15 13:02:14'),
(13, 'edit_roles', 'roles', '2019-12-15 13:02:14', '2019-12-15 13:02:14'),
(14, 'add_roles', 'roles', '2019-12-15 13:02:14', '2019-12-15 13:02:14'),
(15, 'delete_roles', 'roles', '2019-12-15 13:02:14', '2019-12-15 13:02:14'),
(16, 'browse_users', 'users', '2019-12-15 13:02:14', '2019-12-15 13:02:14'),
(17, 'read_users', 'users', '2019-12-15 13:02:15', '2019-12-15 13:02:15'),
(18, 'edit_users', 'users', '2019-12-15 13:02:15', '2019-12-15 13:02:15'),
(19, 'add_users', 'users', '2019-12-15 13:02:15', '2019-12-15 13:02:15'),
(20, 'delete_users', 'users', '2019-12-15 13:02:15', '2019-12-15 13:02:15'),
(21, 'browse_settings', 'settings', '2019-12-15 13:02:15', '2019-12-15 13:02:15'),
(22, 'read_settings', 'settings', '2019-12-15 13:02:15', '2019-12-15 13:02:15'),
(23, 'edit_settings', 'settings', '2019-12-15 13:02:15', '2019-12-15 13:02:15'),
(24, 'add_settings', 'settings', '2019-12-15 13:02:15', '2019-12-15 13:02:15'),
(25, 'delete_settings', 'settings', '2019-12-15 13:02:15', '2019-12-15 13:02:15'),
(26, 'browse_hooks', NULL, '2019-12-15 13:02:17', '2019-12-15 13:02:17'),
(27, 'browse_bids', 'bids', '2019-12-15 15:19:31', '2019-12-15 15:19:31'),
(28, 'read_bids', 'bids', '2019-12-15 15:19:32', '2019-12-15 15:19:32'),
(29, 'edit_bids', 'bids', '2019-12-15 15:19:32', '2019-12-15 15:19:32'),
(30, 'add_bids', 'bids', '2019-12-15 15:19:32', '2019-12-15 15:19:32'),
(31, 'delete_bids', 'bids', '2019-12-15 15:19:32', '2019-12-15 15:19:32'),
(37, 'browse_customers', 'customers', '2019-12-15 15:20:26', '2019-12-15 15:20:26'),
(38, 'read_customers', 'customers', '2019-12-15 15:20:26', '2019-12-15 15:20:26'),
(39, 'edit_customers', 'customers', '2019-12-15 15:20:26', '2019-12-15 15:20:26'),
(40, 'add_customers', 'customers', '2019-12-15 15:20:26', '2019-12-15 15:20:26'),
(41, 'delete_customers', 'customers', '2019-12-15 15:20:26', '2019-12-15 15:20:26'),
(42, 'browse_logs', 'logs', '2019-12-15 15:20:32', '2019-12-15 15:20:32'),
(43, 'read_logs', 'logs', '2019-12-15 15:20:32', '2019-12-15 15:20:32'),
(44, 'edit_logs', 'logs', '2019-12-15 15:20:32', '2019-12-15 15:20:32'),
(45, 'add_logs', 'logs', '2019-12-15 15:20:32', '2019-12-15 15:20:32'),
(46, 'delete_logs', 'logs', '2019-12-15 15:20:32', '2019-12-15 15:20:32'),
(47, 'browse_main_categories', 'main_categories', '2019-12-15 15:20:40', '2019-12-15 15:20:40'),
(48, 'read_main_categories', 'main_categories', '2019-12-15 15:20:40', '2019-12-15 15:20:40'),
(49, 'edit_main_categories', 'main_categories', '2019-12-15 15:20:40', '2019-12-15 15:20:40'),
(50, 'add_main_categories', 'main_categories', '2019-12-15 15:20:40', '2019-12-15 15:20:40'),
(51, 'delete_main_categories', 'main_categories', '2019-12-15 15:20:40', '2019-12-15 15:20:40'),
(52, 'browse_messages', 'messages', '2019-12-15 15:21:04', '2019-12-15 15:21:04'),
(53, 'read_messages', 'messages', '2019-12-15 15:21:04', '2019-12-15 15:21:04'),
(54, 'edit_messages', 'messages', '2019-12-15 15:21:04', '2019-12-15 15:21:04'),
(55, 'add_messages', 'messages', '2019-12-15 15:21:04', '2019-12-15 15:21:04'),
(56, 'delete_messages', 'messages', '2019-12-15 15:21:04', '2019-12-15 15:21:04'),
(57, 'browse_orders', 'orders', '2019-12-15 15:21:44', '2019-12-15 15:21:44'),
(58, 'read_orders', 'orders', '2019-12-15 15:21:44', '2019-12-15 15:21:44'),
(59, 'edit_orders', 'orders', '2019-12-15 15:21:44', '2019-12-15 15:21:44'),
(60, 'add_orders', 'orders', '2019-12-15 15:21:44', '2019-12-15 15:21:44'),
(61, 'delete_orders', 'orders', '2019-12-15 15:21:44', '2019-12-15 15:21:44'),
(62, 'browse_orders_details', 'orders_details', '2019-12-15 15:21:52', '2019-12-15 15:21:52'),
(63, 'read_orders_details', 'orders_details', '2019-12-15 15:21:52', '2019-12-15 15:21:52'),
(64, 'edit_orders_details', 'orders_details', '2019-12-15 15:21:52', '2019-12-15 15:21:52'),
(65, 'add_orders_details', 'orders_details', '2019-12-15 15:21:52', '2019-12-15 15:21:52'),
(66, 'delete_orders_details', 'orders_details', '2019-12-15 15:21:52', '2019-12-15 15:21:52'),
(67, 'browse_products', 'products', '2019-12-15 15:22:04', '2019-12-15 15:22:04'),
(68, 'read_products', 'products', '2019-12-15 15:22:04', '2019-12-15 15:22:04'),
(69, 'edit_products', 'products', '2019-12-15 15:22:04', '2019-12-15 15:22:04'),
(70, 'add_products', 'products', '2019-12-15 15:22:04', '2019-12-15 15:22:04'),
(71, 'delete_products', 'products', '2019-12-15 15:22:04', '2019-12-15 15:22:04'),
(77, 'browse_sellers', 'sellers', '2019-12-15 15:22:14', '2019-12-15 15:22:14'),
(78, 'read_sellers', 'sellers', '2019-12-15 15:22:14', '2019-12-15 15:22:14'),
(79, 'edit_sellers', 'sellers', '2019-12-15 15:22:14', '2019-12-15 15:22:14'),
(80, 'add_sellers', 'sellers', '2019-12-15 15:22:14', '2019-12-15 15:22:14'),
(81, 'delete_sellers', 'sellers', '2019-12-15 15:22:14', '2019-12-15 15:22:14'),
(82, 'browse_sliders', 'sliders', '2019-12-15 15:22:38', '2019-12-15 15:22:38'),
(83, 'read_sliders', 'sliders', '2019-12-15 15:22:38', '2019-12-15 15:22:38'),
(84, 'edit_sliders', 'sliders', '2019-12-15 15:22:38', '2019-12-15 15:22:38'),
(85, 'add_sliders', 'sliders', '2019-12-15 15:22:38', '2019-12-15 15:22:38'),
(86, 'delete_sliders', 'sliders', '2019-12-15 15:22:38', '2019-12-15 15:22:38'),
(87, 'browse_sub_categories', 'sub_categories', '2019-12-15 15:22:43', '2019-12-15 15:22:43'),
(88, 'read_sub_categories', 'sub_categories', '2019-12-15 15:22:43', '2019-12-15 15:22:43'),
(89, 'edit_sub_categories', 'sub_categories', '2019-12-15 15:22:43', '2019-12-15 15:22:43'),
(90, 'add_sub_categories', 'sub_categories', '2019-12-15 15:22:43', '2019-12-15 15:22:43'),
(91, 'delete_sub_categories', 'sub_categories', '2019-12-15 15:22:43', '2019-12-15 15:22:43'),
(117, 'browse_customer_rate', 'customer_rate', '2019-12-15 17:48:19', '2019-12-15 17:48:19'),
(118, 'read_customer_rate', 'customer_rate', '2019-12-15 17:48:19', '2019-12-15 17:48:19'),
(119, 'edit_customer_rate', 'customer_rate', '2019-12-15 17:48:19', '2019-12-15 17:48:19'),
(120, 'add_customer_rate', 'customer_rate', '2019-12-15 17:48:19', '2019-12-15 17:48:19'),
(121, 'delete_customer_rate', 'customer_rate', '2019-12-15 17:48:19', '2019-12-15 17:48:19');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(117, 1),
(118, 1),
(119, 1),
(120, 1),
(121, 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `available` int(11) NOT NULL DEFAULT '1',
  `sub_category_id` int(11) NOT NULL,
  `image1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image5` varchar(257) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seller_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2019-12-15 13:02:13', '2019-12-15 13:02:13'),
(2, 'user', 'Normal User', '2019-12-15 13:02:14', '2019-12-15 13:02:14');

-- --------------------------------------------------------

--
-- Table structure for table `sellers`
--

CREATE TABLE `sellers` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `story_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `story_body` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `story_image1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `story_image2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `story_image3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', 'settings\\December2019\\6TKxaSE20lW5jSUuFW0a.png', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'productive family', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'The productive family website is the first online platform for productive families', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', 'settings\\December2019\\DB0Egkb5ZY4PNa22iy7s.png', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', 'settings\\December2019\\bBtrzSmOvYppIMO9g4Sw.png', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `button_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `button_action` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `View` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `main_category_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'mohammad imwas', 'admin@admin.com', 'users/default.png', NULL, '$2y$10$j48conB0W2Hp/7XWqNSgnenlzx5ua9EUBWdHoETUc.68iEpysVV1i', NULL, NULL, '2019-12-15 13:03:02', '2019-12-15 13:03:02');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bids`
--
ALTER TABLE `bids`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_rate`
--
ALTER TABLE `customer_rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_categories`
--
ALTER TABLE `main_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders_details`
--
ALTER TABLE `orders_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `sellers`
--
ALTER TABLE `sellers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bids`
--
ALTER TABLE `bids`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customer_rate`
--
ALTER TABLE `customer_rate`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `main_categories`
--
ALTER TABLE `main_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders_details`
--
ALTER TABLE `orders_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sellers`
--
ALTER TABLE `sellers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
