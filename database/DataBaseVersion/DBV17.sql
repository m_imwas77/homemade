-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 18, 2020 at 03:04 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `homemade`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `available` int(11) NOT NULL DEFAULT 1,
  `sub_category_id` int(11) NOT NULL,
  `images` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `seller_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `number_store` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `available`, `sub_category_id`, `images`, `seller_id`, `created_at`, `updated_at`, `number_store`) VALUES
(2, 'اكسسوارات', 15, 1, 22, '[\"products\\\\March2020\\\\NtV4GW1Ve4FdNkX7fPhJ.jpg\"]', 3, '2020-03-05 08:04:43', '2020-03-26 17:47:02', 0),
(3, 'كنب', 1000, 1, 1, '[\"products\\\\March2020\\\\BvOnKL1tNX1wVrku3aGl.jpg\",\"products\\\\March2020\\\\ynoDdSrzbny8wRVUbdc5.jpg\",\"products\\\\March2020\\\\9S5VHEzkin5RugaocnK0.jpg\"]', 4, '2020-03-05 08:05:09', '2020-03-05 08:05:09', 0),
(4, 'اجهزة منزلية', 30000, 1, 3, '[\"products\\\\March2020\\\\JiS4VHUvP9NbF2OqnFMb.jpeg\"]', 6, '2020-03-05 08:05:49', '2020-03-05 08:05:49', 0),
(5, 'سفيحا', 2, 1, 1, '[\"products\\\\March2020\\\\byuDAg01b1kRay3UmX7V.jpg\",\"products\\\\March2020\\\\D4BN7nv7WKxhuTVSWgvZ.jpg\",\"products\\\\March2020\\\\DUkANKeir70JTvP7sSOF.jpg\",\"products\\\\March2020\\\\UVhMWIVqNggyIgf77Wab.jpg\"]', 5, '2020-03-05 08:23:00', '2020-03-08 08:26:29', 0),
(6, 'سبانخ', 5, 1, 2, '[\"products\\\\March2020\\\\mskC5H8VTtqe5VD5Kw1U.jpg\"]', 20, '2020-03-05 08:24:00', '2020-04-18 10:01:16', 1),
(7, 'حفر على الخشب', 120, 0, 1, '[\"products\\\\March2020\\\\pO7lCh2oi3INcduoFdcG.png\",\"products\\\\March2020\\\\wT9DWrUgdvgMa2bVOZYO.PNG\",\"products\\\\March2020\\\\3MjlRx5ttnn56tzFYP0y.png\"]', 22, '2020-03-21 12:48:58', '2020-03-25 12:10:45', 0),
(10, 'asd', 12, 1, -1, '[\"products\\\\March2020\\\\7cfkMK1qtMyBMyfnYzLP.png\",\"products\\\\March2020\\\\EV9ZwYoiBnoFwIO1LG6L.png\",\"products\\\\March2020\\\\zZTMfOK83wbEEX6Tn4Vn.png\",\"products\\\\March2020\\\\ZP2TfdH3IIZDKCq25ET2.png\"]', 22, '2020-03-26 17:43:40', '2020-03-26 17:43:40', 0),
(11, 'asd', 120, 1, -1, '[\"products\\\\March2020\\\\EQUKU73DI5Oqkf3PxZMW.png\",\"products\\\\March2020\\\\IhhIch6ugU5WaUUEpXmp.jpg\",\"products\\\\March2020\\\\NzWHGbTKa3uCUy3cC1Ez.jpg\",\"products\\\\March2020\\\\QLfmAKSyB0VgxeK6w9rI.PNG\",\"products\\\\March2020\\\\StHDc4oiF8YYAxqJzcUB.png\",\"products\\\\March2020\\\\jO2FqZ5D6uHsalOCg0Wn.png\"]', 22, '2020-03-26 17:46:31', '2020-03-26 17:46:31', 0),
(12, 'asd', 123, 1, -1, '[\"products\\\\March2020\\\\SRC8Sil3sQUCDCH6IHwW.jpg\",\"products\\\\March2020\\\\d9e8tDO4HHqjRdACFD8d.jpg\",\"products\\\\March2020\\\\H55IM1E2kgOvZUNunw6D.PNG\",\"products\\\\March2020\\\\UJzD7dGm5tpbBfiExCAE.png\",\"products\\\\March2020\\\\E7LGB1jjrawNyGXn61fF.png\"]', 22, '2020-03-26 17:49:46', '2020-03-26 17:49:46', 0),
(13, 'AS', 123, 1, 24, '[\"products\\\\March2020\\\\ewYzyHoGv5748UYInPAZ.jpg\",\"products\\\\March2020\\\\PYmiNEVvDRhipprEWtJa.jpg\",\"products\\\\March2020\\\\lmE5pZa9R6eXGUQsGInI.PNG\",\"products\\\\March2020\\\\y2FVSyP4JtPOX1UWSlSn.png\",\"products\\\\March2020\\\\abk0ekReUpYZFj8e6fnU.png\",\"products\\\\April2020\\\\GKTIUjyNltoMEFoBvTex.jpg\",\"products\\\\April2020\\\\sIFqRtnj4vNgOihGkLMN.jpg\",\"products\\\\April2020\\\\8IFUbda5Sjw5rcFtMurh.jpg\",\"products\\\\April2020\\\\vYsS3E79P6X1rJLaWIFS.jpg\",\"products\\\\April2020\\\\od5Zv1HwqsiKS1liZoBY.jpg\",\"products\\\\April2020\\\\oO7ox6T3tQgdCZiM0ob8.jpg\",\"products\\\\April2020\\\\LaIvtL5cT1mkIMKRytkM.jpg\",\"products\\\\April2020\\\\T9Lo7y7FQH91l6pdKTZx.jpg\"]', 22, '2020-03-26 17:53:41', '2020-04-06 08:50:22', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
