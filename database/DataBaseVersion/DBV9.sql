-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 11, 2020 at 09:43 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `homemade`
--

-- --------------------------------------------------------

--
-- Table structure for table `bids`
--

CREATE TABLE `bids` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `price` double NOT NULL,
  `number_of_day` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bids`
--

INSERT INTO `bids` (`id`, `order_id`, `seller_id`, `price`, `number_of_day`, `description`, `created_at`, `updated_at`) VALUES
(4, 5, 4, 123, 123, '21312312', '2020-02-14 15:34:40', '2020-02-14 16:35:22'),
(7, 6, 5, 12, 21, '12', '2020-02-21 14:00:34', '2020-02-21 14:00:34'),
(8, 8, 5, 150, 12, 'لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود تيمبور أنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا . يوت انيم أد مينيم فينايم,', '2020-03-03 06:35:20', '2020-03-03 06:35:20'),
(10, 8, 6, 123, 123, '12312312', '2020-03-03 07:01:48', '2020-03-03 07:01:48'),
(11, 11, 7, 150, 8, 'jghgf', '2020-03-05 08:57:16', '2020-03-05 08:57:16');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `customer_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `user_id`, `created_at`, `updated_at`, `customer_name`) VALUES
(3, 14, '2020-02-05 07:00:00', '2020-02-05 07:01:11', 'Mohammad Qawasmeh'),
(4, 18, '2020-02-11 06:36:00', '2020-02-11 06:37:01', 'mohammad imwas'),
(5, 21, '2020-03-03 06:13:59', '2020-03-03 06:13:59', 'زبون زببين'),
(6, 24, '2020-03-05 08:25:32', '2020-03-05 08:25:32', 'مالك التميمي'),
(7, 25, '2020-03-08 07:15:50', '2020-03-08 07:15:50', 'mohammad qawasmeh');

-- --------------------------------------------------------

--
-- Table structure for table `customer_rate`
--

CREATE TABLE `customer_rate` (
  `id` int(10) UNSIGNED NOT NULL,
  `rate` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `browse` tinyint(1) NOT NULL DEFAULT 1,
  `read` tinyint(1) NOT NULL DEFAULT 1,
  `edit` tinyint(1) NOT NULL DEFAULT 1,
  `add` tinyint(1) NOT NULL DEFAULT 1,
  `delete` tinyint(1) NOT NULL DEFAULT 1,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
(3, 1, 'email', 'text', 'Email', 0, 1, 1, 1, 1, 1, '{}', 4),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, '{}', 8),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, '{}', 9),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, '{}', 18),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 19),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, '{}', 11),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\",\"taggable\":\"0\"}', 20),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 21),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, '{}', 13),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, '{}', 5),
(21, 1, 'role_id', 'text', 'Role', 0, 0, 1, 1, 1, 1, '{}', 12),
(22, 4, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 0),
(23, 4, 'order_id', 'hidden', 'Order Id', 1, 0, 0, 1, 1, 1, '{}', 3),
(24, 4, 'seller_id', 'hidden', 'Seller Id', 1, 0, 0, 1, 1, 1, '{}', 5),
(25, 4, 'price', 'text', 'Price', 1, 1, 1, 1, 1, 1, '{}', 6),
(26, 4, 'number_of_day', 'text', 'Number Of Day', 1, 1, 1, 1, 1, 1, '{}', 7),
(27, 4, 'description', 'text', 'Description', 0, 1, 1, 1, 1, 1, '{}', 8),
(28, 4, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 9),
(29, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 10),
(30, 6, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(31, 6, 'user_id', 'hidden', 'User Id', 0, 0, 0, 1, 1, 1, '{}', 3),
(32, 6, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(33, 6, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(34, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(35, 7, 'log', 'text', 'Log', 1, 1, 1, 1, 1, 1, '{}', 3),
(36, 7, 'user_id', 'text', 'User Id', 1, 1, 1, 1, 1, 1, '{}', 4),
(37, 7, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(38, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(39, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(40, 8, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(41, 8, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 3),
(42, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(43, 9, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(44, 9, 'message', 'text', 'Message', 0, 1, 1, 1, 1, 1, '{}', 2),
(45, 9, 'sender_id', 'hidden', 'Sender Id', 0, 1, 1, 1, 1, 1, '{}', 4),
(46, 9, 'resever_id', 'hidden', 'Resever Id', 0, 1, 1, 1, 1, 1, '{}', 6),
(47, 9, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
(48, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(49, 10, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(50, 10, 'customer_id', 'text', 'Customer Id', 1, 1, 1, 1, 1, 1, '{}', 4),
(51, 10, 'seller_id', 'text', 'Seller Id', 0, 1, 1, 1, 1, 1, '{}', 6),
(52, 10, 'min_price', 'number', 'Min Price', 1, 1, 1, 1, 1, 1, '{}', 7),
(53, 10, 'max_price', 'number', 'Max Price', 1, 1, 1, 1, 1, 1, '{}', 8),
(54, 10, 'final_price', 'number', 'Final Price', 0, 1, 1, 1, 1, 1, '{}', 9),
(55, 10, 'start_date', 'date', 'Start Date', 1, 1, 1, 1, 1, 1, '{}', 10),
(56, 10, 'end_date', 'date', 'End Date', 1, 1, 1, 1, 1, 1, '{}', 11),
(57, 10, 'description', 'text', 'Description', 0, 1, 1, 1, 1, 1, '{}', 12),
(58, 10, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 14),
(59, 10, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 15),
(60, 11, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 0),
(61, 11, 'order_id', 'text', 'Order Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(62, 11, 'product_id', 'text', 'Product Id', 0, 1, 1, 1, 1, 1, '{}', 5),
(63, 11, 'quantity', 'text', 'Quantity', 1, 1, 1, 1, 1, 1, '{}', 6),
(64, 11, 'price', 'text', 'Price', 0, 1, 1, 1, 1, 1, '{}', 7),
(65, 11, 'type', 'text', 'Type', 1, 1, 1, 1, 1, 1, '{}', 8),
(66, 11, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 9),
(67, 11, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 10),
(68, 12, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(69, 12, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(70, 12, 'price', 'text', 'Price', 1, 1, 1, 1, 1, 1, '{}', 3),
(71, 12, 'available', 'checkbox', 'Available', 1, 1, 1, 1, 1, 1, '{}', 4),
(72, 12, 'sub_category_id', 'text', 'Sub Category Id', 1, 1, 1, 1, 1, 1, '{}', 6),
(78, 12, 'seller_id', 'hidden', 'Seller Id', 1, 1, 1, 1, 1, 1, '{}', 13),
(79, 12, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 14),
(80, 12, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 15),
(81, 14, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(82, 14, 'user_id', 'hidden', 'User Id', 1, 0, 0, 1, 1, 1, '{}', 3),
(84, 14, 'story_body', 'rich_text_box', 'Story', 1, 1, 1, 1, 1, 1, '{}', 5),
(88, 14, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 9),
(89, 14, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 10),
(90, 15, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(91, 15, 'title', 'rich_text_box', 'Title', 1, 1, 1, 1, 1, 1, '{}', 2),
(92, 15, 'body', 'rich_text_box', 'Body', 1, 1, 1, 1, 1, 1, '{}', 3),
(93, 15, 'image', 'image', 'Image', 1, 1, 1, 1, 1, 1, '{}', 4),
(94, 15, 'button_title', 'text', 'Button Title', 0, 1, 1, 1, 1, 1, '{}', 5),
(95, 15, 'button_action', 'text', 'Button Action', 0, 1, 1, 1, 1, 1, '{}', 6),
(96, 15, 'View', 'checkbox', 'View', 1, 1, 1, 1, 1, 1, '{}', 7),
(97, 15, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(98, 15, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(99, 16, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(100, 16, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, '{}', 2),
(101, 16, 'main_category_id', 'text', 'Main Category Id', 0, 1, 1, 1, 1, 1, '{}', 4),
(102, 16, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(103, 16, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(104, 7, 'log_belongsto_user_relationship', 'relationship', 'users', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"first_name\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(106, 9, 'message_belongsto_user_relationship_1', 'relationship', 'Resever', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"resever_id\",\"key\":\"id\",\"label\":\"first_name\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":\"0\"}', 5),
(109, 16, 'sub_category_belongsto_main_category_relationship', 'relationship', 'main_categories', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\MainCategory\",\"table\":\"main_categories\",\"type\":\"belongsTo\",\"column\":\"main_category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":\"0\"}', 3),
(110, 12, 'product_belongsto_sub_category_relationship', 'relationship', 'sub_categories', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\SubCategory\",\"table\":\"sub_categories\",\"type\":\"belongsTo\",\"column\":\"sub_category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":\"0\"}', 5),
(111, 12, 'product_belongsto_seller_relationship', 'relationship', 'sellers', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Seller\",\"table\":\"sellers\",\"type\":\"belongsTo\",\"column\":\"seller_id\",\"key\":\"id\",\"label\":\"seller_name\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":\"0\"}', 12),
(112, 11, 'orders_detail_belongsto_product_relationship', 'relationship', 'products', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Product\",\"table\":\"products\",\"type\":\"belongsTo\",\"column\":\"product_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":\"0\"}', 4),
(113, 11, 'orders_detail_belongsto_order_relationship', 'relationship', 'orders', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Order\",\"table\":\"orders\",\"type\":\"belongsTo\",\"column\":\"order_id\",\"key\":\"id\",\"label\":\"description\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(114, 10, 'order_belongsto_customer_relationship', 'relationship', 'customers', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Customer\",\"table\":\"customers\",\"type\":\"belongsTo\",\"column\":\"customer_id\",\"key\":\"user_id\",\"label\":\"customer_name\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":\"0\"}', 3),
(115, 10, 'order_belongsto_seller_relationship', 'relationship', 'sellers', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Seller\",\"table\":\"sellers\",\"type\":\"belongsTo\",\"column\":\"seller_id\",\"key\":\"user_id\",\"label\":\"seller_name\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":\"0\"}', 5),
(116, 4, 'bid_belongsto_seller_relationship', 'relationship', 'sellers', 0, 0, 0, 1, 1, 1, '{\"model\":\"App\\\\Seller\",\"table\":\"sellers\",\"type\":\"belongsTo\",\"column\":\"seller_id\",\"key\":\"user_id\",\"label\":\"seller_name\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":\"0\"}', 4),
(117, 4, 'bid_belongsto_order_relationship', 'relationship', 'orders', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Order\",\"table\":\"orders\",\"type\":\"belongsTo\",\"column\":\"order_id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(118, 23, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
(119, 23, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(120, 23, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 3),
(121, 23, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(122, 1, 'first_name', 'text', 'First Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(123, 1, 'email_verified_at', 'timestamp', 'Email Verified At', 0, 0, 0, 0, 0, 1, '{}', 10),
(124, 1, 'last_name', 'text', 'Last Name', 0, 1, 1, 1, 1, 1, '{}', 3),
(125, 1, 'phone_number', 'text', 'Phone Number', 0, 1, 1, 1, 1, 1, '{}', 6),
(126, 1, 'user_name', 'text', 'User Name', 0, 1, 1, 1, 1, 1, '{}', 5),
(127, 1, 'country', 'text', 'Country', 0, 0, 1, 1, 1, 1, '{}', 14),
(128, 1, 'city', 'text', 'City', 0, 0, 1, 1, 1, 1, '{}', 15),
(129, 1, 'street', 'text', 'Street', 0, 0, 1, 1, 1, 1, '{}', 16),
(130, 1, 'gender', 'select_dropdown', 'Gender', 0, 0, 1, 1, 1, 1, '{\"default\":\"male\",\"options\":{\"male\":\"male\",\"female\":\"female\"}}', 7),
(131, 1, 'active', 'checkbox', 'Active', 0, 1, 1, 1, 1, 1, '{}', 17),
(132, 6, 'customer_name', 'hidden', 'Customer Name', 0, 0, 0, 1, 1, 1, '{}', 5),
(133, 14, 'seller_name', 'hidden', 'Seller Name', 0, 0, 0, 1, 1, 1, '{}', 6),
(134, 22, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(135, 22, 'rate', 'text', 'Rate', 1, 1, 1, 1, 1, 1, '{}', 2),
(136, 22, 'description', 'text', 'Description', 1, 1, 1, 1, 1, 1, '{}', 3),
(137, 22, 'customer_id', 'text', 'Customer Id', 1, 1, 1, 1, 1, 1, '{}', 4),
(138, 22, 'seller_id', 'text', 'Seller Id', 1, 1, 1, 1, 1, 1, '{}', 5),
(139, 22, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(140, 22, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(141, 9, 'message_belongsto_user_relationship', 'relationship', 'Sender', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"sender_id\",\"key\":\"id\",\"label\":\"first_name\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":\"0\"}', 3),
(142, 10, 'number_of_days', 'number', 'Number Of Days', 1, 1, 1, 1, 1, 1, '{}', 13),
(143, 10, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 2),
(144, 10, 'accepted', 'text', 'Accepted', 0, 1, 1, 1, 1, 1, '{}', 14),
(145, 12, 'images', 'multiple_images', 'Images', 1, 1, 1, 1, 1, 1, '{}', 6),
(148, 25, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(149, 25, 'customer_id', 'number', 'Customer Id', 0, 1, 1, 1, 1, 1, '{}', 2),
(150, 25, 'type', 'text', 'Type', 0, 1, 1, 1, 1, 1, '{}', 3),
(151, 25, 'type_id', 'number', 'Type Id', 0, 1, 1, 1, 1, 1, '{}', 4),
(152, 25, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(153, 25, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(154, 25, 'favorite_belongsto_customer_relationship', 'relationship', 'customers', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Customer\",\"table\":\"customers\",\"type\":\"belongsTo\",\"column\":\"customer_id\",\"key\":\"user_id\",\"label\":\"customer_name\",\"pivot_table\":\"bids\",\"pivot\":\"0\",\"taggable\":null}', 7);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT 0,
  `server_side` tinyint(4) NOT NULL DEFAULT 0,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'App\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":\"users\"}', '2019-12-15 13:02:12', '2020-02-05 07:29:57'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2019-12-15 13:02:12', '2020-02-05 06:35:11'),
(4, 'bids', 'bids', 'Bid', 'Bids', 'voyager-wallet', 'App\\Bid', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":\"bids\"}', '2019-12-15 15:19:31', '2020-03-03 14:28:31'),
(6, 'customers', 'customers', 'Customer', 'Customers', NULL, 'App\\Customer', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":\"customers\"}', '2019-12-15 15:20:26', '2020-02-05 07:31:11'),
(7, 'logs', 'logs', 'Log', 'Logs', NULL, 'App\\Log', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-12-15 15:20:32', '2020-02-05 06:34:07'),
(8, 'main_categories', 'main-categories', 'Main Category', 'Main Categories', NULL, 'App\\MainCategory', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-12-15 15:20:40', '2020-02-05 06:34:15'),
(9, 'messages', 'messages', 'Message', 'Messages', NULL, 'App\\Message', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":\"messages\"}', '2019-12-15 15:21:04', '2020-02-05 07:36:23'),
(10, 'orders', 'orders', 'Order', 'Orders', NULL, 'App\\Order', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":\"orders\"}', '2019-12-15 15:21:44', '2020-03-03 14:32:39'),
(11, 'orders_details', 'orders-details', 'Orders Detail', 'Orders Details', NULL, 'App\\OrdersDetail', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":\"orderDetails\"}', '2019-12-15 15:21:52', '2020-02-05 06:34:46'),
(12, 'products', 'products', 'Product', 'Products', NULL, 'App\\Product', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":\"products\"}', '2019-12-15 15:22:03', '2020-03-03 08:48:30'),
(14, 'sellers', 'sellers', 'Seller', 'Sellers', NULL, 'App\\Seller', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":\"sellers\"}', '2019-12-15 15:22:14', '2020-02-05 07:31:25'),
(15, 'sliders', 'sliders', 'Slider', 'Sliders', NULL, 'App\\Slider', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-12-15 15:22:38', '2020-03-11 05:17:23'),
(16, 'sub_categories', 'sub-categories', 'Sub Category', 'Sub Categories', NULL, 'App\\SubCategory', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-12-15 15:22:42', '2020-02-05 06:35:34'),
(22, 'customer_rate', 'customer-rate', 'Customer Rate', 'Customer Rates', NULL, 'App\\CustomerRate', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-12-15 17:48:19', '2020-02-05 06:33:42'),
(23, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2020-01-31 05:05:56', '2020-02-05 06:34:21'),
(25, 'favorites', 'favorites', 'Favorite', 'Favorites', NULL, 'App\\Favorite', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-03-11 06:28:27', '2020-03-11 06:28:27');

-- --------------------------------------------------------

--
-- Table structure for table `favorites`
--

CREATE TABLE `favorites` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `favorites`
--

INSERT INTO `favorites` (`id`, `customer_id`, `type`, `type_id`, `created_at`, `updated_at`) VALUES
(3, 21, 'products', 6, '2020-03-11 06:31:00', '2020-03-11 06:33:39');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `log` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `main_categories`
--

CREATE TABLE `main_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `main_categories`
--

INSERT INTO `main_categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(7, 'الصنف الثاني', '2020-02-06 04:59:00', '2020-03-05 08:03:40'),
(8, 'الصنف الاول', '2020-02-07 11:04:00', '2020-03-05 08:03:02');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2019-12-15 13:02:13', '2019-12-15 13:02:13');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2019-12-15 13:02:13', '2019-12-15 13:02:13', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 4, '2019-12-15 13:02:13', '2020-02-05 06:40:53', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2019-12-15 13:02:13', '2019-12-15 13:02:13', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2019-12-15 13:02:13', '2019-12-15 13:02:13', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 5, '2019-12-15 13:02:13', '2020-02-05 06:40:53', NULL, NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2019-12-15 13:02:13', '2020-02-05 06:40:53', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2019-12-15 13:02:13', '2020-02-05 06:40:53', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2019-12-15 13:02:13', '2020-02-05 06:40:53', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 10, '2019-12-15 13:02:13', '2020-02-05 06:42:13', 'voyager.settings.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2019-12-15 13:02:17', '2020-02-05 06:40:53', 'voyager.hooks', NULL),
(12, 1, 'Bids', '', '_self', 'voyager-megaphone', '#000000', NULL, 11, '2019-12-15 15:19:32', '2020-02-05 06:42:11', 'voyager.bids.index', 'null'),
(14, 1, 'Customers', '', '_self', 'voyager-person', '#000000', NULL, 6, '2019-12-15 15:20:26', '2020-02-05 06:41:54', 'voyager.customers.index', 'null'),
(15, 1, 'Logs', '', '_self', 'voyager-logbook', '#000000', NULL, 12, '2019-12-15 15:20:32', '2020-02-05 06:42:11', 'voyager.logs.index', 'null'),
(16, 1, 'Main Categories', '', '_self', 'voyager-categories', '#000000', NULL, 8, '2019-12-15 15:20:40', '2020-02-05 06:42:05', 'voyager.main-categories.index', 'null'),
(17, 1, 'Messages', '', '_self', 'voyager-chat', '#000000', NULL, 13, '2019-12-15 15:21:05', '2020-02-05 06:42:11', 'voyager.messages.index', 'null'),
(18, 1, 'Orders', '', '_self', 'voyager-basket', '#000000', NULL, 14, '2019-12-15 15:21:44', '2020-02-05 06:42:11', 'voyager.orders.index', 'null'),
(19, 1, 'Orders Details', '', '_self', 'voyager-basket', '#000000', NULL, 15, '2019-12-15 15:21:52', '2020-02-05 06:42:11', 'voyager.orders-details.index', 'null'),
(20, 1, 'Products', '', '_self', 'voyager-truck', '#000000', NULL, 16, '2019-12-15 15:22:04', '2020-02-05 06:42:11', 'voyager.products.index', 'null'),
(22, 1, 'Sellers', '', '_self', 'voyager-person', '#000000', NULL, 7, '2019-12-15 15:22:14', '2020-02-05 06:41:58', 'voyager.sellers.index', 'null'),
(23, 1, 'Sliders', '', '_self', 'voyager-new', '#000000', NULL, 17, '2019-12-15 15:22:38', '2020-02-05 06:42:11', 'voyager.sliders.index', 'null'),
(24, 1, 'Sub Categories', '', '_self', 'voyager-categories', '#000000', NULL, 9, '2019-12-15 15:22:43', '2020-02-05 06:42:13', 'voyager.sub-categories.index', 'null'),
(30, 1, 'Customer Rates', '', '_self', 'voyager-star-two', '#000000', NULL, 18, '2019-12-15 17:48:19', '2020-02-05 06:40:53', 'voyager.customer-rate.index', 'null'),
(31, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2020-01-31 05:05:56', '2020-02-05 06:40:53', 'voyager.menus.index', NULL),
(32, 1, 'Favorites', '', '_self', NULL, NULL, NULL, 19, '2020-03-11 06:28:27', '2020-03-11 06:28:27', 'voyager.favorites.index', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `message` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sender_id` bigint(20) DEFAULT NULL,
  `resever_id` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `message`, `sender_id`, `resever_id`, `created_at`, `updated_at`) VALUES
(1, 'Hello', 1, 17, '2020-02-05 07:36:48', '2020-02-05 07:36:48');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` bigint(20) NOT NULL,
  `seller_id` bigint(20) DEFAULT NULL,
  `min_price` double NOT NULL,
  `max_price` double NOT NULL,
  `final_price` double DEFAULT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `number_of_days` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `accepted` tinyint(4) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `customer_id`, `seller_id`, `min_price`, `max_price`, `final_price`, `start_date`, `end_date`, `created_at`, `updated_at`, `number_of_days`, `title`, `description`, `accepted`) VALUES
(3, 4, -1, 112, 12, NULL, '0012-12-12', '0001-12-12', '2020-02-11 17:27:25', '2020-02-11 17:27:25', 12, 'adsas', NULL, 0),
(4, 4, -1, 123, 12, NULL, '0012-12-12', '0012-12-12', '2020-02-11 17:27:54', '2020-02-11 17:27:54', 121, 'asds', NULL, 0),
(5, 4, -1, 1221222, 123123, NULL, '2020-03-10', '2020-02-03', NULL, '2020-02-12 16:37:22', 6, 'test123123', NULL, 0),
(6, 4, -1, 100, 350, NULL, '2020-02-18', '2020-02-19', '2020-02-18 16:26:12', '2020-02-18 16:26:12', 1, 'اريد عمل اشاياء لعزومه', NULL, 0),
(8, 5, 20, 150, 250, NULL, '2020-03-03', '2020-03-07', '2020-03-03 06:18:40', '2020-03-03 07:34:16', 30, 'هاذا اول طلبيه عامه سوف اقوم بعملها الان', NULL, 1),
(9, 5, -1, 123, 123, NULL, '2020-03-01', '2020-03-02', '2020-03-03 08:05:31', '2020-03-03 08:05:31', 123, 'اطلبها الان', NULL, NULL),
(10, 5, -1, 444, 4414, NULL, '2020-03-03', '2020-03-07', NULL, '2020-03-03 08:29:01', 12, 'تجربه 1010', NULL, NULL),
(11, 6, -1, 100, 200, NULL, '2020-03-05', '2020-03-06', '2020-03-05 08:37:47', '2020-03-05 08:37:47', 10, 'معجنات', NULL, NULL),
(12, 1, NULL, 5, 5, 5, '2020-03-11', '2020-03-21', '2020-03-11 06:05:41', '2020-03-11 06:05:41', 10, 'سبانخ', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `orders_details`
--

CREATE TABLE `orders_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `sub_category_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders_details`
--

INSERT INTO `orders_details` (`id`, `order_id`, `sub_category_id`, `created_at`, `updated_at`, `name`, `desc`) VALUES
(3, 5, 2, '2020-02-12 16:37:36', '2020-02-12 16:41:01', '13123', 'asdasd12312312312'),
(4, 6, 1, '2020-02-18 16:26:12', '2020-02-18 16:26:12', 'مقلوبه', 'مقلوبه مع جاد'),
(5, 6, 3, '2020-02-18 16:26:12', '2020-02-18 16:26:12', 'بيتزا', 'بيتزا حجم صغبر بعنب سناك و اشي زي هيك'),
(6, 8, 1, '2020-03-03 06:18:40', '2020-03-03 06:18:40', 'المنتج الاول', 'لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود تيمبور\r\n\r\nأنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا . يوت انيم أد مينيم فينايم,'),
(7, 8, 2, '2020-03-03 06:18:40', '2020-03-03 06:18:40', 'النتج الثاني', 'لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود تيمبور\r\n\r\nأنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا . يوت انيم أد مينيم فينايم,'),
(8, 8, 3, '2020-03-03 06:18:40', '2020-03-03 06:18:40', 'المنتج الثاني', 'لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود تيمبور\r\n\r\nأنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا . يوت انيم أد مينيم فينايم,'),
(9, 8, 11, '2020-03-03 06:18:40', '2020-03-03 06:18:40', 'المنتج الرابع', 'لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود تيمبور\r\n\r\nأنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا . يوت انيم أد مينيم فينايم,'),
(10, 10, 2, '2020-03-03 08:29:00', '2020-03-03 08:29:00', 'qwewqe', 'wqe wqe qwe qweqw eqwe qweqw'),
(11, 11, 2, '2020-03-05 08:37:47', '2020-03-05 08:37:47', 'سفيحه', 'سفيحه بالطحينه');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2019-12-15 13:02:14', '2019-12-15 13:02:14'),
(2, 'browse_bread', NULL, '2019-12-15 13:02:14', '2019-12-15 13:02:14'),
(3, 'browse_database', NULL, '2019-12-15 13:02:14', '2019-12-15 13:02:14'),
(4, 'browse_media', NULL, '2019-12-15 13:02:14', '2019-12-15 13:02:14'),
(5, 'browse_compass', NULL, '2019-12-15 13:02:14', '2019-12-15 13:02:14'),
(11, 'browse_roles', 'roles', '2019-12-15 13:02:14', '2019-12-15 13:02:14'),
(12, 'read_roles', 'roles', '2019-12-15 13:02:14', '2019-12-15 13:02:14'),
(13, 'edit_roles', 'roles', '2019-12-15 13:02:14', '2019-12-15 13:02:14'),
(14, 'add_roles', 'roles', '2019-12-15 13:02:14', '2019-12-15 13:02:14'),
(15, 'delete_roles', 'roles', '2019-12-15 13:02:14', '2019-12-15 13:02:14'),
(16, 'browse_users', 'users', '2019-12-15 13:02:14', '2019-12-15 13:02:14'),
(17, 'read_users', 'users', '2019-12-15 13:02:15', '2019-12-15 13:02:15'),
(18, 'edit_users', 'users', '2019-12-15 13:02:15', '2019-12-15 13:02:15'),
(19, 'add_users', 'users', '2019-12-15 13:02:15', '2019-12-15 13:02:15'),
(20, 'delete_users', 'users', '2019-12-15 13:02:15', '2019-12-15 13:02:15'),
(21, 'browse_settings', 'settings', '2019-12-15 13:02:15', '2019-12-15 13:02:15'),
(22, 'read_settings', 'settings', '2019-12-15 13:02:15', '2019-12-15 13:02:15'),
(23, 'edit_settings', 'settings', '2019-12-15 13:02:15', '2019-12-15 13:02:15'),
(24, 'add_settings', 'settings', '2019-12-15 13:02:15', '2019-12-15 13:02:15'),
(25, 'delete_settings', 'settings', '2019-12-15 13:02:15', '2019-12-15 13:02:15'),
(26, 'browse_hooks', NULL, '2019-12-15 13:02:17', '2019-12-15 13:02:17'),
(27, 'browse_bids', 'bids', '2019-12-15 15:19:31', '2019-12-15 15:19:31'),
(28, 'read_bids', 'bids', '2019-12-15 15:19:32', '2019-12-15 15:19:32'),
(29, 'edit_bids', 'bids', '2019-12-15 15:19:32', '2019-12-15 15:19:32'),
(30, 'add_bids', 'bids', '2019-12-15 15:19:32', '2019-12-15 15:19:32'),
(31, 'delete_bids', 'bids', '2019-12-15 15:19:32', '2019-12-15 15:19:32'),
(37, 'browse_customers', 'customers', '2019-12-15 15:20:26', '2019-12-15 15:20:26'),
(38, 'read_customers', 'customers', '2019-12-15 15:20:26', '2019-12-15 15:20:26'),
(39, 'edit_customers', 'customers', '2019-12-15 15:20:26', '2019-12-15 15:20:26'),
(40, 'add_customers', 'customers', '2019-12-15 15:20:26', '2019-12-15 15:20:26'),
(41, 'delete_customers', 'customers', '2019-12-15 15:20:26', '2019-12-15 15:20:26'),
(42, 'browse_logs', 'logs', '2019-12-15 15:20:32', '2019-12-15 15:20:32'),
(43, 'read_logs', 'logs', '2019-12-15 15:20:32', '2019-12-15 15:20:32'),
(44, 'edit_logs', 'logs', '2019-12-15 15:20:32', '2019-12-15 15:20:32'),
(45, 'add_logs', 'logs', '2019-12-15 15:20:32', '2019-12-15 15:20:32'),
(46, 'delete_logs', 'logs', '2019-12-15 15:20:32', '2019-12-15 15:20:32'),
(47, 'browse_main_categories', 'main_categories', '2019-12-15 15:20:40', '2019-12-15 15:20:40'),
(48, 'read_main_categories', 'main_categories', '2019-12-15 15:20:40', '2019-12-15 15:20:40'),
(49, 'edit_main_categories', 'main_categories', '2019-12-15 15:20:40', '2019-12-15 15:20:40'),
(50, 'add_main_categories', 'main_categories', '2019-12-15 15:20:40', '2019-12-15 15:20:40'),
(51, 'delete_main_categories', 'main_categories', '2019-12-15 15:20:40', '2019-12-15 15:20:40'),
(52, 'browse_messages', 'messages', '2019-12-15 15:21:04', '2019-12-15 15:21:04'),
(53, 'read_messages', 'messages', '2019-12-15 15:21:04', '2019-12-15 15:21:04'),
(54, 'edit_messages', 'messages', '2019-12-15 15:21:04', '2019-12-15 15:21:04'),
(55, 'add_messages', 'messages', '2019-12-15 15:21:04', '2019-12-15 15:21:04'),
(56, 'delete_messages', 'messages', '2019-12-15 15:21:04', '2019-12-15 15:21:04'),
(57, 'browse_orders', 'orders', '2019-12-15 15:21:44', '2019-12-15 15:21:44'),
(58, 'read_orders', 'orders', '2019-12-15 15:21:44', '2019-12-15 15:21:44'),
(59, 'edit_orders', 'orders', '2019-12-15 15:21:44', '2019-12-15 15:21:44'),
(60, 'add_orders', 'orders', '2019-12-15 15:21:44', '2019-12-15 15:21:44'),
(61, 'delete_orders', 'orders', '2019-12-15 15:21:44', '2019-12-15 15:21:44'),
(62, 'browse_orders_details', 'orders_details', '2019-12-15 15:21:52', '2019-12-15 15:21:52'),
(63, 'read_orders_details', 'orders_details', '2019-12-15 15:21:52', '2019-12-15 15:21:52'),
(64, 'edit_orders_details', 'orders_details', '2019-12-15 15:21:52', '2019-12-15 15:21:52'),
(65, 'add_orders_details', 'orders_details', '2019-12-15 15:21:52', '2019-12-15 15:21:52'),
(66, 'delete_orders_details', 'orders_details', '2019-12-15 15:21:52', '2019-12-15 15:21:52'),
(67, 'browse_products', 'products', '2019-12-15 15:22:04', '2019-12-15 15:22:04'),
(68, 'read_products', 'products', '2019-12-15 15:22:04', '2019-12-15 15:22:04'),
(69, 'edit_products', 'products', '2019-12-15 15:22:04', '2019-12-15 15:22:04'),
(70, 'add_products', 'products', '2019-12-15 15:22:04', '2019-12-15 15:22:04'),
(71, 'delete_products', 'products', '2019-12-15 15:22:04', '2019-12-15 15:22:04'),
(77, 'browse_sellers', 'sellers', '2019-12-15 15:22:14', '2019-12-15 15:22:14'),
(78, 'read_sellers', 'sellers', '2019-12-15 15:22:14', '2019-12-15 15:22:14'),
(79, 'edit_sellers', 'sellers', '2019-12-15 15:22:14', '2019-12-15 15:22:14'),
(80, 'add_sellers', 'sellers', '2019-12-15 15:22:14', '2019-12-15 15:22:14'),
(81, 'delete_sellers', 'sellers', '2019-12-15 15:22:14', '2019-12-15 15:22:14'),
(82, 'browse_sliders', 'sliders', '2019-12-15 15:22:38', '2019-12-15 15:22:38'),
(83, 'read_sliders', 'sliders', '2019-12-15 15:22:38', '2019-12-15 15:22:38'),
(84, 'edit_sliders', 'sliders', '2019-12-15 15:22:38', '2019-12-15 15:22:38'),
(85, 'add_sliders', 'sliders', '2019-12-15 15:22:38', '2019-12-15 15:22:38'),
(86, 'delete_sliders', 'sliders', '2019-12-15 15:22:38', '2019-12-15 15:22:38'),
(87, 'browse_sub_categories', 'sub_categories', '2019-12-15 15:22:43', '2019-12-15 15:22:43'),
(88, 'read_sub_categories', 'sub_categories', '2019-12-15 15:22:43', '2019-12-15 15:22:43'),
(89, 'edit_sub_categories', 'sub_categories', '2019-12-15 15:22:43', '2019-12-15 15:22:43'),
(90, 'add_sub_categories', 'sub_categories', '2019-12-15 15:22:43', '2019-12-15 15:22:43'),
(91, 'delete_sub_categories', 'sub_categories', '2019-12-15 15:22:43', '2019-12-15 15:22:43'),
(117, 'browse_customer_rate', 'customer_rate', '2019-12-15 17:48:19', '2019-12-15 17:48:19'),
(118, 'read_customer_rate', 'customer_rate', '2019-12-15 17:48:19', '2019-12-15 17:48:19'),
(119, 'edit_customer_rate', 'customer_rate', '2019-12-15 17:48:19', '2019-12-15 17:48:19'),
(120, 'add_customer_rate', 'customer_rate', '2019-12-15 17:48:19', '2019-12-15 17:48:19'),
(121, 'delete_customer_rate', 'customer_rate', '2019-12-15 17:48:19', '2019-12-15 17:48:19'),
(122, 'browse_menus', 'menus', '2020-01-31 05:05:56', '2020-01-31 05:05:56'),
(123, 'read_menus', 'menus', '2020-01-31 05:05:56', '2020-01-31 05:05:56'),
(124, 'edit_menus', 'menus', '2020-01-31 05:05:56', '2020-01-31 05:05:56'),
(125, 'add_menus', 'menus', '2020-01-31 05:05:56', '2020-01-31 05:05:56'),
(126, 'delete_menus', 'menus', '2020-01-31 05:05:56', '2020-01-31 05:05:56'),
(127, 'browse_favorites', 'favorites', '2020-03-11 06:28:27', '2020-03-11 06:28:27'),
(128, 'read_favorites', 'favorites', '2020-03-11 06:28:27', '2020-03-11 06:28:27'),
(129, 'edit_favorites', 'favorites', '2020-03-11 06:28:27', '2020-03-11 06:28:27'),
(130, 'add_favorites', 'favorites', '2020-03-11 06:28:27', '2020-03-11 06:28:27'),
(131, 'delete_favorites', 'favorites', '2020-03-11 06:28:27', '2020-03-11 06:28:27');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 3),
(1, 4),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(27, 3),
(27, 4),
(28, 1),
(28, 3),
(28, 4),
(29, 1),
(29, 3),
(29, 4),
(30, 1),
(30, 3),
(30, 4),
(31, 1),
(31, 3),
(31, 4),
(37, 1),
(38, 1),
(39, 1),
(39, 4),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(52, 3),
(52, 4),
(53, 1),
(53, 3),
(53, 4),
(54, 1),
(54, 3),
(54, 4),
(55, 1),
(55, 3),
(55, 4),
(56, 1),
(56, 3),
(56, 4),
(57, 1),
(57, 3),
(57, 4),
(58, 1),
(58, 3),
(58, 4),
(59, 1),
(59, 4),
(60, 1),
(60, 4),
(61, 1),
(61, 4),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(67, 3),
(68, 1),
(68, 3),
(69, 1),
(69, 3),
(70, 1),
(70, 3),
(71, 1),
(71, 3),
(77, 1),
(78, 1),
(79, 1),
(79, 3),
(80, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(117, 1),
(118, 1),
(119, 1),
(120, 1),
(121, 1),
(122, 1),
(123, 1),
(124, 1),
(125, 1),
(126, 1),
(127, 1),
(128, 1),
(129, 1),
(130, 1),
(131, 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `available` int(11) NOT NULL DEFAULT 1,
  `sub_category_id` int(11) NOT NULL,
  `images` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `seller_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `available`, `sub_category_id`, `images`, `seller_id`, `created_at`, `updated_at`) VALUES
(2, 'اكسسوارات', 15, 1, 17, '[\"products\\\\March2020\\\\NtV4GW1Ve4FdNkX7fPhJ.jpg\"]', 3, '2020-03-05 08:04:43', '2020-03-05 08:04:43'),
(3, 'كنب', 1000, 1, 1, '[\"products\\\\March2020\\\\BvOnKL1tNX1wVrku3aGl.jpg\",\"products\\\\March2020\\\\ynoDdSrzbny8wRVUbdc5.jpg\",\"products\\\\March2020\\\\9S5VHEzkin5RugaocnK0.jpg\"]', 4, '2020-03-05 08:05:09', '2020-03-05 08:05:09'),
(4, 'اجهزة منزلية', 30000, 1, 3, '[\"products\\\\March2020\\\\JiS4VHUvP9NbF2OqnFMb.jpeg\"]', 6, '2020-03-05 08:05:49', '2020-03-05 08:05:49'),
(5, 'سفيحا', 2, 1, 1, '[\"products\\\\March2020\\\\byuDAg01b1kRay3UmX7V.jpg\",\"products\\\\March2020\\\\D4BN7nv7WKxhuTVSWgvZ.jpg\",\"products\\\\March2020\\\\DUkANKeir70JTvP7sSOF.jpg\",\"products\\\\March2020\\\\UVhMWIVqNggyIgf77Wab.jpg\"]', 5, '2020-03-05 08:23:00', '2020-03-08 08:26:29'),
(6, 'سبانخ', 5, 1, 2, '[\"products\\\\March2020\\\\mskC5H8VTtqe5VD5Kw1U.jpg\"]', 5, '2020-03-05 08:24:00', '2020-03-11 05:43:08');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2019-12-15 13:02:13', '2019-12-15 13:02:13'),
(2, 'user', 'Normal User', '2019-12-15 13:02:14', '2019-12-15 13:02:14'),
(3, 'seller', 'seller', '2020-01-31 07:47:47', '2020-01-31 07:47:47'),
(4, 'customer', 'customer', '2020-01-31 07:48:06', '2020-01-31 07:48:06');

-- --------------------------------------------------------

--
-- Table structure for table `sellers`
--

CREATE TABLE `sellers` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `story_body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `seller_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sellers`
--

INSERT INTO `sellers` (`id`, `user_id`, `story_body`, `created_at`, `updated_at`, `seller_name`) VALUES
(3, 17, '<p>wqewqe</p>', '2020-02-05 07:10:00', '2020-02-05 07:17:43', 'sameer Mohammad'),
(4, 19, '123123', '2020-02-14 14:24:58', '2020-02-14 14:24:58', 'mm mm'),
(5, 20, 'اقوم بعمل اشياء مذهله', '2020-02-21 09:07:03', '2020-02-21 09:07:03', 'محمد امواس'),
(6, 22, 'asdasdasdasda asdasdasdasdaasdasdasdasdaasdasdasdasdaasdasdasdasdaasdasdasdasdaasdasdasdasda', '2020-03-03 06:36:27', '2020-03-03 06:36:27', 'testSeller testSeller'),
(7, 23, 'معجنات', '2020-03-05 08:22:43', '2020-03-05 08:22:43', 'طارق التميمي');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', 'settings\\December2019\\6TKxaSE20lW5jSUuFW0a.png', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'productive family', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'The productive family website is the first online platform for productive families', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', 'settings\\December2019\\DB0Egkb5ZY4PNa22iy7s.png', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', 'settings\\March2020\\XM1OkpEYKP5y9aK8gzsa.jpg', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `button_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `button_action` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `View` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `title`, `body`, `image`, `button_title`, `button_action`, `View`, `created_at`, `updated_at`) VALUES
(1, 'اطلبها الان', 'this is this is', 'sliders\\March2020\\PcsBKZgfq9vcp9tHRCZa.jpg', 'asd', 'asd', 1, '2020-03-03 08:25:00', '2020-03-05 08:00:07'),
(2, 'عرض العروض', 'افحص اخر العروض لدينا', 'sliders\\March2020\\zFhr4o1pMTOXRF4ab5cr.jpeg', 'العروض', '/projects', 1, '2020-03-05 08:01:10', '2020-03-05 08:01:10');

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `main_category_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `name`, `main_category_id`, `created_at`, `updated_at`) VALUES
(1, 'الصنف الثاني 1', 7, '2020-02-05 15:40:04', '2020-03-05 08:03:40'),
(2, 'الصنف الثاني 2', 7, '2020-02-05 15:40:04', '2020-03-05 08:03:40'),
(3, 'الصنف الثاني 3', 7, '2020-02-05 15:40:04', '2020-03-05 08:03:40'),
(10, 'الصنف الثاني 4', 7, '2020-02-06 04:59:08', '2020-03-05 08:03:40'),
(11, 'الصنف الثاني 5', 7, '2020-02-06 04:59:08', '2020-03-05 08:03:40'),
(17, 'صنف الاول رقم واحد', 8, '2020-03-05 08:03:02', '2020-03-05 08:03:02');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `first_name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`, `last_name`, `phone_number`, `user_name`, `country`, `city`, `street`, `gender`, `active`) VALUES
(1, 1, 'mohammad imwas', 'admin@admin.com', 'users/default.png', NULL, '$2y$10$j48conB0W2Hp/7XWqNSgnenlzx5ua9EUBWdHoETUc.68iEpysVV1i', 'GBC1F7SzIL1dvCVZaaYjNoCGcLKZOk4VyG6rCCnWR5eOHufmeH4wvriPIXkk', NULL, '2019-12-15 13:03:02', '2019-12-15 13:03:02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(14, 4, 'Mohammad', 'm@m.com', 'users/default.png', NULL, '$2y$10$m4FLe3bO7KgRq0UBKwy4v.HdUnr1qlFhTTpLvO2E1spJCL9S0IHAa', NULL, NULL, '2020-02-05 07:00:46', '2020-02-05 07:06:52', 'Qawasmeh', '2203354', 'mm', 'swe', 'aswqe', 'wqe', 0, 0),
(17, 3, 'sameer', 's@s.com', 'users/default.png', NULL, '$2y$10$EuBpTd51KcnSu0.cQtCeW.F6GrzGiI5ApRh8EU2eC66vmUU5bZNUa', NULL, NULL, '2020-02-05 07:10:17', '2020-02-05 07:17:58', 'Mohammad', '1234567', 'ss', 'ewqe', 'wqe', 'wqe', 0, 0),
(18, 4, 'mohammad', 'mohammadimwas77@gmail.com', 'users/default.png', NULL, '$2y$10$ixN39HtcRvSSzEWhgnALN./W2Hrot5enjE6kha2TOMnS1sNxQ6fBm', NULL, NULL, '2020-02-11 06:36:19', '2020-02-11 06:37:01', 'imwas', '059991699', 'manager@wazcam.com', 'asdfd', 'asdas', 'asdass', 0, 0),
(19, 3, 'mm', 'mm@nn.com', 'users/default.png', NULL, '$2y$10$og6O/XfA/LC7YJ/ecEFPo.KuDNtFTHfKqTJ0csFYgcGu/CCqh8.Wq', NULL, NULL, '2020-02-14 14:24:57', '2020-02-14 14:24:57', 'mm', '123', 'manager@wazcam.com', '123', '123', '123', 0, 0),
(20, 3, 'محمد', 'imwas@gmial.com', 'users/default.png', NULL, '$2y$10$1uwarwOMiQeusbPRQJLVsO0Gw0oE8huSNjRjyurLYDp6E2SJieISy', 'IwMOgYvF1cVMPCa2YOKTjDzbL4gVzqbZGBTSeIvdpXhq9VhVlFdfK90g91u1', '{\"locale\":\"ar\"}', '2020-02-21 09:07:03', '2020-02-21 09:07:03', 'امواس', '0599197580', 'manager@wazcam.com', 'فلسطين', 'الخليل', 'بني نعيم', 0, 0),
(21, 4, 'زبون', 'customer@gmail.com', 'users/default.png', NULL, '$2y$10$IIq2svP.xntUnBDjpfFfmuZ7nk8ZRGT6vzvtCWYMZPfnfc81ED7Lq', NULL, NULL, '2020-03-03 06:13:59', '2020-03-03 06:13:59', 'زببين', '059919758', 'manager@wazcam.com', 'asd', 'asd', 'asd', 0, 0),
(22, 3, 'testSeller', 'testSeeler@gmial.com', 'users/default.png', NULL, '$2y$10$zCb6yeO9Qbl0I6nKA8V8o.7mq.Oj3wNhnSUO1WRuRUnj9S3W4/ijG', NULL, NULL, '2020-03-03 06:36:27', '2020-03-03 06:36:27', 'testSeller', '0599875', '123', 'ads', 'asd', 'ad', 0, 0),
(23, 3, 'طارق', 't@t.com', 'users/default.png', NULL, '$2y$10$8hqvnJGeE/MG2qFJ65eBVO/3epLzC7mRAPy/nb3Z.otJURZAYZpEu', NULL, NULL, '2020-03-05 08:22:43', '2020-03-05 08:22:43', 'التميمي', '025597425', 'ta3', 'فلسطين', 'الخليل', 'مفرق الجامعة', 0, 0),
(24, 4, 'مالك', 'm@m2.com', 'users/default.png', NULL, '$2y$10$xiEOnGcFfpvMEeAeOXEoIueSB/OUc/2BMI2UPzjkcPp1CN5x07v/S', NULL, NULL, '2020-03-05 08:25:32', '2020-03-05 08:25:32', 'التميمي', '1234455', 'ms', 'فلسطين', 'الخليل', 'حي الجامعة', 0, 0),
(25, 4, 'mohammad', 'Q@q.com', 'users/default.png', NULL, '$2y$10$lyxc1K8K7UHubRQv6NrOO.9ziBrXLwpxHTbn.jgjoqJV2RozsgPvi', NULL, '{\"locale\":\"ar\"}', '2020-03-08 07:15:50', '2020-03-08 07:15:50', 'qawasmeh', '123456789', 'ewq', 'فلسطين', 'الخليل', 'خخ', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bids`
--
ALTER TABLE `bids`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_rate`
--
ALTER TABLE `customer_rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `favorites`
--
ALTER TABLE `favorites`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_categories`
--
ALTER TABLE `main_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders_details`
--
ALTER TABLE `orders_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `sellers`
--
ALTER TABLE `sellers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bids`
--
ALTER TABLE `bids`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `customer_rate`
--
ALTER TABLE `customer_rate`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=155;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `favorites`
--
ALTER TABLE `favorites`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `main_categories`
--
ALTER TABLE `main_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `orders_details`
--
ALTER TABLE `orders_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sellers`
--
ALTER TABLE `sellers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
