<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Bindorder;
use App\CustomerRate;
use App\Message;
use App\Order;
use App\SellerRate;
use App\SubCategory;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


Route::get('/', function () {
    return view('welcome');
});
Route::get('/test', function () {
    return view('test');    
}); 

Route::get('/register', 'RegesterController@goToRegiser'); 
Route::post('/register/create', 'RegesterController@createNewAccount')->name('createUser');

Route::post('/orders/products', 'ProductsController@store');
Route::get('/products/show/{id}', 'ProductsController@index');
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();

    Route::get('user/password/{id}', function ($id) {
        return view('passwprdChange');
    });
    Route::post('/user/password/{id}', function($id, Request $request){
        // return $request;
        if (Hash::check($request->old_pass, Auth::user()->password)) {
                $request->validate(['password' => 'required|confirmed|min:6'],
                    [
                        'password.required'=> "كلمه المرور مطلوبه",
                        'password.confirmed' => 'كلمتا المرور غير متطابقات',
                        'password.min' => 'كلمه المرور يجب ان تحتوي على الاقل على 6 احرف',
                    ]);
                $user = \App\User::where('id', '=', Auth::user()->id)->first();
                $user->password = bcrypt($request->password);
                $user->save();
                return redirect()->to('/admin');
        }
        return redirect()->back()->with('old_pass1', 'كلمه المرور غير صحيه');
        
    })->name('changePass');
    Route::get('/messages/conv/{id}', function ($id) {
        if(!Auth::check()) {
            return abort(404);
        }
        DB::table('messages')->where(function($q) use($id) {
            $q->where('sender_id', '=', $id)->where('resever_id', '=', Auth::user()->id);
        })->update(array('read' => 1));
        
        $sended_msg = 
        Message::where(function($q)use($id) {
            $q->where('sender_id', '=', Auth::user()->id)->where('resever_id', '=', $id);
        })->orWhere(function($q) use($id) {
            $q->where('sender_id', '=', $id)->where('resever_id', '=', Auth::user()->id);
        })->orderBy('created_at', 'desc')->get();
        return view('vendor.voyager.messages.conv', compact('sended_msg', 'id'));
    })->name('viewMsg');


    Route::post('/message/conv/store', function(Request $request) {
        if(!Auth::check()) {
            return abort(404);
        }
        $request->validate([
            'message' => ['required'],
        ],[
            'message.required' => "الرجاء كتابه رساله"
        ]);
        $newMsg = new Message();
        $newMsg->sender_id = Auth::user()->id;
        $newMsg->resever_id = $request->resever_id;
        $newMsg->message = $request->message ;
        $newMsg->save();
        return redirect()->back();
    })->name('storeMsg');
});

// Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/main-category/{id}', 'CategoriesController@index');
Route::get('/sub-category/{id}', 'CategoriesController@show');

Route::get('/projects', 'ProjectController@index')->name('allProjects');
Route::get('/project/{id}', 'ProjectController@brows')->name('browsProject');
Route::post('/project/bid/store', 'ProjectController@addBid')->name('addBid');
Route::delete('/project/bid/delete/{id}', 'ProjectController@deleteBid')->name('deleteBid');
Route::put('/project/bid/update/', 'ProjectController@updateBid')->name('updateBid');
Route::post('/project/accept/bid/{seeler_id}/{order_id}', 'ProjectController@acceptBid')->name('acceptBid');
Route::get('/projects', 'ProjectController@filter')->name('projectFillter');
Route::post('/project/seller/aceept/{id}','ProjectController@sellerAceept')->name('sellerAceept');
Route::post('/project/seller/reject/{id}','ProjectController@sellerReject')->name('sellerReject');
Route::post('/cat/add/new' , "CategoriesController@addNewByAdmin")->name("addNewCat");
Route::post('/cat/add/reject' , "CategoriesController@rejectByAdmin")->name("rejectSubs");

Route::get('/go/{id}', function($id) {
    return redirect()->to('/project/'.$id);
});

Route::get('/pro/{id}', function($id) {
    return redirect()->to('/products/show/'.$id);
});
Route::get('profile/{id}', function($id) {
    $seller = App\Seller::findOrFail($id);
    return view('profile.show',compact('seller'));
});
Route::get('/profile/product/{id}', function($id) {
    $products = App\Product::where('seller_id' , $id)->get();
    $seller = App\Seller::where('user_id',$id)->first();
    return view('profile.products', compact('products', 'seller'));
});
Route::get('/profile/rate/{id}', function($id) {
    // $rates = App\Rate::where('seller_id' , $id)->get();
    $seller = App\Seller::where('user_id',$id)->first();
    return view('profile.rate', compact('seller'));
});

Route::post('/project/seller/done/{id}', function($id) {
    $bind = Bindorder::where('id', $id)->first();
    $order = Order::where('id', $bind->order_id)->first();
    $bind->status = 3;
    $bind->save();
    $order->completed = 1;
    $order->save();
    return redirect()->back();
})->name('sellerDone');

Route::post('/saverate', function(Request $request) {
    // seller Rate 
    // return $request;
    $order = Order::where('id', '=', $request->order_id)->first();
    if($request->rate != '0' && !isset($request->type)){
        $reate = new SellerRate();
        $reate->rate = $request->rate;
        $reate->description	 = $request->reate_text;
        $reate->customer_id = $order->customer_id;
        $reate->seller_id = $order->seller_id;
        $reate->save();
    }
    if(isset($request->type)) {
        $bind = Bindorder::where('order_id', '=', $request->order_id)->first();
        // return $bind;
        // return $request;
        if($request->rate != '0'){
            $reate = new CustomerRate();
            $reate->rate = $request->rate;
            $reate->description	 = isset($request->reate_text) || "";
            $reate->customer_id = $order->customer_id;
            $reate->seller_id = $order->seller_id;
            $reate->save();
        }
        $bind->status = 4;
        $bind->save();
        return redirect()->back();
    }
    $order->done_me = 1;
    $order->save();
    return redirect()->back();

})->name('saveRate');

Route::get('/serche-by-name', function(Request $request) {
    $str = utf8_decode(isset($request->key)? $request->key : '');
    // return strlen($str);
    if(strlen($str) <= 2) {
        return abort(404);
        
    }
    
    if(isset($request->sort)) {
        if($request->sort == 'n-o') {
            $products = \App\Product::where('name', 'like', '%'.  $request->key .'%')->orderBy('created_at', 'desc')->get();
        }

        if($request->sort == 'o-n') {
            $products = \App\Product::where('name', 'like', '%'.  $request->key .'%')->orderBy('created_at', 'asc')->get();
        }

        if($request->sort == 'h-l') {
            $products = \App\Product::where('name', 'like', '%'.  $request->key .'%')->orderBy('price', 'desc')->get();
        }

        if($request->sort == 'l-h') {
            $products = \App\Product::where('name', 'like', '%'.  $request->key .'%')->orderBy('price', 'asc')->get();
        }
    }
    else {
        $products = \App\Product::where('name', 'like', '%'.  $request->key .'%')->get();
    }
    // $products->get();
    return view('serche', compact('products'));
})->name('sercheByName');

Route::get('/test', function() {
    return SubCategory::all();
});

Route::get('/resetPassword', 'passwordsController@showResetForm')->name('resetPasswordForm');
Route::post('/resetPassword', 'passwordsController@resetPassword')->name('resetPassword');
Route::get('/resetPassword/{token}', 'passwordsController@showNewPassowrdForm')->name('passwordReset');
Route::post('/resetPassword/changePassword', 'passwordsController@changePassowrd')->name('resetAndChangePassowrd');

Route::get('/following/{id}', 'SellersController@following');
Route::get('/unfollowing/{id}', 'SellersController@unfollowing');

