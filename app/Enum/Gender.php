<?php 
    namespace App\Enum;

    class Gender 
    {
        const MALE   = 0;
        const FEMALE = 1;
    }

