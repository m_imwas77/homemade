<?php 
    namespace App\Enum;

    class Role 
    {
        const ADMIN    = 1;
        const USER     = 2;
        const SELLER   = 3;
        const CUSTOMER = 4;          
    }

