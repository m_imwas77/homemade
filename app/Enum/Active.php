<?php 
    namespace App\Enum;

    class Active 
    {
        const ACTIVE   = 1;
        const UNACTIVE = 0;
    }

