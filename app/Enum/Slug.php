<?php 
    namespace App\Enum;

    class Slug 
    {
        const USER_SLUG            = 'users';
        const CUSTOMRT_SLUG        = 'customers';
        const SELLER_SLUG          = 'sellers';
        const MAIN_CATEGORIES_SLUG = "main-categories";
        const SUB_CATEGORIES_SLUG  = "sub-categories";
        const ORDERS_SLUG          = 'orders';
        const PRODUCTS_SLUG        = 'products';
    }

