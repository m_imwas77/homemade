<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class OrdersDetail extends Model
{

    protected $appends = ['sub_cat'];

    public function getSubCatAttribute() {
        return SubCategory::where('id', '=', $this->sub_category_id	)->first();
    }

    public function scopeOrderDetails($query) {
        if( Auth::user()->role->name == 'admin') {
            return $query;
        }
        else {
            return $query->join('orders', 'orders.id', '=', 'orders_details.order_id')
                        ->select('orders_details.*')
                        ->where( 'customer_id', Auth::user()->id );
        }
    }
    public function order()
    {
        return $this->belongsTo('App\Order');
    }


}
