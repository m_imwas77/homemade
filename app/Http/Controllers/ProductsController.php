<?php

namespace App\Http\Controllers;

use App\Bindorder;
use App\Favorite;
use App\Log;
use App\Message;
use App\Order;
use App\OrdersDetail;
use App\Product;
use App\Seller;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductsController extends Controller
{
    public function index($id , Request $request) {
        $product = Product::find($id);
        if($request->action != null) {
            $this->favorits($request->action, $id, $product);
        }
        return view('products.show', compact('product'));
    }
    private function favorits($action, $id, $product) {
      $msg = "";
        if($action == 'add') {
            $favorits = new Favorite();
            $favorits->customer_id = Auth::user()->id;
            $favorits->type = 'products';
            $favorits->type_id = $id ;
            $favorits->save();
            $msg = "تم اضافه المنتج للمفضله";
            $this->createLog('add to fav');
        }
        else if( $action == 'delete' ) {
            $favorits = Favorite::where('customer_id', Auth::user()->id)->where('type','products')->where('type_id', $product->id)->first();
            Favorite::destroy( $favorits->id);
            $msg = "تم ازاله المنتج للمفضله";
            $this->createLog('remove form fav');
        }
        return redirect('/products/show/'.$id)->with('status', $msg);
    }
    
    public function store(Request $request) {
        $prodeuct = Product::where('id', '=', $request->products_id)->first();
        $seller = User::where('id', '=', $prodeuct->seller_id)->first()->seller;

        Product::where('id', '=', $request->products_id)->update(   ['number_store'=>  $request->number_store]  );
        $order = new Order();
        $order->title = $request->title;
        $order->final_price = $request->price;
        $order->start_date = Carbon::now();
        $order->end_date = Carbon::now();
        $order->min_price = $request->price;
        $order->max_price = $request->price;
        $order->number_of_days = 10;
        $order->customer_id = Auth::user()->customer->id;
        $order->seller_id = $seller->user_id;
        $order->accepted = 1;
        $order->save();

        $binder = new Bindorder();
        $binder->seller_id = $seller->user_id;
        $binder->order_id = $order->id;
        $binder->save();

        $deteals = new OrdersDetail();
        $deteals->order_id = $order->id;
        $deteals->quantity = $request->quantity;
        $deteals->sub_category_id =  $prodeuct->sub_category_id;
        $deteals->name  = $prodeuct->name; 
        $deteals->desc =  $prodeuct->description;
        $deteals->save();
        $customerName = $order->customer->user->first_name;
        $customerName = $order->customer->user->first_name;

        $msg = ' لقد تم طلب المنتج  ' . $prodeuct->name;
        $msg .= " من قبل "  .$customerName;
        $msg .= '  <a type="button" href=' . Route('browsProject',['id' => $order->id]) .'> شاهد تفاصيل الطلبيه من هنا </a>';
        $this->sendMsg($seller->user_id,$msg, Auth::user()->id);
        $this->createLog('add new order');
        return redirect('/products/show/'.$request->products_id)->with('status', 'لقد تم طلب المنتج بنجاح ');
   
    }

    public function sendMsg($resever_id, $msg, $sender_id) {
        $message = new Message();
        $message->sendMsg($resever_id, $msg, $sender_id);
    }
    public function createLog($log) {
        $loger = new Log();
        $loger->log  = $log ;
        $loger->user_id = Auth::user()->id;
        $loger->save();
    }
}
