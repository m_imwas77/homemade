<?php

namespace App\Http\Controllers;

use App\MainCategory;
use App\Product;
use App\SubCategory;
use Illuminate\Http\Request;
use App\BindCategory;
use App\Message;
use Illuminate\Support\Facades\Auth;

class CategoriesController extends Controller
{
    public function index($id) {
        $subCategories = SubCategory::where('main_category_id', $id)->get();
        $nameCategory = MainCategory::findOrFail($id)->name;
        return view('categories.main_category', compact('subCategories', 'nameCategory')); 
    }
    public function show($id) {
        $products = Product::where('sub_category_id', $id)->get();
        $nameCategory = SubCategory::findOrFail($id)->name;
        return view('categories.sub_category', compact('products', 'nameCategory'));
    }
    // TODO : handel Product
    public function addNewByAdmin(Request $request) {
        // return $request;
        $bind = BindCategory::find($request->bind_cat);
        $bindName = $bind->name;
        $product_id = $bind->product_id;
        $msg = 'لقد تم قبول صنفك المقترح';
        $msg .= $bindName;
        $msg .= '<a href="/admin/products/'.$product_id.'"> شاهد المنتج </a>';;
        $this->sendMsg($bind->user_id, $msg, Auth::user()->id);
        $bindName = $request->name_bind;
        $unut_id = $request->unit_id;
        if($request->old_main_cat != null) {
            $subId = $this->addNewSubCat($request->old_main_cat , $bindName,  $unut_id);
            $this->deleteBindCat($request->bind_cat);
            $this->apdateProdcutCategoryId($product_id, $subId);
        }

        if( $request->new_main_cat != null) {
            $newMainCatId = $this->addnewMainCat($request->new_main_cat);
            $subId = $this->addNewSubCat($newMainCatId, $bindName,  $unut_id);
            $this->deleteBindCat($request->bind_cat);
            $this->apdateProdcutCategoryId($product_id, $subId);
        }
        return redirect()->back();
    }

    public function apdateProdcutCategoryId($product_id, $subId) {
        $prod = Product::where('id', '=', $product_id)->update(['sub_category_id' => $subId]);
    }

    public function deleteBindCat($id) {
       $bind =  BindCategory::find($id);
       $bind->delete();
    }

    public function addNewSubCat($main_cat_id, $name,  $unut_id) {
        $subCat = new SubCategory();
        $subCat->main_category_id	 = $main_cat_id;
        $subCat->name = $name;
        $subCat->unit_id =  $unut_id;
        $subCat->save();
        return $subCat->id;
    }

    public function addnewMainCat($name) {
        $mainCat = new MainCategory();
        $mainCat->name = $name;
        $mainCat->save();
        return $mainCat->id;
    }


    public function rejectByAdmin(Request $request) {
        $bind = BindCategory::find($request->bind_cat);
        $this->apdateProdcutCategoryId($bind->product_id, $request->sub_id);
        
        $msg = 'لقد تم رفض صنفك المقترح';
        $msg .= $bind->name;
        $msg .= '<a href="/admin/products/'.$bind->product_id.'"> شاهد المنتج </a>';;
        $this->sendMsg($bind->user_id, $msg, Auth::user()->id);
        $this->deleteBindCat($bind->id);
        return redirect()->back();
    }

    public function sendMsg($resever_id, $msg, $sender_id) {
        $message = new Message();
        $message->sendMsg($resever_id, $msg, $sender_id);
    }
}
