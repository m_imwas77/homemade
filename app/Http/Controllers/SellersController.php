<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Favorite;
use Auth;

class SellersController extends Controller
{
    public function following($id) {
        $favorite = new Favorite;
        $favorite->type = 'sellers';
        $favorite->type_id = $id;
        $favorite->customer_id = Auth::user()->id;
        $favorite->save();
        return redirect('/profile/'.$id);
    }
    public function unfollowing($id) {
        Favorite::where('type','sellers')->where('type_id',$id)->where('customer_id', Auth::user()->id)->delete();
        return redirect('/profile/'.$id);
    }
}
