<?php

namespace App\Http\Controllers;
use App\Customer;
use App\Log;
use App\Seller;
use App\User;
use Exception;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataRestored;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Events\BreadImagesDeleted;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use TCG\Voyager\Http\Controllers\VoyagerBaseController as BaseVoyagerBaseController;

class RegesterController extends BaseVoyagerBaseController
{   
    private $slugProvider;
    private $roleProvider;
    private $activeProvider;
    private $genderProvider;
    public function __construct() {
        $this->slugProvider = new \App\Enum\Slug;
        $this->roleProvider = new \App\Enum\Role;
        $this->activeProvider = new \App\Enum\Active;
        $this->genderProvider = new \App\Enum\Gender; 
    }
    public function goToRegiser() {
        return Auth::check() ?  redirect()->to('/admin') : view('register'); 
    }
    public function getRegesterFormValidation() {
        return [
            'first_name' => 'required|max:30',
            'last_name' => 'required|max:30',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:6',
            'country' => 'required|max:30', 
            'city' => 'required|max:30',
            'street' => 'required|max:30',
            'phone_number' => 'required|regex:/^[0-9]+$/|max:10|min:10',
            'user_name' => 'unique:users'
        ];
    }
    public function getErrorMsgForRegesterForm() {
        return [
            'first_name.required' => "الاسم الاول مطلوب",
            'first_name.max' => 'يجب ان يكون  اقل من 30 حرف',
            'last_name.required' => "اسم العائله مطلوب",
            'last_name.max' => 'يجب ان يكون  اقل من 30 حرف',
            'email.required' => "البريد الاكتروني مطلوب",
            'email.email' => 'البريد الاكتروني المدخل يحمل صيغه خاطئه',
            'email.unique' => 'البريد الاكتروني مستخدم',
            'password.required'=> "كلمه المرور مطلوبه",
            'password.confirmed' => 'كلمتا المرور غير متطابقات',
            'password.min' => 'كلمه المرور يجب ان تحتوي على الاقل على 6 احرف',
            'country.required' => 'الدوله مطلوبه', 
            'country.max' => 'يجب ان يكون  اقل من 30 حرف',
            'city.required' => 'المديينه مطبوبه',
            'city.max' => 'يجب ان يكون  اقل من 30 حرف',
            'street.required' => 'الشارع مطلوب',
            'street.max' => 'يجب ان يكون  اقل من 30 حرف',
            'phone_number.required' => 'رقم الهاتقف مطلوب',
            'phone_number.max' => 'رقم الهاتف غير صحيح',
            'phone_number.min' => 'رقم الهاتف غير صحيح',
            'phone_number.regs' => 'رقم الهاتف غير صحيح',
            'user_name.unique' => "اسم المستخدم مستخدم ب الفعل"
        ];
    }
    public function createNewAccount(Request $request) {
        $request->validate($this->getRegesterFormValidation(),
                            $this->getErrorMsgForRegesterForm());
        $slug = 'users';
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        $request->merge(['setting' => '{"locale":"ar"}']);
        
        $request->merge(['active' => 1]);
        $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());
        $data->active = 1;
        $data->save();
        if($request->role_id == '4' ) {
            $_slug = 'customers';
            $_dataType = Voyager::model('DataType')->where('slug', '=', $_slug)->first();
            $request->merge(['customer_name' => $request->first_name.' '.$request->last_name]);
            $request->merge(['user_id' => $data->id]);
            $_data = $this->insertUpdateData($request, $_slug, $_dataType->addRows, new $_dataType->model_name());
        }
        else {
            $_slug = 'sellers';
            $_dataType = Voyager::model('DataType')->where('slug', '=', $_slug)->first();
            $request->merge(['seller_name' => $request->first_name.' '.$request->last_name]);
            $request->merge(['user_id' => $data->id]);
            // $request->merge(['story_body' => ' ']);
            $_data = $this->insertUpdateData($request, $_slug, $_dataType->addRows, new $_dataType->model_name());
        }
        Auth::loginUsingId($data->id);
        $this->createLog('create new Account');
        return redirect()->to('/admin');
    }
    public function createLog($log) {
        $loger = new Log();
        $loger->log  = $log ;
        $loger->user_id = Auth::user()->id;
        $loger->save();
    }
}
