<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MainCategory;
use App\Order;
use App\Customer;
use App\Bid;
use App\Bindorder;
use App\Log;
use App\Message;
use Auth;
use Illuminate\Support\Facades\Redirect;
use \carbon\Carbon;
use Illuminate\Support\Facades\Auth as FacadesAuth;
use Illuminate\Support\Facades\DB;

class ProjectController extends Controller
{
    
    private $duration = [
        "أقل من أسبوع واحد" => "0-6",
        "من 1 إلى 2 أسابيع" => "7-14",
        "من  2 أسابيع إلى شهر" => "15-30",
        "من شهر إلى 3 أشهر" => "31-90", 
        "أكثر من 3 أشهر" => "91-<>"
    ];
    private $price = [
        "اقل من 100" => "0-100",
        "من 100 الى 250" => "101-250",
        "من 250 الى 500" => "351-500",
        "من 500 الى 750" => "501-750", 
        "من 750 الى 1000" => "751-1000",
        "من 1000 الى 2000" => "1001-2000",
        "من 2000 الى 3000" => "2001-3000", 
        "اكثر من 3000" => "3001-<>"
    ];
    public function index() {
        $mainCategory = MainCategory::get();
        $time = $this->duration;
        $price = $this->price;
        $orders = Order::orderBy('created_at',  'desc')->where('end_date', '>', Carbon::now())
        ->where('accepted', '=', 0)->paginate(5);
        return view('projects.index', compact('mainCategory', 'time', 'orders', 'price'));
    
        // TODO FIX BUG DOUBLICET
        // $orders =\App\Order::join('orders_details', 'orders.id', '=', 'orders_details.order_id')
        //     ->select('orders.*', 'orders_details.sub_category_id')
        //     ->orderBy('created_at',  'desc')
        //     ->where('end_date', '>', Carbon::now())
        //     ->where('accepted', '=', 0);
    }
    public function filter(Request $request) {

        $priceRequest = $request->price;
        $timeRequest = $request->time;
        $catigories = $request->categoty;

        $mainCategory = MainCategory::get();
        $time = $this->duration;
        $price = $this->price;

        $orders = 
                Order::orderBy('created_at',  'desc')
                ->where('end_date', '>', Carbon::now())
                ->where('accepted', '=', 0);

        if($priceRequest) {
            $orders = $orders->where(function($query) use($priceRequest) {
                $start = true;
                foreach ($priceRequest as $priceR) {
                    $priceArray =  explode('-', $priceR);
                    if($priceArray[1] == "<>") {
                        if($start == true)  $query->where('max_price', '>', $priceArray[0]);
                         $query->orWhere('max_price', '>', $priceArray[0]);
                    }
                    else {
                        if($start == true)  $query->whereBetween('max_price', [ $priceArray[0],  $priceArray[1]]);
                         $query->orWhereBetween('max_price', [$priceArray[0],  $priceArray[1]]);
                    }
                    $start = false;
                }
            });
        }

        if($timeRequest) {
           $orders = $orders->where(function($query) use($timeRequest) {
            $start = true;
            foreach ($timeRequest as $timeR) {
                $timeArray =  explode('-', $timeR);
                if($timeArray[1] == "<>") {
                    if($start == true)  $query->where('number_of_days', '>', $timeArray[0]);
                     $query->orWhere('number_of_days', '>', $timeArray[0]);
                }
                else {
                    if($start == true)  $query->whereBetween('number_of_days', [ $timeArray[0],  $timeArray[1]]);
                     $query->orWhereBetween('number_of_days', [$timeArray[0],  $timeArray[1]]);
                }
                $start = false;
            }
        });
        }
        
        if($catigories) {
           $ids = $this->getSubCategoriesIds($catigories);
           $orders->whereIn('sub_category_id', $ids);
        }

        $orders =  $orders->paginate(5);
        
        return view('projects.index', compact('mainCategory', 'time', 'orders', 'price'));
    }

    public function getSubCategoriesIds($catigories) {
        return \App\SubCategory::whereIn('main_category_id', $catigories)->pluck('id')->toArray();
    }

    public function brows($id) {
        $order = Order::find($id);

        if(!$order) {
            abort(404);
        }
        if($this->isAuthSeller()) {
            $AuthBid = $this->AuthHasBid($order->bids); 
            if($AuthBid) {
                $order->AuthBId =  true;
            } else {
                $order->AuthBId =  false;;
            }
        }
        $order->avg_bid = $order->bids->avg('price');
        $order->total_bid = count($order->bids);

        // swap bid to be first bid if auth user create a bid
        if(Auth::check()){
            foreach($order->bids as $key => $bid) {
                if($bid->seller->user->id == Auth::user()->id) {
                    $firstObj = $order->bids[0];
                    $order->bids[0] = $order->bids[$key];
                    $order->bids[$key] = $firstObj;
                }
            }   
        }
        
        return view('projects.brows', compact('order'));
    }

    public function isAuthSeller() {
        return (Auth::check() && Auth::user()->role_id == 3);
    }

    public function AuthHasBid($bids) {
        return $bids->where('seller_id', Auth::user()->seller->id)->first();
    }   

    public function addBid(Request $request) {
        $order = Order::find($request->order_id);

        if(!$order || $order->end_receive_bid == true) {
            abort(404);
        }
        $bid = new Bid();
        $bid->seller_id = $request->seller_id;
        $bid->order_id = $request->order_id;
        $bid->price = $request->price;
        $bid->number_of_day = $request->number_of_day;
        $bid->description = $request->description;
        $bid->save();
        $this->createLog('create new bid');
        return redirect()->back();
    }

    public function deleteBid($id) {
        $bid = Bid::find($id);
        if($bid) {
            $bid->destroy($id);
            $this->createLog('remove bind');
            return redirect()->back();
        }
        else {
            abort(404);
        }
        return $id;
    }

    public function updateBid(Request $request) {
        $bid = Bid::find($request->id);
        if($bid) {
            $bid->number_of_day = $request->number_of_day;
            $bid->price = $request->price;
            $bid->description = $request->description; 
            $bid->save();
            $this->createLog('update bid');
            return redirect()->back();
        }
        else {
            abort(404);
        }
    }

    public function acceptBid($seller_id, $porject_id) {
        $order = Order::where('id', $porject_id)->first();
        if($order) {
            $order->seller_id = $seller_id ;
            $order->accepted = 1;
            $order->save();
            
            $bindorder = new Bindorder();
            $bindorder->seller_id = $seller_id ;	
            $bindorder->order_id = $order->id;
            $bindorder->save();

            $msg = 'لقد تم قبول عرضك المقدم على الطلبيه ';
            $msg .= $order->customer->first_name;
            $msg .= '  <a type="button" href=' . Route('browsProject',['id' => $order->id]) .'> شاهد تفاصيل الطلبيه من هنا </a>';
            $this->sendMsg($seller_id,$msg, Auth::user()->id);
            $this->createLog('accept bid');
            return redirect()->back();
        }
        else {
            abort(404);
        }
    }

    public function sellerAceept($id) {
        $bind = \App\Bindorder::find($id);
        if($bind) {
            $order = Order::where('id', '=', $bind->order_id)->first();
            $order->in_progress = 1;
            $order->save();
            $bind->status = 1;
            $bind->save();
            $msg = '   لقد تم الموافقه على العمل على طلبيتك ';
            $msg .= '('. $order->title .')';
            $msg .=     '  من قبل ';
            $msg .= ' ('.Auth::user()->first_name.') ';
            $this->sendMsg($order->customer->user->id,$msg, Auth::user()->id);
            $this->createLog('seller accept work');
            return redirect()->back();
        }
        else {
            abort(404);
        }

    }

    public function sellerReject($id) {
        $bind = \App\Bindorder::find($id) ;
        if($bind) {
            $order = Order::where('id', '=', $bind->order_id)->first();
            $order->rejected = 1;
            $order->save();
            $bind->status = 2;
            $bind->save();
            $msg = ' لقد تم رفض العمل على طلبيتك ';
            $msg .= ' ('. $order->title .') ';
            $msg .=  ' من قبل ';
            $msg .= ' ('.Auth::user()->first_name.') ';
            $this->sendMsg($order->customer->user->id,$msg, Auth::user()->id);
            $this->createLog('seller reject work');
            return redirect()->back();
        }
        else {
            abort(404);
        }
    }
    public function createLog($log) {
        $loger = new Log();
        $loger->log  = $log ;
        $loger->user_id = Auth::user()->id;
        $loger->save();
    }
    public function sendMsg($resever_id, $msg, $sender_id) {
        $message = new Message();
        $message->sendMsg($resever_id, $msg, $sender_id);
    }

}   
