<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class passwordsController extends Controller
{
    public function showResetForm() {
        return view('password.reset');
    }

    public function resetPassword(Request $request) {
        $email = $request->email;
        $user = \App\User::where('email', '=', $email)->first();

        if(!$user) {
            return redirect()->back()->with('error', 'البريد الاكتروني غير موجود');
        }

        $token = $this->generateRandomString(60);

        DB::table('password_resets')->insert(
            ['email' => $email, 'token' => $token, 'created_at' => Carbon::now()]
        );

        $this->sendEmail($email, $token);

        return redirect()->back()->with('success' , 'لقد تم رساله لاستعاده كلمه المرور لحسابك');
    }

    public function showNewPassowrdForm($token) {
        $resets = DB::table('password_resets')->where('token', '=', $token)->first();
        
        if(Carbon::now()->diffInHours($resets->created_at) > 1) {
            return 'انتهت صلاحية الرابط';
        }
        
        return view('password.changePassowrd' , ['token' => $token]);
    }

    public function changePassowrd(Request $request) {

        $request->validate(['password' => 'required|confirmed|min:6'],
        [
            'password.required'=> "كلمه المرور مطلوبه",
            'password.confirmed' => 'كلمتا المرور غير متطابقات',
            'password.min' => 'كلمه المرور يجب ان تحتوي على الاقل على 6 احرف',
        ]);

        $resets = DB::table('password_resets')->where('token', '=', $request->token)->first();
        $user = \App\User::where('email', '=', $resets->email)->first();
       
        $user->password = bcrypt($request->password);
        $user->save();
        Auth::loginUsingId($user->id);

        return redirect()->to('/admin');
    }

    public function sendEmail($email, $token) {
            $to_name = '';
            $to_email = $email;
            $data = array('token'=>$token);
            Mail::send('mails.resetPassword', $data, function ($message) use($to_email, $to_name) {
                $message->to($to_email, $to_name);
                $message->subject('استعاده كلمه المرور');
                $message->from('homemadehu77@gmail.com','استعاده كلمه المرور');
            });
    }
    
    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString.uniqid();
    }
}
