<?php

namespace App\Http\Controllers\Voyager;

use App\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Http\Controllers\VoyagerAuthController as BaseVoyagerAuthController;

use Illuminate\Validation\ValidationException;
class VoyagerAuthController extends BaseVoyagerAuthController
{
    
    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'user_name' => 'required',
            'password' => 'required'
        ],[
            'user_name.required' => 'جميع الحقول مطلوبه',
            'password.required' => 'جميع الحقول مطلوبه'
        ]);

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            $this->createLog('user login');
            return $this->sendLockoutResponse($request);
        }

        $firstCredential  = filter_var($request->user_name, FILTER_VALIDATE_EMAIL) ? 'email' : 'user_name';
        

        $credentials = $request->only('password');
        
        $credentials[$firstCredential] = $request->user_name;

        $isAuth = $this->guard()->attempt($credentials, $request->has('remember'));
        // dd($isAuth && Auth::user()->active == 1);
        if ($isAuth && Auth::user()->active == 1) {
            return $this->sendLoginResponse($request);
        }
        else {
            Auth::logout();

            return $this->sendFailedLoginResponse($request);

        }
        return redirect()->back();

        // dd("asd");

        
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        throw ValidationException::withMessages([
            $this->username() => 'البريد الإلكتروني أو كلمة السر غير صالحة.',
        ]);
    }

    public function createLog($log) {
        $loger = new Log();
        $loger->log  = $log ;
        $loger->user_id = Auth::user()->id;
        $loger->save();
    }
}
