<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Favorite extends Model
{
    public function scopeFavorite($query) {
        return $query->where('customer_id',Auth::user()->id);
    }
}
