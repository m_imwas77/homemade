<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Message extends Model
{
    public function scopeMessages($query) {
        if(Auth::user()->role_id == 1){
            return $query->where('sender_id',Auth::user()->id)
                     ->orWhere('resever_id', Auth::user()->id);
        }
        $AdminUsersIds = User::where('role_id', '=', 1)->pluck('id')->toArray();
        return $query->where(function($q){
            $q->where('sender_id',Auth::user()->id)
            ->orWhere('resever_id', Auth::user()->id);
        })->where(function($q)use ($AdminUsersIds) {
            $q->whereNotIn('sender_id', $AdminUsersIds)
            ->whereNotIn('resever_id', $AdminUsersIds);
        });
    }
    
    public function user_sender() {
        return $this->belongsTo(User::class , 'sender_id');
    }
    public function user_resever() {
        return $this->belongsTo(User::class , 'resever_id');
    }

    public function sendMsg($resever_id, $msg, $sender_id) {
        $message = new Message();
        $message->resever_id = $resever_id;
        $message->message = $msg;
        $message->sender_id = $sender_id;
        $message->save();
    }
}
