<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Product extends Model
{
    public function scopeProducts($query) {
        if(Auth::user()->role->name == 'admin') {
            return $query;
        }
        else {
            return $query->where('seller_id', Auth::user()->id);
        }
    }

    public function sellers() {
        return $this->belongsTo(Seller::class,'seller_id');
    }
    public function SubCategory() {
        return $this->belongsTo(SubCategory::class,'sub_category_id');
    }
}
