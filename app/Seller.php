<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Seller extends Model
{
    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->seller_name = request('seller_name');
        });
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function scopeSellers($query) {
        if( Auth::user()->role->name == 'admin') {
            return $query;
        }
        else {
            return $query->where('user_id',Auth::user()->id);
        }
    }
}
