<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Bindorder extends Model
{
   protected $table = "bindorder";


   protected $appends = ['numberOfDayToEnd'];
    

    public function getNumberOfDayToEndAttribute() {
        $user = \App\User::where('id', $this->seller_id)->first();

        if(!$user) {
            return 0 ;
        }

        $bid = \App\Bid::where('order_id', $this->order_id)->where('seller_id', $user->seller->id)->first();
        
        if(!$bid) {
            return 0 ;
        }

        
        $end_date = Carbon::parse($this->updated_at);
        $now = Carbon::now();
        $dayFromStart = $now->diffInDays($end_date);

        $numberOfDay = $bid->number_of_day;
        $diff =  $numberOfDay  - $dayFromStart;
       
        return $diff;
        
    }
   public function scopeUser($query) {
        if(Auth::user()->role->name == 'seller') {
            return $query->where('bindorder.seller_id', Auth::user()->id);   
        }
        return $query;
    }

    
}
