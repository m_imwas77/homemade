<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class SubCategory extends Model
{
    protected $appends = ['unit_name'];

    public function getUnitNameAttribute() {
        return Unit::where('id', '=', $this->unit_id)->first()->title;
    }

    public function mainCategory()
    {
        return $this->belongsTo('App\MainCategory');
    }
    public function unit()
    {
        return $this->belongsTo('App\Unit');
    }

}
