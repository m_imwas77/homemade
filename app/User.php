<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends \TCG\Voyager\Models\User
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function setSettingsAttribute($value)
    {
        $this->attributes['settings'] = JSON_encode($value);
    }
    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $setting = (object) array('locale' => 'ar');
            // $setting['locale'] = 'ar'; 
            $model->settings = $setting;
        });
    }

    public function scopeUsers($query) {
        if( Auth::user()->role->name == 'admin') {
            return $query->where('role_id', 1);
        }
    }

    public function customer()
    {
        return $this->hasOne('App\Customer', 'user_id', 'id');
    }
    public function seller()
    {
        return $this->hasOne('App\Seller', 'user_id', 'id');
    }
}
