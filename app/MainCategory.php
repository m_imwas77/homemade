<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class MainCategory extends Model
{
    public function subCategories()
    {
        return $this->hasMany('App\SubCategory');
    }

}
