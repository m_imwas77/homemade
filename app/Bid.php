<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Bid extends Model
{
    public function seller()
    {
        return $this->belongsTo('App\Seller', 'seller_id', 'id');
    }
    public function order() {
        return $this->belongsTo(\App\order::class, 'order_id', 'id');
    }
    public function scopeBids($query) {
        if( Auth::user()->role->name == 'admin') {
            return $query;
        }
        if(Auth::user()->role->name == 'seller') {
            return $query->where('bids.seller_id', Auth::user()->seller->id);
        }
        else {
            return $query->join('orders', 'orders.id', '=', 'bids.order_id')
                         ->join('customers', 'customers.id', '=', 'orders.customer_id')
                         ->select('bids.*')
                         ->where('customers.user_id', Auth::user()->id);
        }
    }
}
