<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class SellerRate extends Model
{
    protected $table = 'seller_rate';
}
