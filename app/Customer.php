<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Customer extends Model
{
    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->customer_name = request('customer_name');
        });
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }  
    public function scopeCustomers($query) {
        if( Auth::user()->role->name == 'admin') {
            return $query;
        }
        else {
            return $query->where('user_id',Auth::user()->id);
        }
    } 
}
