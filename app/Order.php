<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class Order extends Model
{
    protected $appends = ['numberOfCreatedDay', 'numberOfBid', 'end_receive_bid'];
    

    public function getEndReceiveBidAttribute() {
        $end_date = Carbon::parse($this->end_date);
        $now = Carbon::now();
        if($end_date < $now || $this->accepted == 1) {
            return true;
        }
        return false;
    }
    
    public function getNumberOfCreatedDayAttribute() {
        $date = Carbon::parse($this->start_date);
        $now = Carbon::now();

        $diff = $date->locale('ar')->diffForHumans();
        return $diff;
    }
    
    
    public function getNumberOfBidAttribute() {        
        return $this->bids()->count();
    }
    
    
    public function scopeOrders($query) {
        if( Auth::user()->role->name == 'admin') {
            return $query;
        }
        else {
            return $query->where( 'customer_id', Auth::user()->customer->id)->orderBy('created_at', 'desc');
        }
    }
    public function ordersDetails()
    {
        return $this->hasMany('App\OrdersDetail');
    }
    public function customer()
    {
        return $this->belongsTo('App\Customer', 'customer_id', 'id');
    }
    public function bids()
    {
        return $this->hasMany('App\Bid');
    }
}
